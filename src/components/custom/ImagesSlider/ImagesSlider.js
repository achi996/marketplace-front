import React from 'react'
import { UncontrolledCarousel } from 'reactstrap'

function ImagesSlider({ data=[] }) {
  return (
    <UncontrolledCarousel
      indicators={false}
      items={ data }
    />
  )
}

export default ImagesSlider
