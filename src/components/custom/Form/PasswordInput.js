import React, { useState } from 'react'
import { Eye, EyeOff } from 'react-feather';
import { Input } from 'reactstrap';

function PasswordInput(props) {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <Input type={isOpen ? "text" : "password"} placeholder="Icon Right, Default Input" {...props}/>
      <div onClick={()=>setIsOpen(prev=>!prev)} className="form-control-position">
        {isOpen ? <Eye size={15} /> : <EyeOff size={15} />}
      </div>
    </>
  )
}

export default PasswordInput
