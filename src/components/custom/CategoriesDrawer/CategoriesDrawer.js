import React, { Component } from 'react'
import { connect } from 'react-redux';
import '../../../assets/scss/components/custom/categories-drawer.scss';
import CategoriesCard from './CategoriesCard';

export const categoriesDrawerCtx = React.createContext({
  isOpen: false,
  currentCategory: null,
  toggleCategoriesDrawer: ()=>{},
  setCurrentCategory: ()=>{},
});

export class CategoriesDrawer extends Component {
  state = {
    isOpen: false,
    currentCategory: null,
  };

  toggleCategoriesDrawer=()=>{
    this.setState(state=>({
      ...state,
      isOpen: !state.isOpen
    }));
  }

  setCurrentCategory=(cat)=>{
    this.setState({
      currentCategory: cat,
    })
  }
  
  render() {
    return (
      <categoriesDrawerCtx.Provider value={{
        isOpen: this.state.isOpen,
        currentCategory: this.state.currentCategory,
        toggleCategoriesDrawer: this.toggleCategoriesDrawer,
        setCurrentCategory: this.setCurrentCategory,
      }}>
        {this.props.children}
        {this.state.isOpen && <CategoriesCard/>}
      </categoriesDrawerCtx.Provider>
    )
  }
}

const mapStateToProps = (state) => ({
  queryCategory: state.query.category,
  categories: state.categories.categories,
})

export default connect(mapStateToProps, {})(CategoriesDrawer)
