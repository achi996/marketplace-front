import React, { useCallback, useContext, useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { ChevronDown, ChevronRight } from 'react-feather';
import { Card, CardBody, CardHeader } from 'reactstrap';
import { categoriesDrawerCtx } from './CategoriesDrawer';
import classNames from 'classnames';
import TreeMenu from 'react-simple-tree-menu';
import { history } from '../../../history';

function CategoriesDrawerChild({ cat }) {
  const [ isHide, setIsHide ] = useState(false);
  const queryCategory = useSelector(state => state.query.category);

  const {
    setCurrentCategory,
    toggleCategoriesDrawer,
  } = useContext(categoriesDrawerCtx);

  useEffect(()=>{
    setIsHide(cat.children && cat.children.length > 3);
  },[cat]);

  const selectCategory = useCallback((cat)=>{
    history.push(`/products?category=${cat.id}`);
    setCurrentCategory(cat);
    toggleCategoriesDrawer();
  }, [setCurrentCategory, toggleCategoriesDrawer])

  const handleItemClick = useCallback((cat)=>{
    if(!cat.hasNodes){
      selectCategory(cat);
    }
  }, [selectCategory])

  return (
    <li
      className={classNames(
        "child-categories__item child-categories__second-layer-item",
        { "isHide": isHide }
      )}
    >
      <p className="child-categories__parent" onClick={()=>selectCategory(cat)}>{cat.title}</p>
      <TreeMenu
        data={cat.children || []}
        onClickItem={handleItemClick}
        initialActiveKey={ queryCategory[0] && `category-${queryCategory[0]}`} // the path to the active node
        initialOpenNodes={[ queryCategory[0] && `category-${queryCategory[0]}`]}
        resetOpenNodesOnDataUpdate={true}
      >
      {({ items }) => (
        <ul className="list-unstyled">
          {items.map(props =>
            <li
              className="child-categories__item"
              onClick={(...args)=>{
                props.hasNodes && props.toggleNode && props.toggleNode();
                props.onClick(...args);
              }}
              key={props.key}
              style={{
                paddingLeft: 10*props.level + (props.hasNodes ? 0 : 20) + "px"
              }}
            >
              <p>
                {props.hasNodes ? (
                  props.isOpen ?
                    <ChevronDown
                      size={20}
                    /> :
                    <ChevronRight
                      size={20}
                    />
                ) : " -"} {props.title}
              </p>
            </li>
          )}
        </ul>
      )}
      </TreeMenu>
      {cat.children && cat.children.length > 3 &&
        <div
          className="view-more__shadow"
          onClick={() => setIsHide(!isHide)}
        >
          {isHide ? "ещё" : "cкрыть"}
        </div>
      }
    </li>
  )
}

function CategoriesCard() {
  const {
    categories
  } = useSelector(state => state.categories);

  // const dispatch = useDispatch();

  const {
    // isOpen,
    toggleCategoriesDrawer,
    setCurrentCategory,
    currentCategory,
  } = useContext(categoriesDrawerCtx);

  const selectCategory = (cat)=>{
    history.push(`/products?category=${cat.id}`);
    setCurrentCategory(cat);
    toggleCategoriesDrawer();
  }

  const handleClose = (e)=>{
    if(e.target === e.currentTarget){
      toggleCategoriesDrawer()
    }
  }

  return (
    <div className="categories-drawer" onClick={handleClose}>
      <Card className="mb-1 categories-drawer__card">
        <CardHeader>
          <h3>Категории</h3>
          <span className="float-right cursor-pointer" onClick={handleClose}>Закрыть</span>
        </CardHeader>
        <CardBody className="categories-drawer__card-body">
          <div className="parent-categories-wrapper">
            <ul className="list-unstyled parent-categories">
              {categories.map(cat => (
                <li className="parent-categories__item" onClick={()=>selectCategory(cat)} onMouseEnter={() => setCurrentCategory(cat)}>
                  {cat.title}
                </li>
              ))}
            </ul>
          </div>
          <div className="categories-drawer__right">
            {currentCategory && (
              <>
                <h4>{currentCategory.title}</h4>
                <div className="child-categories__wrapper">
                  <ul className="list-unstyled child-categories__grid">
                    {currentCategory?.children && currentCategory.children.map(cat => (
                      <CategoriesDrawerChild
                        cat={cat}
                        key={`child-categories-${cat.slug}`}
                      />
                    ))}
                  </ul>
                </div>
              </>
            )}
          </div>
        </CardBody>
      </Card>
    </div>
  )
}

export default CategoriesCard
