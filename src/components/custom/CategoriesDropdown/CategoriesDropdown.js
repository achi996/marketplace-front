import React, { Component, useState } from 'react'
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap"
import classnames from "classnames"
import { ChevronDown, AlignLeft } from "react-feather"
import { Link } from "react-router-dom"
import { connect } from "react-redux"

const dropdownModifiers = {
  setMaxHeight: {
    enabled: true,
    fn: data => {
      let pageHeight = window.innerHeight;
      let ddTop = data.instance.reference.getBoundingClientRect().top;
      let ddHeight = data.popper.height;
      let maxHeight;
      let stylesObj;

      if (pageHeight - ddTop - ddHeight - 28 < 1) {
        maxHeight = pageHeight - ddTop - 25;
        stylesObj = {
          maxHeight: maxHeight,
          // overflowY: "auto"
        };
      }

      return {
        ...data,
        styles: {
          ...stylesObj
        }
      }
    }
  }
};

const CustomDropdown = ({
  children,
  title,
  icon,
  toggleProps={},
  hover=false,
  toggle,
  ...props
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const handleToggle = (...args)=>{
    if(toggle){
      return toggle({
        setIsOpen,
        isOpen
      }, ...args);
    };
    setIsOpen(!isOpen);
  }
  return (
    <Dropdown
      toggle={handleToggle}
      isOpen={isOpen}
      onMouseEnter={ () => hover && setIsOpen(true) }
      onMouseLeave={ () => hover && setIsOpen(false) }
      {...props}
    >
      <DropdownToggle
        onClick={ () => setIsOpen(!isOpen) }
        {...toggleProps}
      >
        <div className="dropdown-toggle-sub text-truncate">
          {icon && (
            <span className="menu-icon align-bottom mr-1">
              {icon}
            </span>
          )}
          <span className="menu-title align-middle">
            {title}
          </span>
        </div>
        <ChevronDown className="ml-50 align-bottom" size={15}/>
      </DropdownToggle>
      {children}
    </Dropdown>
  )
}

class CategoriesDropdown extends Component {
  state = {
    isOpen: false,
    openDropdown: [],
  }

  renderSubMenu = (categories) => {
    return (
      <DropdownMenu
        tag="ul"
        className="mt-50"
        onMouseEnter={e => e.preventDefault()}
        modifiers={dropdownModifiers}
      >
        {categories.map(child => {
          // const CustomAnchorTag = child.type === "external-link" ? `a` : Link;
          const CustomAnchorTag = Link;
          return (
            <React.Fragment key={child.id}>
              <li
                className={classnames({
                  // active: this.state.activeParents.includes(child.id)
                })}
              >
                <DropdownItem
                  className={classnames("w-100", {
                    // hover: this.state.itemHover === child.id,
                    "has-sub": child.children,
                    active: child.navLink && child.navLink === this.props.activePath,
                    // "has-active-child": this.state.openDropdown.includes(child.id),
                  })}
                  tag={child.navLink ? CustomAnchorTag : "div"}
                  to={child.navLink && child.type === "item" ? child.navLink : "#"}//TODO
                  // target={child.newTab ? "_blank" : undefined}
                  // onMouseEnter={() => this.handleItemHover(child.id)}
                  // onMouseLeave={() => this.handleItemHover(null)}
                >
                  {child.children?.length ? (
                    <CustomDropdown
                      title={child.title}
                      className="sub-menu w-100 p-1"
                      direction={this.props.leftDir ? "left" : "right"}
                      toggle={()=>true}
                      toggleProps={{
                        className:"d-flex justify-content-between align-items-center item-content",
                        tag:"div",
                      }}
                      hover
                    >
                      {!!child.children?.length && this.renderSubMenu(child.children) }
                    </CustomDropdown>
                  ) : (
                    <div className="item-content p-1 text-truncate">
                      <span className="menu-icon align-top mr-1">
                        {child.icon}
                      </span>
                      <span className="menu-title align-middle">
                        {child.title}
                      </span>
                    </div>
                  )}
                </DropdownItem>
              </li>
            </React.Fragment>
          )
        })}
      </DropdownMenu>
    );
  };
  render() {
    return !!this.props.categories?.length &&
      <CustomDropdown
        className="nav-link"
        icon={<AlignLeft />}
        title="Категории"
        toggle={({setIsOpen}) => setIsOpen(true)}
        toggleProps={{
          className:"d-flex align-items-center",
          tag:"div",
        }}
        hover
      >
        {this.renderSubMenu(this.props.categories)}
      </CustomDropdown>
  }
}

const mapStateToProps = state => {
  return {
    categories: state.categories.categories,
  }
}

export default connect(mapStateToProps, null)(CategoriesDropdown)
