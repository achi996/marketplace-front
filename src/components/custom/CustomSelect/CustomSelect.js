import React from 'react'
import classNames from 'classnames'
import {
  UncontrolledDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
} from "reactstrap"

import '../../../assets/scss/components/custom/custom-select.scss'

function CustomSelect({
  items = [],
  handleItemClick,
  value = "",
  placeholder = "",
  className,
  dropdownProps = {},
  dropdownToggleProps = {},
  dropdownMenuProps = {},
  dropdownItemProps = {},
}) {
  return (
    <UncontrolledDropdown className={classNames("custom-select", className)} {...dropdownProps}>
      <DropdownToggle
        tag="div"
        className="font-small-3 cursor-pointer"
        {...dropdownToggleProps}
      >
        <span className="align-middle">{value || placeholder || "Select..."}</span>
      </DropdownToggle>
      <DropdownMenu tag="ul" className="p-50" {...dropdownMenuProps}>
        {items.map(item=>(
          <DropdownItem key={item.key} onClick={()=>handleItemClick && handleItemClick(item)} {...dropdownItemProps}>
            {item.label}
          </DropdownItem>
        ))}
      </DropdownMenu>
    </UncontrolledDropdown>
  )
}

export default CustomSelect
