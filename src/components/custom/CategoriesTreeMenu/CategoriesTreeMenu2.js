import React, { useMemo } from 'react'
import { ChevronDown, ChevronRight } from 'react-feather';
import { useDispatch, useSelector } from 'react-redux';
import TreeMenu from 'react-simple-tree-menu';
// import { Input } from 'reactstrap';
import { setQueryParam } from '../../../redux/slices/query.slice';

import '../../../assets/scss/components/custom/category-tree.scss';
import { history } from '../../../history';

function CategoriesTreeMenu({ force, onChange, categoryId}) {

  const dispatch = useDispatch();

  const handleItemClick = (cat) => {
    if(force){
      history.push(`/products?category=${cat.id}`)
    }else{
      dispatch(
        setQueryParam({
          key: "category",
          value: cat.children?.length ? cat.id + "," + cat.children.map(child => child.id).join(",") : cat.id
        })
      )
    }
    if(onChange)onChange()
  };

  const {
    categories
  } = useSelector(state => state.categories);

  const treeData = useMemo(()=>[{label: "Все", key: "category-ALL", id: ""},...categories], [categories])

  const queryCategory = useSelector(state => state.query.category);

  const handleChevronClick = (e, toggleNode)=>{
    e.stopPropagation();
    return toggleNode && toggleNode();
  }

  return (
    <TreeMenu
      data={treeData}
      onClickItem={handleItemClick}
      initialActiveKey={`category-${queryCategory[0]||"ALL"}`} // the path to the active node
      initialOpenNodes={[`category-${queryCategory[0]||"ALL"}`]}
      resetOpenNodesOnDataUpdate={true}
    >
      {({ items }) => (
        <>
          {/* <Input className="mb-1" onChange={e => search(e.target.value)} placeholder="Поиск категории" /> */}
          <ul className="list-unstyled categories-list categories-tree row flex-column">
            {items.map(props => (
              // You might need to wrap the third-party component to consume the props
              // check the story as an example
              // https://github.com/iannbing/react-simple-tree-menu/blob/master/stories/index.stories.js
              <li
                className={`categories-tree__item ${props.active ? "active" : ""}`}
                onClick={(...args)=>{
                  props.onClick(...args);
                }}
                // active={props.active}
                // toggleNode={props.toggleNode}
                // hasNodes={props.hasNodes}
                // isOpen={props.isOpen}
                // level={props.level}
                key={props.key}
                // label={props.label}
                // nodes={props.nodes}
                style={{
                  paddingLeft: 10*props.level + (props.hasNodes ? 0 : 20) + "px"
                }}
              >
                {props.hasNodes && (
                  props.isOpen ? 
                    <ChevronDown
                      size={20}
                      onClick={(e)=>handleChevronClick(e, props.toggleNode)}
                    /> : 
                    <ChevronRight
                      size={20}
                      onClick={(e)=>handleChevronClick(e, props.toggleNode)}
                    />
                )}
                <span>
                  {props.label}
                </span>
              </li>
            ))}
          </ul>
        </>
      )}
    </TreeMenu>
  )
}

export default CategoriesTreeMenu
