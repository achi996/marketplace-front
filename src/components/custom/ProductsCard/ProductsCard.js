import React, {memo, useMemo} from "react"
import { Card, CardBody } from "reactstrap"
import { Heart, ShoppingCart } from "react-feather"
import {Link, useHistory} from "react-router-dom"
import {useDispatch, useSelector} from "react-redux"

import {toggleItemInCart} from "../../../redux/slices/cart.slice"
import {toggleItemInWishlist} from "../../../redux/slices/wishlist.slice"
import classNames from "classnames"
import {getPrices} from "../../../helpers/utils/productPrices"
import Swiper from "react-id-swiper";

import "../../../assets/scss/plugins/extensions/swiper.scss"

function ProductsCard({data, disableShopAvatar}) {
  const history = useHistory();
  const isInCart = useSelector(state => !!state.cart.entities[data.slug]);
  const isInWishlist = useSelector(state => !!state.wishlist.entities[data.slug]);

  const {
    viewPrice,
    strikethroughPrice,
    actual_wholesale_price,
    wholesale,
    retail,
  } = useMemo(()=>getPrices(data),[data])

  const dispatch = useDispatch();
  const handleAddToCart = () => {
    dispatch(toggleItemInCart(data));
  };
  const handleAddToWishlist = () => {
    dispatch(toggleItemInWishlist(data));
  }
  const productOptionsImages = data.images
    .map(image => (
      {
        src: image.image,
        id: image.id,
        altText: "",
        caption: "",
        header: ""
      }
    ));

  const gallerySwiperParams = {
    spaceBetween: 10,
    shouldSwiperUpdate: false,
    rebuildOnUpdate: false,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    }
  }


  // const handleCarouselClick = (e) => {
  //   if(e.target.nodeName === "IMG"){
  //     history.push(`/products/${oldImages.slug}`)
  //   }
  // }

  return (
    <Card className="mb-1 ecommerce-card">
      <div className="full-height card-content flex-column justify-content-between ">
        {!disableShopAvatar && (
          <h5 className="text-left">
            <img src={data?.shop_logo} className="shop-avatar" style={{objectFit: "cover"}} alt="avatar"/>
            <Link className="ml-2" to={`/shops/${data.shop}`}>{data.shop_name}</Link>
          </h5>
        )}
        <div className="slider">
          {productOptionsImages && (
            <>
              <Swiper {...gallerySwiperParams}>
                {productOptionsImages.map(item => (
                  <img onClick={() => history.push(`/products/${data.slug}`)} key={item.id} src={item.src} style={{width: '100% !important', objectFit: "cover"}} alt=""/>
                ))}
              </Swiper>
            </>
          )}
        </div>
        <CardBody className={`p-1`}>
          <div
            className="item-wrapper d-flex align-items-center justify-content-between"
            onClick={() => history.push(`/products/${data.slug}`)}
          >
            <div>
              {wholesale && <>
                <h6 className="item-price m-0 mb-1">Оптом</h6>
                <h6 className="item-price m-0">{actual_wholesale_price} cом</h6>
              </>}
            </div>
            <div>
              {retail && <>
                <h6 className="item-price m-0 mb-1">В розницу</h6>
                { strikethroughPrice &&
                  <p className="item-regular-price">{strikethroughPrice} cом</p>
                }
                <h6 className="item-price m-0">{viewPrice} cом</h6>
              </>}
            </div>
          </div>
          <div
            className="item-name text-center pt-2 mt-0 cursor-pointer"
            onClick={() => history.push(`/products/${data.slug}`)}
          >
            <span className={`full-width text-center`}>{data.title}</span>
          </div>
          <div className="item-desc" onClick={() => history.push(`/products/${data.slug}`)}>
            <p className="item-description m-0">{data.description}</p>
          </div>
        </CardBody>
        <div className="item-options text-center">
          <div className="wishlist" onClick={handleAddToWishlist}>
            <Heart
              size={15}
              fill={
                isInWishlist
                  ? "#EA5455"
                  : "transparent"
              }
              stroke={
                isInWishlist ? "#EA5455" : "#626262"
              }
            />
            <span className="align-middle ml-50">Избранное</span>
          </div>
          <div
            className={classNames("cart", {
              "view-in-cart": isInCart,
              "bg-warning": !data.in_stock,
            })}
            onClick={data.in_stock ? handleAddToCart : undefined}
          >
            <ShoppingCart size={15}/>
            <span className="align-middle ml-50">
              {isInCart ? "Убрать из корзины" : data.in_stock ? "Добавить в корзину" : "Нет в наличии"}
            </span>
          </div>
        </div>
      </div>
    </Card>
  )
}

export default memo(ProductsCard)
