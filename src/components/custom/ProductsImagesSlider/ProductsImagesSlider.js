import React, { useEffect, useState } from 'react'
import Swiper from "react-id-swiper"
import ImageZoom from "react-medium-image-zoom";

import '../../../assets/scss/plugins/extensions/swiper.scss';

function ProductsImagesSlider({images, alt}) {
  const [data, setData] = useState(images);
  const [loading, setLoading] = useState(true);
  const [gallerySwiper, getGallerySwiper] = useState(null);
  const [thumbnailSwiper, getThumbnailSwiper] = useState(null);
  const gallerySwiperParams = {
    getSwiper: getGallerySwiper,
    spaceBetween: 10,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    }
  }
  const thumbnailSwiperParams = {
    getSwiper: getThumbnailSwiper,
    spaceBetween: 10,
    centeredSlides: true,
    slidesPerView: "auto",
    touchRatio: 0.2,
    slideToClickedSlide: true
  }

  useEffect(() => {
    if (
      gallerySwiper !== null &&
      gallerySwiper.controller &&
      thumbnailSwiper !== null &&
      thumbnailSwiper.controller
    ) {
      gallerySwiper.controller.control = thumbnailSwiper
      thumbnailSwiper.controller.control = gallerySwiper
    }
  }, [gallerySwiper, thumbnailSwiper]);

  useEffect(()=>{
    if(data !== images){
      setData(images);
      setLoading(true);
    }else{
      setLoading(false);
    }
  },[images, setData, setLoading, data]);

  return (
    <div className="product-swiper-gallery swiper-gallery w-100">
      {!loading && (
        <>
          <Swiper {...gallerySwiperParams}>
            {images.map(item=>(
              // <img 
              //   className="product-swiper-gallery__preview-item" 
              //   src={item.image} 
              //   alt={`${alt}-${item.id}`}
              //   height="250" width="250"
              // />
              <div 
                style={{
                  overflow: "hidden"
                }}
                key={`${alt}-${item.id}`}
              >
                <ImageZoom
                  image={{
                    src: item.image,
                    alt: `${alt}-${item.id}`,
                    className: 'img',
                    style: { 
                      objectFit: "contain",
                      width: "100%",
                      height: "100%",
                    }
                  }}
                  defaultStyles={{
                    zoomImage: {
                      objectFit: "contain",
                    }
                  }}
                  zoomImage={{
                    src: item.image,
                    alt: `${alt}-${item.id}`,
                  }}
                />
              </div>
              // <div
              //   key={`${alt}-${item.id}`}
              //   style={{
              //     backgroundImage: `url(${item.image})`,
              //     backgroundSize: "contain",
              //     backgroundRepeat: "no-repeat",
              //     backgroundPosition: "center center"
              //   }}
              // />
            ))}
          </Swiper>
          <Swiper {...thumbnailSwiperParams}>
            {images.map(item=>(
              <div
                key={`${alt}-${item.id}`}
                style={{
                  backgroundImage: `url(${item.image})`,
                  backgroundSize: "contain",
                  backgroundRepeat: "no-repeat",
                  backgroundPosition: "center center"
                }}
              />
            ))}
          </Swiper>
        </>
      )}
    </div>
  )
}

export default ProductsImagesSlider
