import React, {useState} from "react"
import {Card, CardBody, Collapse} from "reactstrap"
import "rc-slider/assets/index.css"
import * as Icon from "react-feather"
import {NavLink} from  "react-router-dom";
import "../../../assets/scss/components/custom/sidebar.scss"
import Sidebar from "react-sidebar"

const mql = window.matchMedia(`(min-width: 992px)`)

export default function ShopSidebar() {
  const [open, setOpen] = useState(true)
  const [sidebarDocked, setSidebarDocked] = useState(mql.ma)
  const links = [
    {id: 1, icon: "Activity", title: "Hello", slug: "activity"},
    {id: 2, icon: "Activity", title: "Hello", slug: "activity"},
    {id: 3, icon: "Activity", title: "Hello", slug: "activity"},
    {id: 4, icon: "Activity", title: "Hello", slug: "activity"},
    {id: 5, icon: "Activity", title: "Hello", slug: "activity"},
    {id: 6, icon: "Activity", title: "Hello", slug: "activity"},
    {id: 7, icon: "Activity", title: "Бытовая техника", slug: "activity"},
    {id: 8, icon: "Activity", title: "Hello", slug: "activity"},
    {id: 9, icon: "Activity", title: "Hello", slug: "activity"},
    {id: 10, icon: "Activity", title: "Hello", slug: "activity"},
  ]

  return (
    <React.Fragment>
      <h6 className="filter-heading d-none d-lg-block">Filters</h6>
      <Card className="mb-1">
        <CardBody className="p-2">
          <div className="product-categories">
            <div className="category-head mb-1">
              <h6 className="filter-title">Categories</h6>
              <button className={"burger-menu"} onClick={() => setOpen(!open)}><Icon.Menu/></button>
            </div>
           <Sidebar sidebar={
             <ul className="categories-list">
               {links.map(link=>{
                 const LinkIcon = Icon[link.icon];
                 return (
                   <li key={link.id+"-category-item"}>
                     <NavLink to={`/products/?category=${link.slug}`} className={"link"}>
                       <LinkIcon/>
                       {link.title}
                     </NavLink>
                   </li>
                 );
               })}
             </ul>
           } shadow={false} docked={true} contentClassName={`d-none`} children={''}/>
          </div>
        </CardBody>
      </Card>
    </React.Fragment>
  )
}
