import React, {useCallback} from "react";
import {useDropzone} from "react-dropzone";

export default function CustomDragNDrop({setState, onActiveText, text, disabled}) {
  const onDrop = useCallback(acceptedFiles => {
    setState(acceptedFiles[0])
  }, [setState])
  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})

  return (
    <div className={`full-width`}>
      <div className={`drag-n-drop`} {...getRootProps()}>
        <input {...getInputProps()} disabled={disabled ? disabled : false}/>
        {
          isDragActive ?
            <p className={`m-0`}>{onActiveText}</p> :
            <p className={`m-0`}>{text}</p>
        }
      </div>
    </div>
  )
}
