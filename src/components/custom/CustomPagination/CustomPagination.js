import React from 'react'
import ReactPaginate from "react-paginate"
import {ChevronLeft, ChevronRight} from 'react-feather'
import "../../../assets/scss/plugins/extensions/react-paginate.scss"
import { useDispatch } from 'react-redux'
import { setQueryParam } from '../../../redux/slices/query.slice'

function CustomPagination({active = 1, totalPages, linkPredicate = "?page="}) {
  const dispatch = useDispatch()

  const handleChangePage = (page) => {
    dispatch(setQueryParam({
      key: "page",
      value: page.selected + 1
    }))
  }

  return <ReactPaginate
    previousLabel={<ChevronLeft size={15}/>}
    nextLabel={<ChevronRight size={15}/>}
    breakLabel="..."
    breakClassName="break-me"
    pageCount={totalPages}
    containerClassName="vx-pagination separated-pagination pagination-end pagination-sm mb-0 mt-2"
    activeClassName="active"
    forcePage={
      active
        ? parseInt(active - 1)
        : 1
    }
    onPageChange={handleChangePage}
  />
}

export default CustomPagination
