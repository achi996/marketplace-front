import React from 'react'
import { Facebook, Instagram, Twitter, Youtube } from 'react-feather'
import { Button } from 'reactstrap'

function ShareToSocials() {
  return (
    <React.Fragment>
      <Button.Ripple
        className="mr-1 btn-icon rounded-circle"
        color="primary"
        outline
      >
        <Facebook size={15} />
      </Button.Ripple>
      <Button.Ripple
        className="mr-1 btn-icon rounded-circle"
        color="info"
        outline
      >
        <Twitter size={15} />
      </Button.Ripple>
      <Button.Ripple
        className="mr-1 btn-icon rounded-circle"
        color="danger"
        outline
      >
        <Youtube size={15} />
      </Button.Ripple>
      <Button.Ripple
        className="btn-icon rounded-circle"
        color="primary"
        outline
      >
        <Instagram size={15} />
      </Button.Ripple>
    </React.Fragment>
  )
}

export default ShareToSocials
