import Axios from 'axios';
import React, {useEffect, useState} from 'react'
import {Card, CardBody, CardHeader, CardTitle, Col, Row} from 'reactstrap';
import {API_BASE_URL} from '../../../helpers/constants';
import {AlertCircle, AtSign, Facebook, Instagram, Phone, PhoneCall, Send} from "react-feather";

const fetchData = async () => {
  const {data} = await Axios.get(`${API_BASE_URL}/store_settings/contact_info/`);
  return data.results;
}

export default function Contacts() {
  const [data, setData] = useState([]);
  useEffect(() => {
    fetchData().then(setData);
  }, [setData]);

  return !!data.length ? (
    <div>
      <Row>
        {!!data.length &&
        data.map(infos => (
          <Col key={infos.id + "contact-info"} md="6" lg={'4'} sm="12">
            <Card className='mb-1'>
              <CardHeader>
                <CardTitle>{infos.name}</CardTitle>
              </CardHeader>
              <CardBody>
                <ul className="activity-timeline timeline-left list-unstyled">
                  <li>
                    <div className="timeline-icon" style={{backgroundColor: "#f55c47"}}>
                      <PhoneCall size="18"/>
                    </div>
                    <div className="timeline-info">
                      <p className="font-weight-bold">Номер телефона</p>
                      <span>
                          {infos.phone_number}
                    </span>
                    </div>
                  </li>
                  <li>
                    <div className="timeline-icon" style={{backgroundColor: "#4aa96c"}}>
                      <AtSign size="18"/>
                    </div>
                    <div className="timeline-info">
                      <p className="font-weight-bold">Email</p>
                      <span>{infos.email}</span>
                    </div>
                  </li>
                  {infos.instagram &&
                  <li>
                    <div className="timeline-icon" style={{backgroundColor: "#CE97B0"}}>
                      <Instagram size="18"/>
                    </div>
                    <div className="timeline-info">
                      <p className="font-weight-bold">Instagram</p>
                      <span>{infos.instagram}</span>
                    </div>
                  </li>}
                  {infos.facebook &&
                  <li>
                    <div className="timeline-icon" style={{backgroundColor: "#38A3A5"}}>
                      <Facebook size="18"/>
                    </div>
                    <div className="timeline-info">
                      <p className="font-weight-bold">Facebook</p>
                      <span>{infos.instagram}</span>
                    </div>
                  </li>}
                  {infos.telegram &&
                  <li>
                    <div className="timeline-icon" style={{backgroundColor: "#22577A"}}>
                      <Send size="18"/>
                    </div>
                    <div className="timeline-info">
                      <p className="font-weight-bold">Telegram</p>
                      <span>{infos.telegram}</span>
                    </div>
                  </li>}
                  {infos.whatsapp &&
                  <li>
                    <div className="timeline-icon" style={{backgroundColor: "#80ED99"}}>
                      <Phone size="18"/>
                    </div>
                    <div className="timeline-info">
                      <p className="font-weight-bold">Whatsapp</p>
                      <span>{infos.whatsapp}</span>
                    </div>
                  </li>}
                  <li>
                    <div className="timeline-icon" style={{backgroundColor: "#564a4a"}}>
                      <AlertCircle size="18"/>
                    </div>
                    <div className="timeline-info">
                      <p className="font-weight-bold">ИНН</p>
                      <span>{infos.inn}</span>
                    </div>
                  </li>
                </ul>
              </CardBody>
            </Card>
          </Col>
        ))
        }
      </Row>
    </div>
  ) : null
}
