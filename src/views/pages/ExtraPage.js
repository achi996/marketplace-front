import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useRouteMatch } from 'react-router-dom';
import { API_BASE_URL } from '../../helpers/constants';

const fetchPageData = async (page)=>{
  const { data } = await Axios.get(`${API_BASE_URL}/store_settings/${page}/`);
  return data?.results?.[0]?.content || "";
}

const useFetchPageData = () => {
  const { params: { page } } = useRouteMatch();
  const [content, setContent] = useState();
  useEffect(()=>{
    fetchPageData(page).then(setContent);
  },[page]);

  return {
    content: content,
  }
}

function createMarkup(content){
  return {
    __html: content
  }
}

function ExtraPage() {
  const { content } = useFetchPageData();
  return (
    <div dangerouslySetInnerHTML={createMarkup(content)}>
    </div>
  )
}

export default ExtraPage
