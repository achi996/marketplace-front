import React, {useEffect} from "react";
import {Card, CardBody, Col, Row} from "reactstrap";
// import Carousel from "react-multi-carousel";
import {useDispatch, useSelector} from "react-redux";

import {fetchMainPageProducts} from "../../../redux/slices/products.slice";
import ProductsCard from "../../../components/custom/ProductsCard/ProductsCard";

import "../../../assets/scss/pages/app-ecommerce-shop.scss";
import "../../../assets/scss/plugins/forms/react-select/_react-select.scss"

const Recomendations = () => {
  // const [hoverSlide, setHoverSlide] = useState();
  const products = useSelector(state => state.products.mainPageProducts);

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchMainPageProducts());
  }, [dispatch])

  if (!products?.length) return null;
  return (
    <React.Fragment>
      <Card className="mb-1">
        <CardBody>
          <Row>
            <Col className="main-page-swiper text-center" sm="12">
              <div className="heading-section">
                <h2 className="text-uppercase">Новинки</h2>
              </div>

              <div className="grid-view main-page-grid-view">
                {products.map((item, index) => {
                  return (
                    <ProductsCard data={item} key={index + "-recomendations"}/>
                  );
                })}

              </div>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </React.Fragment>
  );
}

export default Recomendations;
