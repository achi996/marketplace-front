import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from "reactstrap";
import { ChevronRight } from "react-feather";
import React from "react";
import { history } from "../../../history";

function closeDrop(id, setState) {
  setTimeout(() => {
    setState(s => s.filter(i => i !== id))
  }, 50)
}

const SubCatDropDown = ({ id, title, icon, navLink, child, openedDrops, closeDrops, setOpenedDrops }) => {
  return (
    <DropdownItem
      tag="div"
      className={`m-0 p-0`}
      // toggle={() => true}
      onMouseEnter={() => setOpenedDrops(s => [...s, id])}
      onMouseLeave={() => closeDrop(id, setOpenedDrops)}
      style={{ width: "300px" }}
      onClick={(e)=>{
        e.stopPropagation();
        if(navLink){
          history.push(`/products/${navLink}`);
          closeDrops()
        }
      }}
    >
      {!!child?.length ?
        <Dropdown
          isOpen={openedDrops.includes(id)}
          direction="right"
          className={`m-0`}
        >
          <DropdownToggle
            className={`d-flex align-items-center justify-content-between pr-2`}
            style={{ background: "none !important" }}
            tag="div"
            caret
          >
            {icon && icon}
            <p className={`m-0 ml-2`}>{title}</p>
            <ChevronRight
              className="has-sub-arrow align-middle ml-50"
              size={15}
            />
          </DropdownToggle>
          <DropdownMenu>
            {
              child.map(reChild => (
                <SubCatDropDown
                  id={reChild.id}
                  key={reChild.id}
                  title={reChild.title}
                  navLink={reChild.navLink}
                  child={reChild.children}
                  closeDrops={closeDrops}
                  openedDrops={openedDrops}
                  setOpenedDrops={setOpenedDrops}
                />
              ))
            }
          </DropdownMenu>
        </Dropdown> :
        <div className={`d-flex align-items-center`}>
          {icon && icon}
          <p className={`m-0 ml-2`}>{title}</p>
        </div>
      }
    </DropdownItem>
  )
}

export default function CustomDropdown({ id, icon, title, children, closeDrops, navLink, openedDrops, setOpenedDrops }) {

  return (
    <Dropdown
      className={`full-width`}
      isOpen={openedDrops.includes(id)}
      toggle={() => true}
      direction="right"
    >
      <DropdownToggle
        className={`d-flex align-items-center justify-content-between pr-2`}
        style={{ background: "none !important" }}
        tag="div"
        caret
        onClick={(e)=>{
          e.stopPropagation();
          if(navLink){
            history.push(`/products/${navLink}`);
            closeDrops()
          }
        }}
      >
        {icon}
        <p className={`m-0 pl-1`}>{title}</p>
        <ChevronRight
          className="has-sub-arrow align-middle ml-50"
          size={15}
        />
      </DropdownToggle>
      <DropdownMenu>
        {
          children.length && children.map(child => (
            <SubCatDropDown
              id={child.id}
              key={child.id}
              title={child.title}
              icon={child.icon}
              navLink={child.navLink}
              child={child.children}
              closeDrops={closeDrops}
              setOpenedDrops={setOpenedDrops}
              openedDrops={openedDrops}
            />
          ))
        }
      </DropdownMenu>
    </Dropdown>
  )

}
