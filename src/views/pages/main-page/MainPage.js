import React, {useContext, useEffect, useState} from "react";
import {Button, Card, Col, Row} from "reactstrap";
import classnames from "classnames"
import MainPageRecommendations from "./recommendations";
import {useDispatch, useSelector} from "react-redux";
import CustomDropdown from "./CustomDropdown";
import {Link} from "react-router-dom";
import {fetchBanners} from "../../../redux/slices/banners.slice";
import {fetchMainPageProducts} from "../../../redux/slices/products.slice";
import { categoriesDrawerCtx } from "../../../components/custom/CategoriesDrawer/CategoriesDrawer";
import MainPageCarousel from "./MainPageCarousel";
import "../../../assets/scss/pages/main-page.scss"


function closeDrop(id, setState) {
  setTimeout(() => {
    setState(s => s.filter(i => i !== id))
  }, 50)
}

const formatBannersData = (banners, key) => banners.map((banner, index) => ({
  src: banner.image,
  link_to_product: banner.link_to_product,
  caption: "",
  id: `${key}-${index}`,
  altText: "",
  header: ""
}))

export default function MainPage() {
  const [openedDrops, setOpenedDrops] = useState([])
  const {
    images1,
    images2,
    images3,
    categories,
  } = useSelector(state => {
    return {
      images1: formatBannersData(state.banners.bannersOne, "B1"),
      images2: formatBannersData(state.banners.bannersTwo, "B2"),
      images3: formatBannersData(state.banners.bannersThree, "B3"),
      categories: state.categories.categories,
    }
  });

  const { toggleCategoriesDrawer } = useContext(categoriesDrawerCtx)

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchBanners());
    dispatch(fetchMainPageProducts());
  }, [dispatch])

  return (
    <>
      <div className={'main-page ecommerce-application'}>
        <Row className={`m-0 p-0 flex-nowrap`}>
          <div className={`pl-0 pr-md-2 d-none d-lg-block main-page_categories`}>
            <Card style={{width: "25rem"}} className={`m-0 h-100`}>
              <div className="pt-1 px-2">
                <Link
                  to="/products/?wholesale=true"
                  className="w-50 pr-1 d-inline-block"
                >
                  <Button
                    color="primary"
                    outline
                    className="w-100"
                  >
                    Оптом
                  </Button>
                </Link>
                <Link
                  to="/products/?retail=true"
                  className="w-50 pl-1 d-inline-block"
                >
                  <Button
                    color="primary"
                    outline
                    className="w-100"
                  >
                    Розница
                  </Button>
                </Link>
              </div>

              <Button
                className="d-block mx-auto mt-2"
                color="primary"
                onClick={toggleCategoriesDrawer}
              >
                Посмотреть все категории
              </Button>

              <ul className={`list-unstyled d-flex flex-column pt-1 pb-1 m-0 p-0 w-100`}>
                {
                  categories && categories.map(c =>
                    <li
                      onMouseEnter={() => setOpenedDrops(s => !s.includes(c.id) ? [...s, c.id] : s)}
                      onMouseLeave={() => closeDrop(c.id, setOpenedDrops)}
                      className={`d-flex align-items-center cursor-pointer ${classnames({
                        active: openedDrops.includes(c.id)
                      })}`} key={c.id}>
                      {c.children.length ? <CustomDropdown
                          id={c.id}
                          key={c.id}
                          title={c.title}
                          children={c.children}
                          navLink={c.navLink}
                          closeDrops={() => closeDrop(c.id, setOpenedDrops)}
                          openedDrops={openedDrops}
                          setOpenedDrops={setOpenedDrops}
                        /> :
                        <div className={`d-flex align-items-center`}>
                          <p className={`m-0 pl-1`}>{c.title}</p>
                        </div>}
                    </li>
                  )
                }
              </ul>
            </Card>
          </div>
          <div className={`pl-0 pr-0 main-page-carousels`}>
            {/* //TODO ВОТ НАХУЯ Я ЭТО ПЕРЕДЕЛЫВАЮ ССССУКА, УЖЕ МИЛЛИОННЫЙ РАЗ, ААААА!!! */}
            <Col className={`swiper p-0 main-page-carousels__top__wrapper`}>
              <MainPageCarousel items={images1} interval={8000}/>
            </Col>
            <div className={`w-100 row m-0 main-page-carousels__bottom__wrapper`}>
              {/* //TODO FIX ME OR KILL ME PLEASE */}
              <Col className={`p-0 h-100`}>
                <MainPageCarousel items={images2} interval={3000}/>
              </Col>
              <Col className={`p-0 h-100`}>
                {/* <UncontrolledCarousel indicators={false} items={images3} interval="5000"/> */}
                <MainPageCarousel items={images3} interval={5000}/>
              </Col>
            </div>
          </div>
        </Row>
        <MainPageRecommendations/>
      </div>
    </>
  )
}
;
