const data = [
    {
        id: 1,
        name: "Amazon - Fire TV Stick with Alexa Voice Remote - Black",
        by: "Google",
        desc: `Enjoy smart access to videos, games and apps with this Amazon Fire TV stick. Its Alexa voice remote lets you
        deliver hands-free commands when you want to watch television or engage with other applications. With a
        quad-core processor, 1GB internal memory and 8GB of storage, this portable Amazon Fire TV stick works fast
        for buffer-free streaming.`,
        price: "$39.99",
        img: [
            {
                src: require("../../../assets/img/pages/eCommerce/1.png"),
                altText: "Slide 1",
                key: '1',
                caption: "",
            },{
                src: require("../../../assets/img/pages/eCommerce/2.png"),
                altText: "Slide 1",
                key: '2',
                caption: "",
            },{
                src: require("../../../assets/img/pages/eCommerce/3.png"),
                altText: "Slide 1",
                key: '3',
                caption: "",
            },
        ]
      },
      {
        id: 2,
        price: "$35",
        img: [
            {
                src: require("../../../assets/img/pages/eCommerce/1.png"),
                altText: "Slide 1",
                key: '1',
                caption: "",
            },{
                src: require("../../../assets/img/pages/eCommerce/2.png"),
                altText: "Slide 1",
                key: '2',
                caption: "",
            },{
                src: require("../../../assets/img/pages/eCommerce/3.png"),
                altText: "Slide 1",
                key: '3',
                caption: "",
            },
        ],
        name: "Google - Chromecast - Black",
        by: "Google",
        desc: `Google Chromecast: Enjoy a world of entertainment with Google Chromecast. Just connect to your HDTV's HDMI
        interface and your home Wi-Fi network to get started. You can stream your favorite apps from your compatible
        phone, tablet or laptop, plus use your phone as a remote to search, play and pause content.`
      },
      {
        id: 3,
        desc: `Dell Inspiron Laptop: Get speed and performance from this 15.6-inch Dell Inspiron laptop. Supported by an
        Intel Core i5-5200U processor and 6GB of DDR3L RAM, this slim touch screen laptop lets you run multiple
        applications without lag. The 1TB hard drive in this Dell Inspiron laptop lets you store multiple music,
        video and document files.`,
        price: "$499.99",
        img: [
            {
                src: require("../../../assets/img/pages/eCommerce/1.png"),
                altText: "Slide 1",
                key: '1',
                caption: "",
            },{
                src: require("../../../assets/img/pages/eCommerce/2.png"),
                altText: "Slide 1",
                key: '2',
                caption: "",
            },{
                src: require("../../../assets/img/pages/eCommerce/3.png"),
                altText: "Slide 1",
                key: '3',
                caption: "",
            },
        ],
        name: `Dell - Inspiron 15.6" Touch-Screen Laptop - Black`,
        by: "Dell"
      },
      {
        id: 4,
        img: [
            {
                src: require("../../../assets/img/pages/eCommerce/1.png"),
                altText: "Slide 1",
                key: '1',
                caption: "",
            },{
                src: require("../../../assets/img/pages/eCommerce/2.png"),
                altText: "Slide 1",
                key: '2',
                caption: "",
            },{
                src: require("../../../assets/img/pages/eCommerce/3.png"),
                altText: "Slide 1",
                key: '3',
                caption: "",
            },
        ],
        name: "Amazon - Echo Dot",
        by: "Amazon",
        desc: `Echo Dot is the latest addition to Amazon's voice-controlled devices.
        Deliver your favorite playlist anywhere in your home with the Amazon Echo Dot voice-controlled device.
        Control most electric devices through voice activation, or schedule a ride with Uber and order a pizza. The
        Amazon Echo Dot voice-controlled device turns any home into a smart home with the Alexa app on a smartphone
        or tablet.`,
        price: "$49.99"
      },
      {
        id: 5,
        by: "Apple",
        desc: `MacBook Air is a thin, lightweight laptop from Apple.
        MacBook Air features up to 8GB of memory, a fifth-generation Intel Core processor, Thunderbolt 2, great
        built-in apps, and all-day battery life.1 Its thin, light, and durable enough to take everywhere you go-and
        powerful enough to do everything once you get there, better.`,
        price: "$999.99",
        img: [
            {
                src: require("../../../assets/img/pages/eCommerce/1.png"),
                altText: "Slide 1",
                key: '1',
                caption: "",
            },{
                src: require("../../../assets/img/pages/eCommerce/2.png"),
                altText: "Slide 1",
                key: '2',
                caption: "",
            },{
                src: require("../../../assets/img/pages/eCommerce/3.png"),
                altText: "Slide 1",
                key: '3',
                caption: "",
            },
        ],
        name: `Apple - MacBook Air® (Latest Model) - 13.3" Display - Silver`
      },
      {
        id: 6,
        by: "Sharp",
        desc: `Only at Best Buy Sharp LC-50LB481U LED Roku TV: Get a TV that enjoys full Internet connectivity with this
        Sharp 49.5-inch smart TV. Full HD resolutions give you plenty of detail whether you're streaming content
        from the Internet using the integrated Roku player or watching via cable`,
        price: "$429.99",
        img: [
            {
                src: require("../../../assets/img/pages/eCommerce/1.png"),
                altText: "Slide 1",
                key: '1',
                caption: "",
            },{
                src: require("../../../assets/img/pages/eCommerce/2.png"),
                altText: "Slide 1",
                key: '2',
                caption: "",
            },{
                src: require("../../../assets/img/pages/eCommerce/3.png"),
                altText: "Slide 1",
                key: '3',
                caption: "",
            },
        ],
        name: `Sharp - 50" Class (49.5" Diag.) - LED - 1080p - Black`
      },
];
const recomendationsData = [
    {
      id: 1,
      title: "Новинки 2021",
      data
    },
    {
      id: 2,
      title: "Старинки 2021",
      data
    },
    {
      id: 3,
      title: "Будущий 2021",
      data
    },
];

export default recomendationsData;