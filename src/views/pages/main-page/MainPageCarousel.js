import React, {useEffect, useState} from 'react';
import Swiper from "react-id-swiper";

const MainPageCarousel = ({items, interval}) => {
  const [data, setData] = useState(items);
  const gallerySwiperParams = {
    spaceBetween: 10,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    shouldSwiperUpdate: false,
    rebuildOnUpdate: false,
    autoplay: {
      delay: interval,
      disableOnInteraction: false
    },
  }

  useEffect(() => {
    if (data !== items) {
      setData(items);
    }
  }, [items, setData, data]);

  return (
    <div className="main-page-carousels__swiper-gallery">
      {!!data.length && (
        <>
          <Swiper {...gallerySwiperParams}>
            {data.map(item => (
              <a key={item.id} href={item.link_to_product}>
                <img src={item.src} style={{width: '100%', height: "100%", objectFit: "cover"}} alt=""/>
              </a>
            ))}
          </Swiper>
        </>
      )}
    </div>
  );
}

export default MainPageCarousel;
