import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import { updateQueryState } from "../../redux/slices/query.slice";

const QueryStringHandler = ()=>{
  const { search } = useLocation();
  const dispatch = useDispatch();
  
  useEffect(()=>{
    dispatch(updateQueryState());
  },[search, dispatch]);
  return null;
}

export default QueryStringHandler;