import React, {useEffect} from "react";
import {Card, CardBody, Input} from "reactstrap";
import {Search, Star} from "react-feather";
import {useDispatch, useSelector} from "react-redux";
import {Redirect} from "react-router-dom";
import CustomRange from "../../../../helpers/utils/range";
import {feedbacksSelector, fetchFeedbacks} from "../../../../redux/slices/store/myFeedbacks";
import {setQueryParam} from "../../../../redux/slices/query.slice";

const StoreFeedbacks = () => {
  const {data: store, loading, error} = useSelector(state => state.store.myStore);
  const feedbacks = useSelector(state=>feedbacksSelector.selectAll(state.store.myFeedbacks));
  const dispatch = useDispatch();

  const {search} = useSelector(state => state.query);
  useEffect(() => {
    dispatch(fetchFeedbacks(search));
  }, [dispatch, search]);

  const renderStars = (stars)=> new CustomRange(0,4).map((i)=>(
    <Star
      key={`product-review-star-${i}`}
      size={20}
      fill={i<stars ? "#ff9f43" : "#ffffff"}
      stroke={i<stars ? "#ff9f43" : "#b8c2cc"}
    />
  ));

  if (loading) return null;
  if (error || (store && !store?.is_active)) return <Redirect to="/account/store" />;
  return (
    <Card className="mb-1">
      <CardBody>
        <div className="position-relative has-icon-left mb-1">
          <Input onChange={e => dispatch(setQueryParam({key: "search", value: e.target.value}))}/>
          <div className="form-control-position">
            <Search size="15" />
          </div>
        </div>
        {feedbacks.map(f => (
          <Card key={`feedbacks-item-${f.id}`} style={{ border: "1px solid #d9d9d9" }} className={`d-flex justify-content-between flex-row p-1`} id={f.id}>
            {/* <div style={{ width: "20px", height: "20px", backgroundColor: f.color, borderRadius: "50%" }} /> */}
            <p className={`col-8`}>
              {f.comment}
            </p>
            <div className={`col-3 d-flex flex-column justify-content-between`}>
              <div>
                {renderStars(f.stars)}
              </div>
              <p>Артикул: {f.product}</p>
            </div>
          </Card>
        ))}
      </CardBody>
    </Card>
  )
};

export default StoreFeedbacks;
