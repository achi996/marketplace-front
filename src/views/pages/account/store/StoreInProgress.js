import React from "react";

export default function StoreInProgress(){

  return (
    <div style={{minHeight: "500px", backgroundColor: "#C4C4C4", fontSize: "24px"}} className={`d-flex p-5 text-center align-items-center justify-content-center`}>
      Ваша заявка принята, после модерации у вас будет доступ к адмнистрированию своего кабинета
    </div>
  )
}
