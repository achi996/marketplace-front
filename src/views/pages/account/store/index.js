import React, {useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import { Redirect } from 'react-router-dom';
import { getMyStore } from '../../../../redux/slices/store/myStore.slice';
import StoreInProgress from "./StoreInProgress";

const AccountStore = () => {
  const { data: store, loading, isActive } = useSelector(state => state.store.myStore);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMyStore());
  }, [dispatch]);

  if (loading) return null;
  if (store && !isActive) return <StoreInProgress/>;
  return <Redirect to="/account/store/info"/>;
}

export default (props) => {
  const {store} = useSelector(state => state.auth);
  return <AccountStore store={store} {...props} />
};