import React from 'react'
import {useSelector} from 'react-redux'
import {Redirect} from 'react-router-dom';
import {Card, CardBody, Col, FormGroup, Input, Label, Row} from 'reactstrap';

const StoreInfo = () => {
  const {data: store, loading, error, isActive} = useSelector(state => state.store.myStore);
  if (loading) return null;
  if (error || (store && !isActive)) return <Redirect to="/account/store"/>;
  return (
    <Card className="mb-1">
      <CardBody>
        <h4>Мой магазин {JSON.stringify({store, isActive})}</h4>
        <Row>

          <Col xs={12}>
            <FormGroup>
              <Label for="owner_full_name">Название</Label>
              <Input value={store?.name} readOnly/>
            </FormGroup>
          </Col>

          <Col xs={12}>
            <FormGroup>
              <Label for="owner_full_name">Описание</Label>
              <Input value={store?.description} readOnly/>
            </FormGroup>
          </Col>

          <Col xs={12}>
            <FormGroup>
              <Label for="owner_full_name">Тип продажи</Label>
              <Input
                value={`${store?.retail ? "Оптом" : ""}${(store?.retail && store?.wholesale) ? " и " : ""}${store?.wholesale ? "В розницу" : ""}`}
                readOnly/>
            </FormGroup>
          </Col>

          <Col xs={12}>
            <FormGroup>
              <p>Логотип</p>
              <img width="200px" src={store?.logo} alt="Store Patent"/>
            </FormGroup>
          </Col>
          <Col xs={12}>
            <FormGroup>
              <p>Патент</p>
              <img width="200px" src={store?.patent} alt="Store Patent"/>
            </FormGroup>
          </Col>
        </Row>
      </CardBody>
    </Card>
  )
}

export default StoreInfo
