import React, {useCallback, useEffect, useState} from 'react'
import {useFormik} from "formik";
import * as Yup from "yup";
import {Button, Card, CardBody, Col, Form, FormGroup, Input, Label, Row} from "reactstrap";
import Select from "react-select";
import "../../../../assets/scss/plugins/forms/react-select/_react-select.scss"
import "../../../../assets/scss/pages/auth-store.scss"
import CustomDragNDrop from "../../../../components/custom/CustomDragNDrop/DragNDrop";
import {useDispatch, useSelector} from 'react-redux';
import {changeMyStore, createMyStore} from '../../../../redux/slices/store/myStore.slice';
import CheckBoxesVuexy from '../../../../components/@vuexy/checkbox/CheckboxesVuexy';
import {Check} from 'react-feather';
import ReactInputMask from 'react-input-mask';
import Axios from 'axios';
import { API_BASE_URL } from '../../../../helpers/constants';
import { Redirect } from 'react-router';

const RequestSchema = Yup.object().shape({
  name: Yup.string()
    .required("Заполните это поле!"),
  description: Yup.string()
    .required("Заполните это поле!"),
  inn: Yup.string()
    .required("Заполните это поле!"),
  logo: Yup.mixed()
    .required("Загрузите логотип!"),
  patent: Yup.mixed()
    .required("Загрузите патент!"),
  owner_full_name: Yup.string(),
  phone_number: Yup.string()
    .required("Заполните это поле!"),
});

// function getSaleType(type) {
//   return type === "R" ?
//     { value: "R", label: "Розницу" } :
//     { value: "O", label: "Оптом" }
// }

// const typesOfSale = [
//   { value: "O", label: "Оптом" },
//   { value: "R", label: "Розницу" },
// ]

function getOwnerType(type) {
  return type === "J" ?
    {value: "J", label: "Юридическое лицо"} :
    {value: "I", label: "Физическое лицо"}
}

const owner_types = [
  {value: "J", label: "Юридическое лицо"},
  {value: "I", label: "Физическое лицо"}
]

function createSrc(file) {
  return new Promise((res) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.addEventListener("load", () => {
      res(reader.result);
    });
  });
};

const fetchAgreementDocUrl = async (initalUrl)=>{
  try {
    const { data: { results: [ { file } ] } } = await Axios.get(`${API_BASE_URL}/store_settings/agreement/`);
    return file;
  } catch (error) {
    return initalUrl;
  }
};

const useFetchAgreementDocUrl = (initalUrl)=>{
  const [url, setUrl] = useState(initalUrl);
  useEffect(()=>{
    fetchAgreementDocUrl(initalUrl).then(setUrl);
  }, [initalUrl])
  return url;
}

function StoreApplication() {
  const [logoFile, setLogoFile] = useState(null)
  const [logoSrc, setLogoSrc] = useState(null)
  const [patentSrc, setPatentSrc] = useState(null)
  const [patentFile, setPatentFile] = useState(null)

  const agreementDocUrl = useFetchAgreementDocUrl("/documents/public-offerta.docx");

  const dispatch = useDispatch();
  const {data, loading, isActive} = useSelector(state => state.store.myStore);

  const formik = useFormik({
    initialValues: {
      name: data?.name || "",
      description: data?.description || "",
      inn: data?.inn || "",
      owner_type: (data?.owner_type && getOwnerType(data.owner_type)) || null,
      logo: data?.logo || null,
      patent: data?.logo || null,
      owner_full_name: data?.owner_full_name || "",
      retail: data?.retail || false,
      wholesale: data?.wholesale || false,
      phone_number: data?.phone_number || "",
      address: data?.address || "",
    },
    onSubmit: async (values) => {
      const formData = new FormData();

      if (values.name) formData.append("name", values.name);
      if (values.description) formData.append("description", values.description);
      if (logoFile) formData.append("logo", logoFile, logoFile.name)
      if (values.owner_type) formData.append("owner_type", values.owner_type.value);
      if (values.shop_type) formData.append("shop_type", values.shop_type.value);
      if (values.inn) formData.append("inn", values.inn);
      if (values.owner_full_name) formData.append("owner_full_name", values.owner_full_name);
      if (patentFile) formData.append("patent", patentFile, patentFile.name);
      if (values.phone_number) formData.append("phone_number", values.phone_number);
      if (values.address) formData.append("address", values.address);
      formData.append("wholesale", values.wholesale);
      formData.append("retail", values.retail);
      if (data) {
        dispatch(changeMyStore({formData: formData, id: data.id}))
      } else {
        dispatch(createMyStore(formData));
      }
    },
    validationSchema: RequestSchema
  });

  const {
    setFieldValue,
  } = formik;

  const handleLogoSave = useCallback(async () => {
    if (logoFile) {
      setFieldValue("logo", logoFile)
      const res = await createSrc(logoFile);
      setLogoSrc(res);
    }
  }, [logoFile, setFieldValue]);

  const handlePatentSave = useCallback(async () => {
    if (patentFile) {
      setFieldValue("patent", patentFile)
      const res = await createSrc(patentFile);
      setPatentSrc(res);
    }
  }, [patentFile, setFieldValue]);

  useEffect(() => {
    handleLogoSave();
  }, [handleLogoSave]);

  useEffect(() => {
    handlePatentSave();
  }, [handlePatentSave]);

  if(data && !isActive) return <Redirect to="/account/store"/>;

  return loading ? null : (
    <Card className="mb-1">
      <CardBody>
        <Form onSubmit={formik.handleSubmit} className="mt-2">
          <h4>{data ? "Мой магазин" : "Оставьте заявку"}</h4>
          <Row>
            <Col sm="12">
              <FormGroup>
                <Label for="name">Название</Label>
                <Input
                  id="name"
                  name="name"
                  disabled={data && !isActive}
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.touched["name"] && formik.errors["name"] && (
                  <p className="text-danger">{formik.errors["name"]}</p>
                )}
              </FormGroup>
            </Col>
            <Col sm="12">
              <FormGroup>
                <Label for="description">Описание</Label>
                <Input
                  id="description"
                  name="description"
                  type="textarea"
                  disabled={data && !isActive}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.description}
                />
                {formik.touched["description"] && formik.errors["description"] && (
                  <p className="text-danger">{formik.errors["description"]}</p>
                )}
              </FormGroup>
            </Col>

            <Col sm="12">
              <FormGroup>
                <Label for="inn">ИНН</Label>
                <ReactInputMask
                  id="inn"
                  className="form-control"
                  mask="99999999999999"
                  name="inn"
                  disabled={data && !isActive}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.inn}
                />
                {formik.touched["inn"] && formik.errors["inn"] && (
                  <p className="text-danger">{formik.errors["inn"]}</p>
                )}
              </FormGroup>
            </Col>

            <Col sm="12">
              <FormGroup>
                <Label for="owner_full_name">Имя владельца</Label>
                <Input
                  id="owner_full_name"
                  name="owner_full_name"
                  disabled={data && !isActive}
                  value={formik.values.owner_full_name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.touched["owner_full_name"] && formik.errors["owner_full_name"] && (
                  <p className="text-danger">{formik.errors["owner_full_name"]}</p>
                )}
              </FormGroup>
            </Col>

            <Col sm="12">
              <FormGroup>
                <Label for="phone_number">Номер телефона</Label>
                <ReactInputMask
                  id="phone_number"
                  className="form-control"
                  mask="+\9\96999999999"
                  disabled={data && !isActive}
                  name="phone_number"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.phone_number}
                />
                {formik.touched["phone_number"] && formik.errors["phone_number"] && (
                  <p className="text-danger">{formik.errors["phone_number"]}</p>
                )}
              </FormGroup>
            </Col>

            <Col sm="12">
              <FormGroup>
                <Label for="address">Адрес</Label>
                <Input
                  id="address"
                  name="address"
                  type="textarea"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.address}
                />
                {formik.touched["address"] && formik.errors["address"] && (
                  <p className="text-danger">{formik.errors["address"]}</p>
                )}
              </FormGroup>
            </Col>

            <Col sm={12}>
              <FormGroup>
                <Label for="owner_type">Тип пользователя</Label>
                <Select
                  classNamePrefix="select"
                  value={formik.values.owner_type}
                  name="owner_type"
                  isDisabled={data && !isActive}
                  placeholder={"Тип пользователя"}
                  onBlur={formik.handleBlur}
                  onChange={e => formik.setFieldValue("owner_type", e)}
                  options={owner_types}
                />
                {formik.touched["owner_type"] && formik.errors["owner_type"] && (
                  <p className="text-danger">{formik.errors["owner_type"]}</p>
                )}
              </FormGroup>
            </Col>

            <Col xs="12">
              <FormGroup>
                <Label for="quantity-123as-asd">Тип продажи</Label>
                <div className="d-flex align-items-center">
                  <CheckBoxesVuexy
                    color="primary"
                    icon={<Check className="vx-icon" size={16}/>}
                    label="Оптом"
                    disabled={data && !isActive}
                    className={`mr-3`}
                    checked={formik.values.wholesale}
                    onChange={(e) => formik.setFieldValue("wholesale", e.target.checked)}
                  />
                  <CheckBoxesVuexy
                    color="primary"
                    icon={<Check className="vx-icon" size={16}/>}
                    label="В розницу"
                    disabled={data && !isActive}
                    checked={formik.values.retail}
                    onChange={(e) => formik.setFieldValue("retail", e.target.checked)}
                  />
                </div>
              </FormGroup>
            </Col>
            <Col
              sm={12}
              style={{gridTemplate: "1fr / 1fr"}}
              className={`pb-2 pt-2 auth-store_img_uploader`}
            >
              <FormGroup className={`full-width`}>
                <Label for="logo">Логотип</Label>
                <div className={`d-flex`}>
                  {(data?.logo || logoSrc) && <img src={logoSrc || data.logo} alt=""/>}
                  <CustomDragNDrop
                    disabled={data && !isActive}
                    onActiveText={"Перетащите изображение для магазина ..."}
                    text={"Перетащите или выберите изображение"}
                    setState={setLogoFile}
                  />
                </div>
                {formik.touched["logo"] && formik.errors["logo"] && (
                  <p className="text-danger">{formik.errors["logo"]}</p>
                )}
              </FormGroup>
            </Col>

            <Col
              sm={12}
              style={{gridTemplate: "1fr / 1fr"}}
              className={`pb-2 pt-2 auth-store_img_uploader`}
            >
              <FormGroup>
                <Label for="patent">Патент / Свидетельство о регистрации</Label>
                <div className={`d-flex`}>
                  {(data?.patent || patentSrc) && <img src={patentSrc || data.patent} alt=""/>}
                  <CustomDragNDrop
                    disabled={data && !isActive}
                    onActiveText={"Перетащите изображение патента магазина ..."}
                    text={"Перетащите или выберите изображение"}
                    setState={setPatentFile}
                  />
                </div>
                {formik.touched["patent"] && formik.errors["patent"] && (
                  <p className="text-danger">{formik.errors["patent"]}</p>
                )}
              </FormGroup>
            </Col>

            {!data &&
            <Col sm={12}>
              <FormGroup>
                <Label>
                  Вы соглашаетесь с нашим <br/>
                  <a href={agreementDocUrl} rel="noopener noreferrer" target="_blank"> договором на создание магазина </a>
                </Label>
                <CheckBoxesVuexy
                  disabled={data && !isActive}
                  color="primary"
                  icon={<Check className="vx-icon" size={16}/>}
                  label="Согласен"
                  name="is_confirmed"
                  checked={formik.values.is_confirmed}
                  onChange={(e) => formik.setFieldValue("is_confirmed", e.target.checked)}
                  required
                />
              </FormGroup>
            </Col>
            }

            <Col xs="12">
              <Button
                disabled={data && !isActive}
                className="w-100"
                type="submit"
                color="primary"
              >
                { data ? "Сохранить" : !isActive ? "Сохранить" : "Дождитесь до подтверждения магазина" }
              </Button>
            </Col>
            {/* <pre>{JSON.stringify(formik.values, null, 2)}</pre> */}
          </Row>
        </Form>
      </CardBody>
    </Card>
  )
}

export default StoreApplication;
