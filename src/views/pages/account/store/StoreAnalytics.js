import React from "react"
import { Row, Col } from "reactstrap"
import SubscribersGained from "../../../ui-elements/cards/statistics/SubscriberGained"
import RevenueGenerated from "../../../ui-elements/cards/statistics/RevenueGenerated"
import QuaterlySales from "../../../ui-elements/cards/statistics/QuaterlySales"
import OrdersReceived from "../../../ui-elements/cards/statistics/OrdersReceived"
import RevenueChart from "../../../ui-elements/cards/analytics/Revenue"
import ClientRetention from "../../../ui-elements/cards/analytics/ClientRetention"
import SessionByDevice from "../../../ui-elements/cards/analytics/SessionByDevice"
import CustomersChart from "../../../ui-elements/cards/analytics/Customers"

import "../../../../assets/scss/plugins/charts/apex-charts.scss"
import { useSelector } from "react-redux"
import { Redirect } from "react-router-dom"

let $primary = "#7367F0",
  // $success = "#28C76F",
  $danger = "#EA5455",
  $warning = "#FF9F43",
  $primary_light = "#9c8cfc",
  $warning_light = "#FFC085",
  $danger_light = "#f29292",
  $stroke_color = "#b9c3cd",
  $label_color = "#e7eef7"

const StoreAnalytics = () => {
  const { data: store, loading, error } = useSelector(state => state.store.myStore);
  if (loading) return null;
  if (error || (store && !store?.is_active)) return <Redirect to="/account/store" />;
  return (
    <React.Fragment>
      <Row className="match-height">
        <Col md="6" sm="6">
          <SubscribersGained />
        </Col>
        <Col md="6" sm="6">
          <RevenueGenerated />
        </Col>
        <Col md="6" sm="6">
          <QuaterlySales />
        </Col>
        <Col md="6" sm="6">
          <OrdersReceived />
        </Col>
      </Row>
      <br />
      <br />
      <br />
      <Row>
        <Col lg="6" md="12">
          <SessionByDevice
            primary={$primary}
            warning={$warning}
            danger={$danger}
            primaryLight={$primary_light}
            warningLight={$warning_light}
            dangerLight={$danger_light}
          />
        </Col>
        <Col lg="6" md="12" className="text-center align-middle">
          <CustomersChart
            primary={$primary}
            warning={$warning}
            danger={$danger}
            primaryLight={$primary_light}
            warningLight={$warning_light}
            dangerLight={$danger_light}
          />
        </Col>
      </Row>

      <RevenueChart
        primary={$primary}
        dangerLight={$danger_light}
        strokeColor={$stroke_color}
        labelColor={$label_color}
      />
      <Row className="match-height">
        <Col>
          <ClientRetention
            strokeColor={$stroke_color}
            primary={$primary}
            danger={$danger}
            labelColor={$label_color}
          />
        </Col>
      </Row>
    </React.Fragment>
  )
}

export default StoreAnalytics
