import React, {useEffect, useState} from "react";
import {Card, CardBody} from "reactstrap";
import {Check, Edit2, Eye, X} from "react-feather";
import {useSelector} from "react-redux";
import {NavLink, Redirect} from "react-router-dom";
import {API_BASE_URL} from "../../../../../helpers/constants";
import StoreOrdersActions from "./StoreOrdersActions";
import DataTable from "react-data-table-component"
import authAxios from "../../../../../helpers/utils/authAxios";
import SpinnerComponent from "../../../../../components/@vuexy/spinner/Fallback-spinner";
import {history} from "../../../../../history";
import OrderUpdateModal from "./OrderUpdateModal";

function sec2time(date) {
  const readyDate = new Date(date)
  const pad = function (num, size) {
      return ('000' + num).slice(size * -1);
    },
    time = parseFloat(readyDate.getSeconds()).toFixed(3),
    hours = Math.floor(time / 60 / 60),
    minutes = Math.floor(time / 60) % 60,
    seconds = Math.floor(time - minutes * 60),
    _date = readyDate.getDate() < 10 ? `0${readyDate.getDate()}` : readyDate.getDate(),
    month = (readyDate.getMonth() + 1) < 10 ? `0${readyDate.getMonth() + 1}` : readyDate.getMonth() + 1,
    year = readyDate.getFullYear();
  return `${_date}.${month}.${year} ${pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2)}`;
}

async function fetchOrders(query) {
  try {
    const {data} = await authAxios.get(`${API_BASE_URL}/accounts/my_orders/${query}`);
    return data.results
  } catch (error) {
    console.log(error)
  }
}

function createTableOrders(orders, setOrderModal) {
  return orders.map(order => {
    return {
      id: order.id,
      order_id: <NavLink to={`/account/store/invoice/${order.order_id}`} exact>{order.order_id}</NavLink>,
      payment_type: order.payment_type === "CASH" ? 'Наличные' : 'PAYBOX',
      phone_number: order.phone_number,
      payed: order.paid ? <Check/> : <X/>,
      delivered: order.delivered ? <Check/> : <X/>,
      created: sec2time(order.created),
      self_delivered: order.self_delivered ? <Check/> : <X/>,
      self_deliver_address_name: order.self_deliver_address_name ? order.self_deliver_address_name : <X/>,
      country: order.country,
      city: order.city,
      street: order.street,
      house_number: order.house_number,
      actions: <div className={`d-flex align-items-center justify-content-between`} style={{gridGap: 10}}>
        <Eye className={`cursor-pointer`} onClick={() => history.push(`/account/store/invoice/${order.order_id}`)}
             size={18}/>
        <Edit2 onClick={() => setOrderModal(order)} className={`cursor-pointer`} size={18}/>
      </div>
    }
  });
};

const StoreOrders = () => {
  const [orders, setOrders] = useState([])
  const [orderModal, setOrderModal] = useState(null)
  const {data: store, loading, error} = useSelector(state => state.store.myStore);
  const {paid, delivered, search, canceled} = useSelector(state => state.query);

  useEffect(() => {
    fetchOrders(`?search=${search}&paid=${paid}&delivered=${delivered}&canceled=${canceled}`).then(setOrders)
  }, [paid, delivered, search, canceled])
  if (error || (store && !store?.is_active)) return <Redirect to="/account/store"/>;

  return (
    <Card className="mb-1">
      {loading ? <SpinnerComponent className={'height-200'}/> :
        <CardBody>
          <StoreOrdersActions orders={orders}/>
          {!!orders?.length && <DataTable
            data={createTableOrders(orders, setOrderModal)}
            columns={columns}
            noHeader
            // selectableRows
            // selectableRowsComponent={Checkbox}
            selectableRowsComponentProps={{
              color: "primary",
              icon: <Check className="vx-icon" size={12}/>,
              label: "",
              size: "sm"
            }}
          />}
        </CardBody>
      }
      <OrderUpdateModal order={orderModal} setState={setOrderModal}/>
    </Card>
  )
};

export default StoreOrders;

const columns = [
  {
    name: "ID заказа",
    selector: "order_id",
    sortable: false
  },
  {
    name: "Вариант оплаты",
    selector: "payment_type",
    sortable: true
  },
  {
    name: "Номер телефона",
    selector: "phone_number",
    sortable: true
  },
  {
    name: "Оплачено",
    selector: "payed",
    sortable: false
  },
  {
    name: "Доставлено",
    selector: "delivered",
    sortable: false
  },
  {
    name: "Создан",
    selector: "created",
    sortable: true
  },
  {
    name: "Самовывоз",
    selector: "self_delivered",
    sortable: false
  },
  {
    name: "Адрес самовывоза",
    selector: "self_deliver_address_name",
    sortable: true
  },
  {
    name: "Страна",
    selector: "country",
    sortable: true
  },
  {
    name: "Город/село",
    selector: "city",
    sortable: true
  },
  {
    name: "Улица",
    selector: "street",
    sortable: true
  },
  {
    name: "Номер дома/квартиры",
    selector: "house_number",
    sortable: true
  },
  {
    name: "Действия",
    selector: "actions",
    sortable: false
  }
]
