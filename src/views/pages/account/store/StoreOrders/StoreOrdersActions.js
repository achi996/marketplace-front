import React from "react";
import Select from "react-select";
import {Button, Input} from "reactstrap";
import {setQueryParam} from "../../../../../redux/slices/query.slice";
import {useDispatch} from "react-redux";
import array2csv from "../../../../../helpers/utils/array2csv";
import { Printer } from "react-feather";

const statusOptions = [
  {value: "cancel", label: "Все"},
  {value: "delivered", label: "Доставлено"},
  {value: "paid", label: "Оплачено"},
  {value: "canceled", label: "Отменен"},
];


function formatOrdersDataToCSV (orders){
  const res = orders.map(({shops, items, ...order})=>({
    ...order,
    shops: shops.join(","),
    items: items.map(item=>`${item.product.slug} * ${item.quantity}`).join(",")
  }))
  console.log(res);
  return res;
}

const downloadCSV = (orders)=>{
  const data = formatOrdersDataToCSV(orders);
  const { url } = array2csv(data);
  console.log(url);
  var element = document.createElement('a');
  element.setAttribute('href', url);
  element.setAttribute('download', `my-orders.csv`);
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

export default function StoreOrdersActions({orders}) {
  const dispatch = useDispatch();

  function onChangeStatus(key) {
    const options = ['delivered', 'paid', 'canceled']
    key !== "cancel" ?
      options.forEach(option => {
        option === key ? dispatch(setQueryParam({key, value: true}))
          : dispatch(setQueryParam({key: option, value: false}))
      }) : options.forEach(option => dispatch(setQueryParam({key: option, value: false})))
  }


  return (
    <div className={`d-flex flex-column mb-1`} style={{gridGap: "5px"}}>
      <Input placeholder={"Поиск"} onChange={e => dispatch(setQueryParam({key: "search", value: e.target.value}))}/>
      <div className={`d-flex align-items-center justify-content-between flex-wrap`} style={{gridGap: "5px"}}>
        <div className={`d-flex align-items-center col-lg-6 col-12 p-0`} style={{gridGap: "5px"}}>
          <Select
            className="col-6 p-0"
            classNamePrefix="select"
            placeholder={"Статус"}
            name="status"
            onChange={value => onChangeStatus(value.value)}
            options={statusOptions}
          />
        </div>
        <Button onClick={()=>downloadCSV(orders)} className={`col-lg-2 col-12 p-1 p-md-0`} color={"primary"} style={{height: 40}}>
          <Printer/>
        </Button>
      </div>
    </div>
  )
}
