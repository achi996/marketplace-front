import React, {useEffect, useState} from "react";
import {Button, FormGroup, Label, Modal, ModalBody, ModalHeader} from "reactstrap";
import {Field, Form, Formik} from "formik";
import * as Yup from "yup";
import CheckBoxesVuexy from "../../../../../components/@vuexy/checkbox/CheckboxesVuexy";
import ReactInputMask from "react-input-mask";
import authAxios from "../../../../../helpers/utils/authAxios";
import {API_BASE_URL} from "../../../../../helpers/constants";

export default function OrderUpdateModal({order, setState}) {
  const [phoneValid, setPhoneValid] = useState(false)
  const formSchema = Yup.object().shape({
    phone_number: Yup.string().required("Заполните это поле!"),
  })
  const initialState = {
    country: order?.country || "",
    city: order?.city || "",
    street: order?.street || "",
    house_number: order?.house_number || "",
    paid: order?.paid || false,
    delivered: order?.delivered || false,
    phone_number: order?.phone_number || "",
    self_delivered: order?.self_delivered || false,
    self_deliver_address_name: order?.self_deliver_address_name || "",
  }

  async function updateReq(values) {
    const formData = new FormData();
    await Object.keys(values).forEach(key => {
      if (order[key] !== values[key]) {
        if (key === "self_delivered" && !values.self_delivered) {
          formData.append("self_deliver_address_name", '')
        } else formData.append(key, values[key])
      }
    })
    authAxios.patch(`${API_BASE_URL}/accounts/my_orders/${order.order_id}/`, formData)
      .then(res => console.log(res))
  }

  function handleChangePhone(e, setFieldValue) {
    setFieldValue("phone_number", e.target.value)
    e.target.value.includes('_') ? setPhoneValid(false) : setPhoneValid(true)
  }

  useEffect(() => {
    (order?.phone_number.length < 13 || order?.phone_number.includes("_")) ? setPhoneValid(false) : setPhoneValid(true)
  }, [order])

  return (
    <Modal
      isOpen={order}
      className="modal-dialog-centered"
    >
      <ModalHeader toggle={() => setState(null)}>
        Редактирование заказа
      </ModalHeader>
      <ModalBody>
        <Formik
          initialValues={initialState}
          validationSchema={formSchema}
          onSubmit={values => updateReq(values)}
        >
          {({errors, touched, values, setFieldValue, handleBlur, handleChange}) => (
            <Form>
              <FormGroup className="my-1">
                <Label for="country">Страна</Label>
                <Field
                  name="country"
                  id="country"
                  className={`form-control ${errors.country &&
                  touched.country &&
                  "is-invalid"}`}
                />
                {errors.country && touched.country ? (
                  <div className="invalid-tooltip mt-1">{errors.country}</div>
                ) : null}
              </FormGroup>
              <FormGroup className="my-1">
                <Label for="city">Город</Label>
                <Field
                  type="city"
                  name="city"
                  id="city"
                  className={`form-control ${errors.city &&
                  touched.city &&
                  "is-invalid"}`}
                />
                {errors.city && touched.city ? (
                  <div className="invalid-tooltip mt-1">{errors.city}</div>
                ) : null}
              </FormGroup>
              <FormGroup className="my-1">
                <Label for="street">Улица</Label>
                <Field
                  name="street"
                  id="street"
                  className={`form-control ${errors.street &&
                  touched.street &&
                  "is-invalid"}`}
                />
                {errors.street && touched.street ? (
                  <div className="invalid-tooltip mt-1">{errors.street}</div>
                ) : null}
              </FormGroup>
              <FormGroup className="my-1">
                <Label for="house_number">Номер дома</Label>
                <Field
                  name="house_number"
                  id="house_number"
                  className={`form-control ${errors.house_number &&
                  touched.house_number &&
                  "is-invalid"}`}
                />
                {errors.house_number && touched.house_number ? (
                  <div className="invalid-tooltip mt-1">{errors.house_number}</div>
                ) : null}
              </FormGroup>
              <FormGroup className="my-1">
                <Label for="phone_number">
                  Номер телефона
                </Label>
                <ReactInputMask
                  id="phone_number"
                  className="form-control"
                  mask="+\9\96999999999"
                  name="phone_number"
                  onChange={e => handleChangePhone(e, setFieldValue)}
                  onBlur={handleBlur}
                  value={values.phone_number}
                />
                {!phoneValid ? <div className="m-0 danger">Заполните поле</div> : null}
              </FormGroup>
              <div className={`d-flex my-1`} style={{gridGap: 5}}>
                <FormGroup className="col-4 p-0 m-0">
                  <Label for="paid">Оплачено</Label>
                  <CheckBoxesVuexy
                    type="checkbox"
                    name="paid"
                    id="paid"
                    checked={values.paid}
                    onChange={handleChange}
                    className={`form-control ${errors.paid &&
                    touched.paid &&
                    "is-invalid"}`}
                  />
                  {errors.paid && touched.paid ? (
                    <div className="invalid-tooltip mt-1">{errors.paid}</div>
                  ) : null}
                </FormGroup>
                <FormGroup className="col-4 p-0 m-0">
                  <Label for="delivered">
                    Доставлено
                  </Label>
                  <CheckBoxesVuexy
                    name="delivered"
                    id="delivered"
                    checked={values.delivered}
                    onChange={handleChange}
                    className={`form-control ${errors.delivered &&
                    touched.delivered &&
                    "is-invalid"}`}
                  />
                  {errors.delivered && touched.delivered ? (
                    <div className="invalid-tooltip mt-1">{errors.delivered}</div>
                  ) : null}
                </FormGroup>
                <FormGroup className="col-4 p-0 m-0">
                  <Label for="self_delivered">
                    Самовывоз
                  </Label>
                  <CheckBoxesVuexy
                    name="self_delivered"
                    id="self_delivered"
                    checked={values.self_delivered}
                    onChange={handleChange}
                    className={`form-control ${errors.self_delivered &&
                    touched.self_delivered &&
                    "is-invalid"}`}
                  />
                  {errors.self_delivered && touched.self_delivered ? (
                    <div className="invalid-tooltip mt-1">{errors.self_delivered}</div>
                  ) : null}
                </FormGroup>
              </div>
              <FormGroup className="my-1">
                <Label for="self_deliver_address_name">Адрес самовывоза</Label>
                <Field
                  name="self_deliver_address_name"
                  id="self_deliver_address_name"
                  disabled={!values.self_delivered}
                  className={`form-control ${errors.self_deliver_address_name &&
                  touched.self_deliver_address_name &&
                  "is-invalid"}`}
                />
                {errors.self_deliver_address_name && touched.self_deliver_address_name ? (
                  <div className="invalid-tooltip mt-1">{errors.self_deliver_address_name}</div>
                ) : null}
              </FormGroup>
              <Button.Ripple disabled={!phoneValid} color="primary" type="submit">
                Сохранить
              </Button.Ripple>
            </Form>
          )}
        </Formik>
      </ModalBody>
    </Modal>
  )
}
