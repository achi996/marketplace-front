import {Card, CardBody, Col, Row, Table} from "reactstrap";
import {getPrices} from "../../../../../helpers/utils/productPrices";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchAddresses} from "../../../../../redux/slices/cart.slice";

const PAYMENT_METHODS = {
  "CASH": "Наличные",
  "PAYBOX": "Безналичные",
};

export default function PdfDocument({invoice, user}) {

  const addresses = useSelector(state => state.cart.self_deliver_addresses_list);
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchAddresses());
  }, [dispatch]);

  const selfDeliveredAddress = addresses.find(item => item.id === invoice.self_deliver_address)?.address;

  const address = invoice.self_delivered && selfDeliveredAddress ?
    selfDeliveredAddress :
    [invoice.country, invoice.oblast, invoice.city, invoice.street, invoice.house_number].join(", ");

  return (
    <Card className="mb-1 invoice-page">
      <CardBody>
        <Row className={`d-flex justify-content-end`}>
          <Col md="5" sm="12" className="text-right">
            <div className="invoice-details mt-2">
              <h1>Детали заказа</h1>
              <h6>№{invoice.order_id}</h6>
            </div>
            <div className="invoice-details mt-2">
              <div className={`d-flex align-items-center flex-wrap mt-1`}>
                <h6 className="m-0 mr-1">Дата поступления:</h6>
                <p className={`m-0`}>{new Date(invoice.created).toLocaleString()}</p>
              </div>
              <div className={`d-flex align-items-center flex-wrap mt-1`}>
                <h6 className="m-0 mr-1">Самовывоз:</h6>
                <p className={`m-0`}>{invoice.self_delivered ? "Да" : "Нет"}</p>
              </div>
              <div className={`d-flex align-items-center flex-wrap mt-1`}>
                <h6 className="m-0 mr-1">Вариант оплаты:</h6>
                <p className={`m-0`}>{PAYMENT_METHODS[invoice.payment_type]}</p>
              </div>
            </div>
          </Col>
        </Row>
        <Row className="pt-2">
          <Col sm="12">
            <div className="recipient-info my-2">
              <div className={`d-flex align-items-center flex-wrap mt-1`}>
                <h6 className={`m-0 mr-1`}>Адрес доставки:</h6>
                <p className={`m-0`}>
                  {address}
                </p>
              </div>
              <div className={`d-flex align-items-center flex-wrap mt-1`}>
                <h6 className={`m-0 mr-1`}>Получатель:</h6>
                <p className={`m-0`}>
                  {invoice?.user_info || `${user?.last_name}  ${user?.first_name}`}
                </p>
              </div>
              <div className={`d-flex align-items-center flex-wrap mt-1`}>
                <h6 className={`m-0 mr-1`}>Номер телефона:</h6>
                <p className={`m-0`}>
                  {invoice?.phone_number}
                </p>
              </div>
              {/*{!!invoice.user &&*/}
              {/*<div className={`d-flex align-items-center flex-wrap mt-1`}>*/}
              {/*  <h6 className={`m-0 mr-1`}>Почтовый адрес:</h6>*/}
              {/*  <p className={`m-0`}>*/}
              {/*    {user?.email}*/}
              {/*  </p>*/}
              {/*</div>*/}
              {/*}*/}
              <div className={`d-flex align-items-center flex-wrap mt-1`}>
                <h6 className={`m-0 mr-1`}>Комментарии к заказу:</h6>
                <p className={`m-0`}>
                  {invoice?.comment}
                </p>
              </div>
            </div>
          </Col>
        </Row>
        <div className="invoice-items-table pt-1">
          <Row>
            <Col sm="12">
              <Table responsive borderless>
                <thead>
                <tr>
                  <th>Список товаров</th>
                  <th>Характеристики</th>
                  <th>Цена (сом)</th>
                  <th>Описание</th>
                  <th>Количество(шт)</th>
                  <th>Итого(сом)</th>
                </tr>
                </thead>
                <tbody>
                {
                  invoice.items.map(item => {
                    const prices = getPrices(item.product)
                    const pr_spec = item.product_specifications;
                    return (
                      <tr key={item.id}>
                        <td>{item.product.title}</td>
                        <td>
                          <ul className={`p-0 m-0`} style={{listStyle: "none", fontSize: '.8em', lineHeight: ".85em"}}>
                            {
                              pr_spec && Object.keys(pr_spec).length ? Object.keys(pr_spec).map(
                                key => {
                                  const spec = pr_spec[key];
                                  return <li style={{marginBottom: 10}} key={`${spec.id}-invoice-product-spec`}>
                                    <b>{spec.name}</b>{`: ${spec.value.name}`}</li>
                                }
                              ) : "Характеристики отсутствуют"
                            }
                          </ul>
                        </td>
                        <td>{prices.viewPrice} сом</td>
                        <td style={{
                          listStyle: "none",
                          fontSize: '.9em',
                          lineHeight: ".95em"
                        }}>{item.product.description}</td>
                        <td>{item.quantity}</td>
                        <td>{prices.viewPrice * item.quantity} сом</td>
                      </tr>
                    )
                  })
                }
                </tbody>
              </Table>
            </Col>
          </Row>
        </div>
        <div className="invoice-total-table">
          <Row>
            <Col
              sm={{size: 7, offset: 5}}
              xs={{size: 7, offset: 5}}
            >
              <Table responsive borderless>
                <tbody>
                <tr>
                  <th>Итого</th>
                  <td>{invoice.total_cost} сом</td>
                </tr>
                </tbody>
              </Table>
            </Col>
          </Row>
        </div>
      </CardBody>
    </Card>
  )
}
