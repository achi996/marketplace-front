import React, {useEffect, useRef, useState} from "react"
import {Button, Col, Row} from "reactstrap"
import {Download} from "react-feather"
import "../../../../../assets/scss/pages/invoice.scss"
import {API_BASE_URL} from "../../../../../helpers/constants";
import {Redirect, useRouteMatch} from "react-router-dom";
import authAxios from "../../../../../helpers/utils/authAxios";
import SpinnerComponent from "../../../../../components/@vuexy/spinner/Fallback-spinner";
import PdfDocument from "./PdfDocument";
import {useSelector} from "react-redux";


async function getInvoice(id, orderType) {
  try {
    if(orderType === "user"){
      const {data} = await authAxios.get(`${API_BASE_URL}/accounts/orders/${id}`)
      return {...data, items: data.items || []};
    }else {
      const {data} = await authAxios.get(`${API_BASE_URL}/accounts/my_orders/${id}`)
      return data
    }
  } catch (err) {
    console.log(err)
  }
}

export default function Invoice() {
  const [invoice, setInvoice] = useState(null)
  const {params: { id, orderType }} = useRouteMatch()
  const node = useRef();

  useEffect(() => {
    getInvoice(id, orderType).then(setInvoice)
  }, [id, orderType]);

  const {data: store, isStoreNotFound} = useSelector(state => state.store.myStore);

  const user = useSelector(state => state?.auth?.userData);

  if ((isStoreNotFound || (store && !store?.is_active)) && orderType !== "user") return <Redirect to="/account/store"/>;

  return invoice ? (
    <React.Fragment>
      <Row>
        <Col
          className="d-flex flex-column flex-md-row justify-content-end invoice-header mb-1 noprint"
          sm="12"
        >
          <Button.Ripple onClick={()=>window.print()} color="primary" outline>
            <Download size="15"/>
            <span className="align-middle ml-50">Скачать файл</span>
          </Button.Ripple>
        </Col>
        <Col className="invoice-wrapper" sm="12">
          <div ref={node}>
            <PdfDocument invoice={invoice} user={user}/>
          </div>
        </Col>
      </Row>
    </React.Fragment>
  ) : <SpinnerComponent/>
}
