import React, { useRef } from 'react'
import * as Icon from 'react-feather'
import { useDispatch } from 'react-redux';
import { Card, CardBody, Row } from 'reactstrap'
import { addImageToProduct, removeProductFormImage } from '../../../../../redux/slices/store/myShop.slice';

function ProductFormImageItem({ image }){
  const dispatch = useDispatch();
  const handleRemove = ()=>{
    dispatch(removeProductFormImage(image.id));
  };
  return (
    <Card className="mb-1 border m-1 position-relative">
      <CardBody>
        <Icon.XSquare
          className="cursor-pointer position-absolute"
          color="red"
          size={20}
          style={{
            top: 10,
            right: 10
          }}
          onClick={handleRemove}
        />
        <img
          src={image.image}
          alt={image.id + "-product-image"}
          width="100"
          height="100"
        />
      </CardBody>
    </Card>
  )
}

function ProductFormImages({ images=[], productId }) {
  const renderImagesList = (
    <Row className="flex-wrap">
      {images.map(image => (
        <ProductFormImageItem
          key={image.id + "-product-image"}
          image={image}
        />
      ))}
    </Row>
  );
  const inputRef = useRef(null);
  const dispatch = useDispatch();
  const handleChange = (e)=>{
    var files = e.target.files;

    const formData = new FormData();
    formData.append("product", productId);
    formData.append("image", files[0]);
    dispatch(addImageToProduct({
      formData,
    }))
  }
  return (
    <Card className="mb-1">
      <CardBody>
        <h4>Изображения</h4>
        {!images?.length ? (
          <p className="text-danger">Нет изображений</p>
        ): renderImagesList }
        <input
          ref={inputRef}
          type="file"
          placeholder="Hello"
          onChange={handleChange}
        />
      </CardBody>
    </Card>
  )
}

export default ProductFormImages
