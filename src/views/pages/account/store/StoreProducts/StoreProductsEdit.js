import React, { useEffect } from "react";
import { Row, Col } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { fetchMyProducts, myShopSelector } from "../../../../../redux/slices/store/myShop.slice";
import EditProduct from "./EditProduct";

const StoreProductsEdit = (props) => {
  const {id} = props?.match?.params
  const product = useSelector(state => myShopSelector.selectById(state.store.myShop, id));
  const dispatch = useDispatch();

  const myStoreId = useSelector(state => state.store?.myStore?.data?.shop_id)
  
  useEffect(() => {
    myStoreId && dispatch(fetchMyProducts({id: myStoreId}))
  }, [dispatch, myStoreId]);

  const {data: store, loading, error} = useSelector(state => state.store.myStore);
  if (loading || !product) return null;
  if (error || (store && !store?.is_active)) return <Redirect to="/account/store"/>;
  return (
    <Row>
      <Col sm="12">
        <EditProduct
          product_options={product.product_options}
          productId={id}
          initialValues={product}
        />
      </Col>
    </Row>
  )
}

export default StoreProductsEdit
