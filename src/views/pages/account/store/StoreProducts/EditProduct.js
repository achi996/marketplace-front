import React from 'react'
import { useFormik } from "formik";
import * as Yup from "yup";
import { Button, Card, CardBody, Col, Form, FormGroup, Input, Label, Row, } from "reactstrap";
import "../../../../../assets/scss/plugins/forms/react-select/_react-select.scss"
import "../../../../../assets/scss/pages/auth-store.scss"
// import CustomDragNDrop from "../../../../../components/custom/CustomDragNDrop/DragNDrop";
import Select from "react-select";
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import CheckBoxesVuexy from "../../../../../components/@vuexy/checkbox/CheckboxesVuexy";
import { Check } from "react-feather";
import { REGION_CHOICES, updateMyProduct } from "../../../../../redux/slices/store/myShop.slice";
import { history } from '../../../../../history';
import ProductSpecificationsForm from './ProductSpecificationsForm';
import ProductFormImages from './ProductFormImages';
import { productsSelector } from '../../../../../redux/slices/products.slice';

const RequestSchema = Yup.object().shape({
  title: Yup.string()
    .required("Заполните это поле!")
  , description: Yup.string()
    .required("Заполните это поле!")
  , category: Yup.mixed()
    .required("Заполните это поле!")
  , region: Yup.string()
    .oneOf(Object.keys(REGION_CHOICES), `Неверный регион! Регион должен быть одним из: \n - ${Object.values(REGION_CHOICES).join("\n - ")}`)
    .notOneOf([""])
    .required("Заполните это поле!")
});

const regionOptions = Object.entries(REGION_CHOICES).map(([key, value])=>({ value: key, label: value }));

export default function EditProduct({
  initialValues = {
    is_active: false,
    title: "",
    description: "",
    address: "",
    retail: false,
    wholesale: false,
    category: null,
    product_specifications: [],
    product_specifications_values: [],
    product_spec: [],
    amount: 0,
    new_retail_price: 0,
    old_retail_price: 0,
    wholesale_price: 0,
    discount_wholesale_price: 0,
    wholesale_amount: 0,
    region: "",
  },
  productId,
}) {
  const { data: store, loading, error } = useSelector(state => state.store.myStore);
  const product = useSelector(state => productsSelector.selectById(state.store.myShop, productId));
  const dispatch = useDispatch()

  const categories = useSelector(state => {
    return state.categories.flattenCategories
      .map(cat => ({ value: cat.id, label: cat.title, isFixed: false }))
  });

  const formik = useFormik({
    initialValues,
    onSubmit: async (values) => {
      const specValues = [];
      const specs = values.product_spec
        .filter(item => item.isSelected)
        .map(item => {
          item.product_specifications_values
            .filter(item => item.isSelected)
            .forEach(item => specValues.push(item.id));
          return item.id;
        });
      dispatch(updateMyProduct({
        slug: product.slug,
        is_active: values.is_active,
        title: values.title,
        description: values.description,
        address: values.address,
        retail: values.retail,
        wholesale: values.wholesale,
        category: values.category,
        product_specifications: specs,
        product_specifications_values: specValues,
        amount: values.amount,
        new_retail_price: values.retail ? values.new_retail_price : 0,
        old_retail_price: values.retail ? values.old_retail_price || 0 : 0,
        wholesale_price: values.wholesale ? values.wholesale_price : 0,
        discount_wholesale_price: values.wholesale ? values.discount_wholesale_price || 0 : 0,
        wholesale_amount: values.wholesale ? values.wholesale_amount || 2 : 2,
        region: values.region,
    }));
      history.push("/account/store/products/list")
    },
    validationSchema: RequestSchema
  });

  if (loading) return null;
  if (error || (store && !store?.is_active)) return <Redirect to="/account/store" />;
  return (
    <Card className="mb-1">
      <CardBody>
        <Form onSubmit={formik.handleSubmit} className="mt-2">
          <h3>Редактировать товар</h3>
          <Row>
            <Col sm="12">
              <FormGroup>
                <Label for="name">Название</Label>
                <Input
                  id="name"
                  name="title"
                  value={formik.values.title}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.touched["title"] && formik.errors["title"] && (
                  <p className="text-danger">{formik.errors["title"]}</p>
                )}
              </FormGroup>
            </Col>

            <Col sm="12">
              <FormGroup>
                <Label for="article">Артикул</Label>
                <Input
                  id="article"
                  name="article"
                  value={formik.values.article}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  readOnly
                />
                {formik.touched["article"] && formik.errors["article"] && (
                  <p className="text-danger">{formik.errors["article"]}</p>
                )}
              </FormGroup>
            </Col>

            <Col sm="12">
              <FormGroup>
                <Label for="description">Описание</Label>
                <Input
                  id="description"
                  name="description"
                  type="textarea"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.description}
                />
                {formik.touched["description"] && formik.errors["description"] && (
                  <p className="text-danger">{formik.errors["description"]}</p>
                )}
              </FormGroup>
            </Col>


            <Col sm="12">
              <FormGroup>
                <Label>Категория</Label>
                <Select
                  // closeMenuOnSelect={false}
                  options={categories}
                  placeholder={"Выберите категорию"}
                  noOptionsMessage={() => 'Нет категории!'}
                  value={categories.find(item => item.value === formik.values.category)}
                  onChange={({ value }) => formik.setFieldValue("category", value)}
                  className="React"
                  classNamePrefix="select"
                  styles={colourStyles}
                />
                {formik.touched["category"] && formik.errors["category"] && (
                  <p className="text-danger">{formik.errors["category"]}</p>
                )}
              </FormGroup>
            </Col>

            <Col sm="12">
              <FormGroup>
                <Label>Регион</Label>
                <Select
                  options={regionOptions}
                  placeholder={"Выберите регион"}
                  noOptionsMessage={() => 'Нет категории!'}
                  value={regionOptions.find(item => item.value === formik.values.region)}
                  onChange={({ value }) => formik.setFieldValue("region", value)}
                  className="React"
                  classNamePrefix="select"
                  styles={colourStyles}
                />
                {formik.touched["region"] && formik.errors["region"] && (
                  <p className="text-danger">{formik.errors["region"]}</p>
                )}
              </FormGroup>
            </Col>


            <Col sm="12">
              <FormGroup>
                <Label for="address">Адрес</Label>
                <Input
                  id="address"
                  name="address"
                  type="textarea"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.address}
                />
                {formik.touched["address"] && formik.errors["address"] && (
                  <p className="text-danger">{formik.errors["address"]}</p>
                )}
              </FormGroup>
            </Col>

            <Col sm="12">
              <ProductSpecificationsForm
                onChange={(value) => formik.setFieldValue("product_spec", value)}
                value={formik.values.product_spec}
                category={formik.values.category}
                product_specifications={product?.product_specifications}
                product_specifications_values={product?.product_specifications_values}
                edit
              />
            </Col>

            <Col xs="12">
              <FormGroup>
                <Label>Тип продажи</Label>
                <div className="d-flex align-items-center">
                  <CheckBoxesVuexy
                    color="primary"
                    icon={<Check className="vx-icon" size={16} />}
                    label="Доступно оптом"
                    className={`mr-3`}
                    name="wholesale"
                    checked={formik.values.wholesale}
                    onChange={(e) => formik.setFieldValue("wholesale", e.target.checked)}
                  />
                  <CheckBoxesVuexy
                    color="primary"
                    icon={<Check className="vx-icon" size={16} />}
                    label="Доступно в розницу"
                    name="retail"
                    checked={formik.values.retail}
                    onChange={(e) => formik.setFieldValue("retail", e.target.checked)}
                  />
                </div>
              </FormGroup>
            </Col>

            {formik.values.retail && (<>
              <Col xs={6}>
                <FormGroup>
                  <Label>Цена в розницу</Label>
                  <Input
                    type="number"
                    name="new_retail_price"
                    value={formik.values.new_retail_price}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {formik.touched["new_retail_price"] && formik.errors["new_retail_price"] && (
                    <p className="text-danger">{formik.errors["new_retail_price"]}</p>
                  )}
                </FormGroup>
              </Col>

              <Col xs={6}>
                <FormGroup>
                  <Label>Зачёркнутая цена</Label>
                  <Input
                    type="number"
                    name="old_retail_price"
                    value={formik.values.old_retail_price}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {formik.touched["old_retail_price"] && formik.errors["old_retail_price"] && (
                    <p className="text-danger">{formik.errors["old_retail_price"]}</p>
                  )}
                </FormGroup>
              </Col>
            </>)}

            {formik.values.wholesale && (
              <Col xs={12}>
                <FormGroup>
                  <Label>Цена оптом</Label>
                  <Input
                    type="number"
                    name="wholesale_price"
                    value={formik.values.wholesale_price}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {formik.touched["wholesale_price"] && formik.errors["wholesale_price"] && (
                    <p className="text-danger">{formik.errors["wholesale_price"]}</p>
                  )}
                </FormGroup>
              </Col>
            )}

            {formik.values.wholesale && formik.values.retail && (
                <Col>
                  <FormGroup>
                    <Label>С какого количества начинается оптом ?</Label>
                    <Input
                      type="number"
                      name="wholesale_amount"
                      value={formik.values.wholesale_amount}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      min="2"
                    />
                    {formik.touched["wholesale_amount"] && formik.errors["wholesale_amount"] && (
                      <p className="text-danger">{formik.errors["wholesale_amount"]}</p>
                    )}
                  </FormGroup>
                </Col>
              )}

            <Col xs={12}>
              <FormGroup>
                <Label>Количество на складе</Label>
                <Input
                  type="number"
                  name="amount"
                  value={formik.values.amount}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  min={0}
                />
                {formik.touched["amount"] && formik.errors["amount"] && (
                  <p className="text-danger">{formik.errors["amount"]}</p>
                )}
              </FormGroup>
            </Col>

            <ProductFormImages images={product.images} productId={productId} />

            <Col sm="12">
              <FormGroup>
                <Label for="is_active"></Label>
                <div className="d-flex align-items-center">
                  <CheckBoxesVuexy
                    defaultChecked={formik.values.is_active}
                    color="primary"
                    icon={<Check className="vx-icon" size={16} />}
                    label="Активировать"
                    name="is_active"
                    className={`mr-3`}
                    onChange={formik.handleChange}
                  />
                </div>
              </FormGroup>
            </Col>

            <Col xs="12">
              <Button
                disabled={false}
                className="w-100"
                type="submit"
                color="primary"
              >
                {"Сохранить"}
              </Button>
            </Col>
          </Row>
        </Form>
      </CardBody>
    </Card>
  )
};

const colourStyles = {
  control: styles => ({ ...styles, backgroundColor: 'white', borderColor: "#7367f0" }),
  option: (styles, { isDisabled, isFocused, isSelected }) => {
    return {
      ...styles,
      backgroundColor: isDisabled
        ? null
        : isSelected
          ? "#7367f0"
          : isFocused
            ? "rgba(115,103,240,0.15)"
            : null,
      ':active': {
        ...styles[':active'],
        backgroundColor:
          !isDisabled && (isSelected ? "#7367f0" : "white"),
      },
    };
  },
};
