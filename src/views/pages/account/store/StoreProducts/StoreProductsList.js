import React, {useEffect} from "react";
import {Col, Row} from "reactstrap";
import queryString from "query-string";
import DataListConfig from "./DataListConfig";
import {useDispatch, useSelector} from "react-redux";
import {Redirect} from "react-router-dom";
import {fetchMyProducts} from "../../../../../redux/slices/store/myShop.slice";

const StoreProductsList = (props) => {
  const dispatch = useDispatch()
  const myStoreId = useSelector(state => state.store?.myStore?.data?.shop_id)
  useEffect(() => {
    myStoreId && dispatch(fetchMyProducts({id: myStoreId}))
  }, [dispatch, myStoreId])
  const {data: store, loading, error} = useSelector(state => state.store.myStore);
  if (loading) return null;
  if (error || (store && !store?.is_active)) return <Redirect to="/account/store"/>;
  return (
    <Row>
      <Col sm="12">
        <DataListConfig thumbView parsedFilter={queryString.parse(props.location.search)}/>
      </Col>
    </Row>
  )
}

export default StoreProductsList
