import React, { useState } from 'react'
import { Check, Edit2, Trash } from 'react-feather';
import { CardBody, Col, Collapse, ListGroupItem, Row } from 'reactstrap'
import ProductsImagesSlider from '../../../../../../components/custom/ProductsImagesSlider/ProductsImagesSlider';
import ProductOptionsForm from './ProductOptionsForm';
import ProductOptionImages from './ProductOptionImages';
import { useDispatch } from 'react-redux';
import { updateProductOption } from '../../../../../../redux/slices/store/myShop.slice';

function ProductOptionItem({ data }) {
  const [isEdit, setIsEdit] = useState(false);
  const dispatch = useDispatch();
  const handleSubmit = (values)=>{
    setIsEdit(false);
    dispatch(updateProductOption({data,...values}))
  }
  const handleDelete = ()=>{

  }
  return (
    <ListGroupItem>
      {/* {JSON.stringify(data)} */}
      <Row>
        <Col style={{width: "15rem"}}>
          {!data.images?.length ? (
            <>
              <h3 className="text-danger">Нет изображений</h3>
              <p>Нажмите изменить чтобы добавить изображение</p>
            </>
          ):(
            <ProductsImagesSlider images={data.images}/>
          )}
        </Col>
        <Col>
          <ul className="mt-3">
            <li>
              <b>Описание: </b><span>{data?.description}</span>
            </li>
            <li>
              <b>Розничная стоимость: </b><span>{data?.regular_retail_price}</span>
            </li>
            <li>
              <b>Скидочная розничная стоимость: </b><span>{data?.discount_retail_price}</span>
            </li>
            <li>
              <b>Оптовая стоимость: </b><span>{data?.regular_wholesale_price}</span>
            </li>
            <li>
              <b>Скидочная оптовая стоимость: </b><span>{data?.discount_wholesale_price}</span>
            </li>
            <li>
              <b>Количество на складе: </b><span>{data?.amount}</span>
            </li>
          </ul>
        </Col>
      </Row>
      {isEdit ?
        <Check
          onClick={() => setIsEdit(false)}
          size={16} className={`mr-1`} /> :
        <Edit2
          onClick={() => setIsEdit(true)}
          size={16} className={`mr-1`} />
      }
      <Trash onClick={handleDelete} size={16} />

      <Collapse isOpen={!!isEdit}>
        <CardBody>
          <ProductOptionsForm initialValues={data} handleSubmit={handleSubmit}/>
          <br/>
          <ProductOptionImages product_option={data.id} images={data.images}/>
        </CardBody>
      </Collapse>
    </ListGroupItem>
  )
}

export default ProductOptionItem
