import React from 'react'
import { useFormik } from 'formik';
import { Button, Col, Form, FormGroup, Input, Label, Row } from 'reactstrap';
import * as Yup from "yup"

// const checkIfFilesAreTooBig = (files)=>{
//   let valid = true;
//   if (files) {
//     // files.map(file => {
//     //   const size = file.size / 1024 / 1024
//     //   if (size > 10) {
//     //     valid = false
//     //   }
//     // })
//   }
//   return valid
// }

// const checkIfFilesAreCorrectType = (files) => {
//   let valid = true;
//   if (files) {
//     files.map(file => {
//       if (!["image/jpg", "image/jpeg", "image/png"].includes(file.type)) {
//         valid = false
//       }
//     })
//   }
//   return valid
// }

const ProductOptionsSchema = Yup.object().shape({
  description: Yup.string()
    .required("Это поле обязательно"),
  regular_retail_price: Yup.number()
    .required("Это поле обязательно"),
  discount_retail_price: Yup.number()
    .required("Это поле обязательно"),
  regular_wholesale_price: Yup.number()
    .required("Это поле обязательно"),
  discount_wholesale_price: Yup.number()
    .required("Это поле обязательно"),
  amount: Yup.number(),
  images: Yup.array()
  // .nullable()
  // .required('Это поле обязательно')
  // .test('is-correct-file', 'Этот файл слишком большой', checkIfFilesAreTooBig)
  // .test(
  //   'is-big-file',
  //   'Файл должен быть в формате jpeg или png',
  //   checkIfFilesAreCorrectType
  // ),
});

function ProductOptionsForm({
  handleSubmit,
  initialValues = {
    description: "",
    amount: 0,
    regular_retail_price: 0,
    discount_retail_price: 0,
    regular_wholesale_price: 0,
    discount_wholesale_price: 0,
  },
  // id = null,
}) {

  const formik = useFormik({
    initialValues,
    onSubmit: (values) => {
      handleSubmit(values);
      formik.resetForm();
    },
    validationSchema: ProductOptionsSchema,
  });

  return (
    <Form className="mt-3" onSubmit={formik.handleSubmit}>
      <Row>
        <Col xs={12}>
          <FormGroup>
            <Label>Описание</Label>
            <Input
              type="textarea"
              name="description"
              value={formik.values.description}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.touched["description"] && formik.errors["description"] && (
              <p className="text-danger">{formik.errors["description"]}</p>
            )}
          </FormGroup>
        </Col>

        <Col xs={12}>
          <FormGroup>
            <Label>Обычная цена в розницу</Label>
            <Input
              type="number"
              name="regular_retail_price"
              value={formik.values.regular_retail_price}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.touched["regular_retail_price"] && formik.errors["regular_retail_price"] && (
              <p className="text-danger">{formik.errors["regular_retail_price"]}</p>
            )}
          </FormGroup>
        </Col>

        <Col xs={12}>
          <FormGroup>
            <Label>Скидочная цена в розницу</Label>
            <Input
              type="number"
              name="discount_retail_price"
              value={formik.values.discount_retail_price}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.touched["discount_retail_price"] && formik.errors["discount_retail_price"] && (
              <p className="text-danger">{formik.errors["discount_retail_price"]}</p>
            )}
          </FormGroup>
        </Col>

        <Col xs={12}>
          <FormGroup>
            <Label>Обычная цена оптом</Label>
            <Input
              type="number"
              name="regular_wholesale_price"
              value={formik.values.regular_wholesale_price}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.touched["regular_wholesale_price"] && formik.errors["regular_wholesale_price"] && (
              <p className="text-danger">{formik.errors["regular_wholesale_price"]}</p>
            )}
          </FormGroup>
        </Col>

        <Col xs={12}>
          <FormGroup>
            <Label>Скидочная цена оптом</Label>
            <Input
              type="number"
              name="discount_wholesale_price"
              value={formik.values.discount_wholesale_price}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.touched["discount_wholesale_price"] && formik.errors["discount_wholesale_price"] && (
              <p className="text-danger">{formik.errors["discount_wholesale_price"]}</p>
            )}
          </FormGroup>
        </Col>

        <Col xs={12}>
          <FormGroup>
            <Label>Количество на складе</Label>
            <Input
              type="number"
              name="amount"
              value={formik.values.amount}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.touched["amount"] && formik.errors["amount"] && (
              <p className="text-danger">{formik.errors["amount"]}</p>
            )}
          </FormGroup>
        </Col>
        <Button color="primary" className="w-100" type="submit">
          Отправить
        </Button>
      </Row>
    </Form>
  )
}

export default ProductOptionsForm
