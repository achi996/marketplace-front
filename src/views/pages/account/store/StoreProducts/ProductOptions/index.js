import React from 'react'
import { Card, CardBody, ListGroup } from "reactstrap";
import ProductOptionItem from "./ProductOptionItem";

import "../../../../../../assets/scss/components/edit-product-options.scss"
import ProductOptionsForm from './ProductOptionsForm';
import { useDispatch } from 'react-redux';
import { addProductOption } from '../../../../../../redux/slices/store/myShop.slice';

function ProductOptions({product_options, productId}) {
  const dispatch = useDispatch();

  const handleSubmit = (values)=>{
    dispatch(addProductOption({
      productId,
      ...values,
    }));
  };

  const renderOptions = (
    <ListGroup>
      {product_options.map(option=>(
        <ProductOptionItem
          data={option}
          key={`product-option-item-${option.id}`}
        />
      ))}
    </ListGroup>
  );
  return (
    <Card className="mb-1 edit-product">
      <CardBody>
        <h4>Варианты товара</h4>
        {
          !product_options?.length ? (
            <p className="text-danger">У вас нет вариантов товара</p>
          ):(renderOptions)
        }
        <Card outline className="border mt-3">
          <CardBody>
            <h4>Добавить вариант</h4>
            <ProductOptionsForm handleSubmit={handleSubmit} />
          </CardBody>
        </Card>
      </CardBody>
    </Card>
  );
}

export default ProductOptions;
