import React, { useCallback, useEffect, useState } from 'react'
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  Button, Card, CardBody, Col, Form, FormGroup, Input, Label, Row,
} from "reactstrap";
import Select from "react-select";
import "../../../../../assets/scss/plugins/forms/react-select/_react-select.scss"
import "../../../../../assets/scss/pages/auth-store.scss"
import CustomDragNDrop from "../../../../../components/custom/CustomDragNDrop/DragNDrop";
import { useDispatch, useSelector } from 'react-redux';
import { upgradeMyProduct, fetchProductUpgradeOptions } from '../../../../../redux/slices/store/myShop.slice';
import { toast } from 'react-toastify';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import {Redirect} from "react-router-dom";
// import ReactInputMask from 'react-input-mask';

const RequestSchema = Yup.object().shape({
  product_article: Yup.string()
    .required("Заполните это поле!"),
  days: Yup.object().shape({
    value: Yup.number().required(),
    price: Yup.number().required(),
    label: Yup.string().required(),
  })
    .nullable()
    .required("Заполните это поле!"),
  payment_type: Yup.object().shape({
    value: Yup.number().required(),
    number: Yup.string().required(),
    label: Yup.string().required(),
  })
    .nullable()
    .required("Заполните это поле!"),
  receipt: Yup.mixed()
    .required("Загрузите фото!"),
});

function createSrc(file) {
  return new Promise((res) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.addEventListener("load", () => {
      res(reader.result);
    });
  });
}

function StoreApplication() {
  const [receiptFile, setReceiptFile] = useState(null)
  const [receiptSrc, setReceiptSrc] = useState(null)

  const { days, payment_types } = useSelector(state=>({
    days: state.store.myShop.days.map(day=>({ value: day.id, price: parseFloat(day.price), label: day.days  })),
    payment_types: state.store.myShop.paymentTypes.map(type=>({ value: type.id, number: type.account_number, label: type.name  })),
  }));

  const dispatch = useDispatch();
  const { data: store, loading, error } = useSelector(state => state.store.myStore);

  const formik = useFormik({
    initialValues: {
      product_article: "",
      days: null,
      payment_type: null,
      receipt: null,
    },
    onSubmit: async (values) => {
      const formData = new FormData();
      if (values.product_article) formData.append("product_article", values.product_article);
      if (values.days) formData.append("days", values.days.value);
      if (values.payment_type) formData.append("payment_type", values.payment_type.value);
      if (receiptFile) formData.append("receipt", receiptFile, receiptFile.name);
      dispatch(upgradeMyProduct(formData));
      formik.resetForm();
      setReceiptFile(null);
      setReceiptSrc(null);
    },
    validationSchema: RequestSchema
  });

  const {
    setFieldValue,
    handleSubmit,
    handleChange,
    handleBlur,
    values,
    errors,
    touched,
  } = formik;

  const handlereceiptSave = useCallback(async () => {
    if (receiptFile) {
      setFieldValue("receipt", receiptFile)
      const res = await createSrc(receiptFile);
      setReceiptSrc(res);
    }
  }, [receiptFile, setFieldValue]);

  const handleCopy = ()=>{
    toast.success("Скопировано!", {
      position: toast.POSITION.TOP_RIGHT,
      autoClose: 2000
    })
  }

  useEffect(() => {
    handlereceiptSave();
  }, [handlereceiptSave]);

  useEffect(()=>{
    dispatch(fetchProductUpgradeOptions());
  },[dispatch]);

  if (loading) return null;
  if (error || (store && !store?.is_active)) return <Redirect to="/account/store" />;

  return (
    <Card className="mb-1">
      <CardBody>
        <Form onSubmit={handleSubmit} className="mt-2">
          <h4>Продвинуть товар</h4>
          <Row>
            <Col sm="12">
              <FormGroup>
                <Label for="product_article">Артикул</Label>
                <Input
                  id="product_article"
                  name="product_article"
                  value={values.product_article}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                {touched["product_article"] && errors["product_article"] && (
                  <p className="text-danger">{errors["product_article"]}</p>
                )}
              </FormGroup>
            </Col>

            <Col sm="12">
              <FormGroup>
                <Label for="days">Выберите количество дней</Label>
                <Select
                  classNamePrefix="select"
                  value={ values.days }
                  name="days"
                  placeholder={"Выберите количество дней"}
                  onBlur={handleBlur}
                  onChange={(option) => formik.setFieldValue("days", option)}
                  options={days}
                />
                {touched["days"] && errors["days"] && (
                  <p className="text-danger">{errors["days"]}</p>
                )}
                {values.days && <p className="mt-1"><b>Cтоимость: </b>{values.days.price}</p>}
              </FormGroup>
            </Col>

            <Col sm={12}>
              <FormGroup>
                <Label for="payment_type">Выберите вариант оплаты</Label>
                <Select
                  classNamePrefix="select"
                  value={ values.payment_type }
                  name="payment_type"
                  placeholder={"Выберите вариант оплаты"}
                  onBlur={handleBlur}
                  onChange={option => formik.setFieldValue("payment_type", option)}
                  options={payment_types}
                />
                {touched["payment_type"] && errors["payment_type"] && (
                  <p className="text-danger">{errors["payment_type"]}</p>
                )}
                {values.payment_type && <p className="mt-1">
                  <b>Переведите деньги на этот счёт: </b>
                  <CopyToClipboard onCopy={handleCopy} text={values.payment_type.number}>
                    <span className="cursor-pointer">{values.payment_type.number}</span>
                  </CopyToClipboard>
                </p>}
              </FormGroup>
            </Col>

            <Col
              sm={12}
              style={{ gridTemplate: "1fr / 1fr" }}
              className={`pb-2 pt-2 auth-store_img_uploader`}
            >
              <FormGroup className={`full-width`}>
                <Label for="receipt">Квитанция об оплате</Label>
                <div className={`d-flex`}>
                  {(receiptSrc) && <img src={receiptSrc} alt="" />}
                  <CustomDragNDrop
                    onActiveText={"Прикрепите квитанцию об оплате ..."}
                    text={"Перетащите или выберите изображение"}
                    setState={setReceiptFile}
                  />
                </div>
                {errors["receipt"] && (
                  <p className="text-danger">{errors["receipt"]}</p>
                )}
              </FormGroup>
            </Col>

            <Col xs="12">
              <Button
                disabled={false}
                className="w-100"
                type="submit"
                color="primary"
              >
                {"Сохранить"}
              </Button>
            </Col>
            {/* <pre>{JSON.stringify(values, null, 2)}</pre> */}
          </Row>
        </Form>
      </CardBody>
    </Card>
  )
}

export default StoreApplication;
