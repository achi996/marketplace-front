import React from "react";
import {Button, Modal, ModalFooter, ModalHeader} from "reactstrap";

export default function DeletionModal({isOpen, setOpen, onAccept}){
  return (
    <Modal isOpen={isOpen}
           toggle={() => setOpen(() => false)} className={`modal-dialog-centered`}>
      <ModalHeader toggle={() => setOpen(() => false)} className={`danger`}>
        Вы уверены, что хотите удалить товар?
      </ModalHeader>
      <ModalFooter>
        <Button color="danger" onClick={() => {
          setOpen(() => false)
          onAccept()
        }}>
          Подтвердить
        </Button>{" "}
      </ModalFooter>
    </Modal>
  )
}
