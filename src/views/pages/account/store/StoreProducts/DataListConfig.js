import React, {Component, useState} from "react"
import {
  Button,
  // UncontrolledDropdown,
  // DropdownMenu,
  // DropdownToggle,
  // DropdownItem,
  Input
} from "reactstrap"
import DataTable from "react-data-table-component"
import ReactPaginate from "react-paginate"
import {
  // Edit,
  ChevronDown,
  Plus,
  Check,
  ChevronLeft,
  ChevronRight,
  Delete
} from "react-feather"
import {connect, useDispatch} from "react-redux"
import Checkbox from "../../../../../components/@vuexy/checkbox/CheckboxesVuexy"
import {history} from "../../../../../history"
import "../../../../../assets/scss/plugins/extensions/react-paginate.scss";
import "../../../../../assets/scss/pages/data-list.scss";
import {deleteMyProduct, myShopSelector} from "../../../../../redux/slices/store/myShop.slice";
import no_data from "../../../../../assets/img/pages/no-data-concept-illustration.jpeg"
import {Link} from "react-router-dom";
import DeletionModal from "./DeletionModal";

const selectedStyle = {
  rows: {
    selectedHighlighStyle: {
      backgroundColor: "rgba(115,103,240,.05)",
      color: "#7367F0 !important",
      boxShadow: "0 0 1px 0 #7367F0 !important",
      "&:hover": {
        transform: "translateY(0px) !important"
      }
    }
  }
}

const ActionsComponent = ({row}) => {
  const [open, setOpen] = useState(false)
  const dispatch = useDispatch();

  const handleDeleteProduct = () => dispatch(deleteMyProduct(row))
  return (
    <div className="data-list-action">
      {/* <Edit
        className="cursor-pointer mr-1"
        size={20}
        onClick={() => history.push(`/account/store/products/${props.row.id}`)}
      /> */}
      <Delete
        size={20}
        onClick={() => setOpen(true)}
      />
      <DeletionModal isOpen={open} setOpen={setOpen} onAccept={handleDeleteProduct}/>
    </div>
  )
}

const CustomHeader = props => {
  return (
    <div className="data-list-header d-flex justify-content-between flex-wrap">
      <div className="actions-left d-flex flex-wrap">
        <Link className="d-block" to="/account/store/products/add">
          <Button
            className="add-new-btn"
            color="primary"
            outline>
            <Plus size={15} />
            <span className="align-middle">Добавить</span>
          </Button>
        </Link>
      </div>
      <div className="actions-right d-flex flex-wrap mt-sm-0 mt-2">
        {/* <UncontrolledDropdown className="data-list-rows-dropdown mr-1 d-md-block d-none">
          <DropdownToggle color="" className="sort-dropdown">
            <span className="align-middle mx-50">
              {`${props.index?.[0] || 1} - ${props.index?.[1] || 4} из ${props?.total || 4}`}
            </span>
            <ChevronDown size={15} />
          </DropdownToggle>
          <DropdownMenu tag="div" right>
            <DropdownItem tag="a" onClick={() => props.handleRowsPerPage(4)}>
              4
            </DropdownItem>
            <DropdownItem tag="a" onClick={() => props.handleRowsPerPage(10)}>
              10
            </DropdownItem>
            <DropdownItem tag="a" onClick={() => props.handleRowsPerPage(15)}>
              15
            </DropdownItem>
            <DropdownItem tag="a" onClick={() => props.handleRowsPerPage(20)}>
              20
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown> */}
        <div className="filter-section">
          <Input type="text" onChange={e => props.handleFilter(e)} />
        </div>
      </div>
    </div>
  )
}

class DataListConfig extends Component {
  static getDerivedStateFromProps(props, state) {
    if (
      props.dataList.length !== state.data.length ||
      state.currentPage !== props.parsedFilter.page
    ) {
      return {
        data: props.dataList,
        allData: props.dataList.filteredData,
        totalPages: props.dataList.totalPages,
        currentPage: parseInt(props.parsedFilter.page) - 1,
        rowsPerPage: parseInt(props.parsedFilter.perPage),
        totalRecords: props.dataList.totalRecords,
        sortIndex: props.dataList.sortIndex
      }
    }

    // Return null if the state hasn't changed
    return null
  }

  state = {
    data: [],
    totalPages: 0,
    currentPage: 0,
    columns: [
      {
        name: "Изображение",
        selector: "img",
        minWidth: "220px",
        cell: row => {
          return (
            <img
              src={row.images?.[0]?.image || no_data}
              height="100" alt={row.name}/>
          )
        }
      },
      {
        name: "Название",
        selector: "title",
        sortable: true,
        minWidth: "250px",
        cell: row => (
          <p title={row.title} className="text-truncate text-bold-500 mb-0">
            {row.title}
          </p>
        )
      },
      {
        name: "Артикул",
        selector: "article",
        sortable: true,
        minWidth: "250px",
        cell: row => (
          <p title={row.article} className="text-truncate text-bold-500 mb-0">
            {row.article}
          </p>
        )
      },
      {
        name: "Категория",
        selector: "category",
        sortable: true
      },
      {
        name: "Действия",
        sortable: false,
        cell: row=><ActionsComponent row={row}/>
      }
    ],
    allData: [],
    value: "",
    rowsPerPage: 4,
    selected: [],
    totalRecords: 0,
    sortIndex: [],
    addNew: ""
  }

  componentDidMount() {
    this.props.getData(this.props.parsedFilter)
    this.props.getInitialData()
  }

  handleFilter = e => {
    this.setState({ value: e.target.value })
    this.props.filterData(e.target.value)
  }

  handleRowsPerPage = value => {
    let { parsedFilter, getData } = this.props
    let page = parsedFilter.page !== undefined ? parsedFilter.page : 1
    history.push(`/account/store/products/?page=${page}&perPage=${value}`)
    this.setState({ rowsPerPage: value })
    getData({ page: parsedFilter.page, perPage: value })
  }

  handlePagination = page => {
    let { parsedFilter, getData } = this.props
    let perPage = parsedFilter.perPage !== undefined ? parsedFilter.perPage : 4
    history.push(
      `?page=${page.selected + 1}&perPage=${perPage}`
    )
    getData({ page: page.selected + 1, perPage: perPage })
    this.setState({ currentPage: page.selected })
  }

  render() {
    let {
      columns,
      data,
      allData,
      totalPages,
      value,
      rowsPerPage,
      totalRecords,
      sortIndex
    } = this.state

    return (
      <div
        className={`data-list thumb-view`}>
        <DataTable
          columns={columns}
          data={value.length ? allData : data}
          pagination
          noDataComponent={
            <div className={`d-flex align-items-center`} style={{height: 100}}>
              Нет записей для отображения
            </div>
          }
          paginationServer
          paginationComponent={() => (
            <ReactPaginate
              previousLabel={<ChevronLeft size={15}/>}
              nextLabel={<ChevronRight size={15}/>}
              breakLabel="..."
              breakClassName="break-me"
              pageCount={totalPages}
              containerClassName="vx-pagination separated-pagination pagination-end pagination-sm mb-0 mt-2"
              activeClassName="active"
              forcePage={
                this.props.parsedFilter.page
                  ? parseInt(this.props.parsedFilter.page - 1)
                  : 0
              }
              onPageChange={page => this.handlePagination(page)}
            />
          )}
          noHeader
          subHeader
          responsive
          pointerOnHover
          selectableRowsHighlight
          onSelectedRowsChange={data =>
            this.setState({ selected: data.selectedRows })
          }
          customStyles={selectedStyle}
          subHeaderComponent={
            <CustomHeader
              handleFilter={this.handleFilter}
              handleRowsPerPage={this.handleRowsPerPage}
              rowsPerPage={rowsPerPage}
              total={totalRecords}
              index={sortIndex}
            />
          }
          onRowClicked={row=>history.push(`edit/${row.id}`)}
          sortIcon={<ChevronDown />}
          selectableRowsComponent={Checkbox}
          selectableRowsComponentProps={{
            color: "primary",
            icon: <Check className="vx-icon" size={12} />,
            label: "",
            size: "sm"
          }}
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    dataList: myShopSelector.selectAll(state.store.myShop)
  }
}

export default connect(mapStateToProps, {
  getData: ()=>({type: "store/myShop/fetchMyProducts"}),
  deleteData: ()=>({type: "none"}),
  updateData: ()=>({type: "none"}),
  addData: ()=>({type: "none"}),
  getInitialData: ()=>({type: "none"}),
  filterData: ()=>({type: "none"}),
})(DataListConfig)
