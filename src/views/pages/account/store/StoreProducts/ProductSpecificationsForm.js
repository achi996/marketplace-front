import Axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Check } from 'react-feather';
import { FormGroup, Label, Row } from 'reactstrap';
import CheckBoxesVuexy from '../../../../../components/@vuexy/checkbox/CheckboxesVuexy';
import { API_BASE_URL } from '../../../../../helpers/constants';

// const initialValue = {

// };

const fetchData = async (category)=>{
  const { data } = await Axios.get(`${API_BASE_URL}/store/product_specifications/?category=${category}`);
  return data.results;
}

function ProductSpecificationsForm({ 
  value = [], //? product_spec
  onChange, 
  category = null, 
  edit=false,
  product_specifications,
  product_specifications_values,
}) {
  const [data, setData] = useState(value);
  
  const handleChange = (checked, specIndex, specValueIndex = null)=>{
    const newData = data.map((_spec, _specIndex)=> specIndex !== _specIndex ? _spec : {
      ..._spec,
      ...( specValueIndex === null ? 
        { isSelected: checked } : 
        {
          product_specifications_values: _spec.product_specifications_values
            .map((_specVal, _specValIndex) => ({
              ..._specVal,
              ...(specValueIndex === _specValIndex ? {isSelected: checked} : {})
            })),
        }
      ),
    });
    setData(newData);
  }
  
  // console.log(newData, newSpecValues, checked, specIndex, specValueIndex);

  useEffect(()=>{
    if(category){
      fetchData(category).then((data)=>{
        if(!edit)return setData(data);
        const newData = data.map(spec=>({
          ...spec, 
          isSelected: product_specifications.includes(spec.id),
          product_specifications_values: spec.product_specifications_values.map(specVal=>({
            ...specVal,
            isSelected: product_specifications_values.includes(specVal.id),
          })),
        }));
        setData(newData);
      });
    }
  }, [category, edit, product_specifications, product_specifications_values]);

  useEffect(()=>{
    if(data!==value){
      onChange(data);
    }
  }, [data, onChange, value]);

  if(!data?.length || !category)return null;
  return (
    <FormGroup>
      <Label>Характеристики товара</Label>
      { data.map((item, specIndex)=>(
        <div key={`product-spec-${item.id}`}>
          <CheckBoxesVuexy
            color="primary"
            icon={<Check className="vx-icon" size={16} />}
            label={item.name}
            name={`product-spec`}
            checked={ !!item?.isSelected }
            onChange={ ({target: {checked}})=>handleChange(checked, specIndex) }
          />
          <Row>
            { item?.isSelected && item.product_specifications_values.map((specValue, specValueIndex)=>(
              <CheckBoxesVuexy
                key={`product-spec-value-${specValue.id}`}
                color="primary"
                icon={<Check className="vx-icon" size={16} />}
                label={`${item.name}: ${specValue.value}`}
                className={`ml-3`}
                name={`product-spec-${item.id}`}
                checked={ !!specValue?.isSelected }
                onChange={ ({target: {checked}})=>handleChange(checked, specIndex, specValueIndex) }
              />
            )) }
          </Row>
        </div>
      )) }
    </FormGroup>
  )
}

export default ProductSpecificationsForm
