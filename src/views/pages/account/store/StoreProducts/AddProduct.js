import React from 'react'
import { useFormik } from "formik";
import * as Yup from "yup";
import { Button, Card, CardBody, Col, Form, FormGroup, Input, Label, Row, } from "reactstrap";
import Select from "react-select";
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import CheckBoxesVuexy from "../../../../../components/@vuexy/checkbox/CheckboxesVuexy";
import { Check } from "react-feather";
import { addMyProduct, REGION_CHOICES } from "../../../../../redux/slices/store/myShop.slice";

import "../../../../../assets/scss/plugins/forms/react-select/_react-select.scss"
import "../../../../../assets/scss/pages/auth-store.scss"
import { history } from '../../../../../history';
import ProductSpecificationsForm from './ProductSpecificationsForm';

const RequestSchema = Yup.object().shape({
  title: Yup.string()
    .required("Заполните это поле!")
  , description: Yup.string()
    .required("Заполните это поле!")
  , category: Yup.mixed()
    .required("Заполните это поле!")
  , region: Yup.string()
    .oneOf(Object.keys(REGION_CHOICES), `Неверный регион! Регион должен быть одним из: \n - ${Object.values(REGION_CHOICES).join("\n - ")}`)
    .notOneOf([""])
    .required("Заполните это поле!")
});

const regionOptions = Object.entries(REGION_CHOICES).map(([key, value])=>({ value: key, label: value }));

const AddProduct = () => {
  const { data: store, loading, error } = useSelector(state => state.store.myStore);
  const dispatch = useDispatch();
  const categories = useSelector(state => {
    return state.categories.flattenCategories
      .map(cat => ({ value: cat.id, label: cat.title, isFixed: false }))
  });

  const handleSubmit = async ({
    title,
    description,
    address,
    retail,
    wholesale,
    category,
    amount,
    product_specifications,
    old_retail_price,
    new_retail_price,
    wholesale_price,
    wholesale_amount,
    region,
  }) => {
    const specValues = [];
    const specs = product_specifications
      .filter(item => item.isSelected)
      .map(item => {
        item.product_specifications_values
          .filter(item => item.isSelected)
          .forEach(item => specValues.push(item.id));
        return item.id;
      });

    dispatch(addMyProduct({
      title,
      description,
      address,
      retail,
      wholesale,
      category,
      amount,
      product_specifications: specs,
      product_specifications_values: specValues,
      old_retail_price: retail ? old_retail_price : 0,
      new_retail_price: retail ? new_retail_price || 0 : 0,
      wholesale_price: wholesale ? wholesale_price : 0,
      wholesale_amount: wholesale ? wholesale_amount || 2 : 2,
      region,
      callback: (data) => {
        history.push(`/account/store/products/edit/${data.id}`);
      }
    }));
  }

  const formik = useFormik({
    initialValues: {
      title: "",
      description: "",
      address: "",
      retail: false,
      wholesale: false,
      category: null,
      product_specifications: [],
      amount: 0,
      old_retail_price: 0,
      new_retail_price: 0,
      wholesale_price: 0,
      wholesale_amount: 0,
      region: "",
    },
    onSubmit: handleSubmit,
    validationSchema: RequestSchema,
  });

  if (loading) return null;
  if (error || (store && !store?.is_active)) return <Redirect to="/account/store" />;
  return (
    <>
      <Card className="mb-1">
        <CardBody>
          <Form onSubmit={formik.handleSubmit} className="mt-2">
            {/* {JSON.stringify(formik.errors)} */}
            <Row>
              <Col sm="12">
                <FormGroup>
                  <Label for="name">Название</Label>
                  <Input
                    id="name"
                    name="title"
                    value={formik.values.title}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                  {formik.touched["title"] && formik.errors["title"] && (
                    <p className="text-danger">{formik.errors["title"]}</p>
                  )}
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup>
                  <Label for="description">Описание</Label>
                  <Input
                    id="description"
                    name="description"
                    type="textarea"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.description}
                  />
                  {formik.touched["description"] && formik.errors["description"] && (
                    <p className="text-danger">{formik.errors["description"]}</p>
                  )}
                </FormGroup>
              </Col>

              <Col sm="12">
                <FormGroup>
                  <Label>Категория</Label>
                  <Select
                    // closeMenuOnSelect={false}
                    options={categories}
                    placeholder={"Выберите категорию"}
                    noOptionsMessage={() => 'Нет категории!'}
                    value={categories.find(item => item.value === formik.values.category)}
                    onChange={({ value }) => formik.setFieldValue("category", value)}
                    className="React"
                    classNamePrefix="select"
                    styles={colourStyles}
                  />
                  {formik.touched["category"] && formik.errors["category"] && (
                    <p className="text-danger">{formik.errors["category"]}</p>
                  )}
                </FormGroup>
              </Col>

              <Col sm="12">
                <FormGroup>
                  <Label>Регион</Label>
                  <Select
                    options={regionOptions}
                    placeholder={"Выберите регион"}
                    noOptionsMessage={() => 'Нет категории!'}
                    value={regionOptions.find(item => item.value === formik.values.region)}
                    onChange={({ value }) => formik.setFieldValue("region", value)}
                    className="React"
                    classNamePrefix="select"
                    styles={colourStyles}
                  />
                  {formik.touched["region"] && formik.errors["region"] && (
                    <p className="text-danger">{formik.errors["region"]}</p>
                  )}
                </FormGroup>
              </Col>

              <Col sm="12">
                <FormGroup>
                  <Label for="address">Адрес</Label>
                  <Input
                    id="address"
                    name="address"
                    type="textarea"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.address}
                  />
                  {formik.touched["address"] && formik.errors["address"] && (
                    <p className="text-danger">{formik.errors["address"]}</p>
                  )}
                </FormGroup>
              </Col>

              <Col sm="12">
                <ProductSpecificationsForm
                  onChange={(value) => formik.setFieldValue("product_specifications", value)}
                  value={formik.values.product_specifications}
                  category={formik.values.category}
                />
              </Col>

              <Col xs="12">
                <FormGroup>
                  <Label>Тип продажи</Label>
                  <div className="d-flex align-items-center">
                    <CheckBoxesVuexy
                      color="primary"
                      icon={<Check className="vx-icon" size={16} />}
                      label="Доступно оптом"
                      className={`mr-3`}
                      name="wholesale"
                      checked={formik.values.wholesale}
                      onChange={(e) => formik.setFieldValue("wholesale", e.target.checked)}
                    />
                    <CheckBoxesVuexy
                      color="primary"
                      icon={<Check className="vx-icon" size={16} />}
                      label="Доступно в розницу"
                      name="retail"
                      checked={formik.values.retail}
                      onChange={(e) => formik.setFieldValue("retail", e.target.checked)}
                    />
                  </div>
                </FormGroup>
              </Col>

              {formik.values.retail && (
                <>
                  <Col xs={6}>
                    <FormGroup>
                      <Label for="new_retail_price">Цена в розницу</Label>
                      <Input
                        type="number"
                        name="new_retail_price"
                        id="new_retail_price"
                        value={formik.values.new_retail_price}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                      />
                      {formik.touched["new_retail_price"] && formik.errors["new_retail_price"] && (
                        <p className="text-danger">{formik.errors["new_retail_price"]}</p>
                      )}
                    </FormGroup>
                  </Col>
                  <Col xs={6}>
                    <FormGroup>
                      <Label for="old_retail_price">Зачёркнутая цена</Label>
                      <Input
                        type="number"
                        name="old_retail_price"
                        id="old_retail_price"
                        value={formik.values.old_retail_price}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                      />
                      {formik.touched["old_retail_price"] && formik.errors["old_retail_price"] && (
                        <p className="text-danger">{formik.errors["old_retail_price"]}</p>
                      )}
                    </FormGroup>
                  </Col>
                </>
              )}

              {formik.values.wholesale && (
                <Col xs={12}>
                  <FormGroup>
                    <Label for="wholesale_price">Цена оптом</Label>
                    <Input
                      type="number"
                      name="wholesale_price"
                      id="wholesale_price"
                      value={formik.values.wholesale_price}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    />
                    {formik.touched["wholesale_price"] && formik.errors["wholesale_price"] && (
                      <p className="text-danger">{formik.errors["wholesale_price"]}</p>
                    )}
                  </FormGroup>
                </Col>
              )}

              {formik.values.wholesale && formik.values.retail && (
                <Col>
                  <FormGroup>
                    <Label for="wholesale_amount">С какого количества начинается оптом ?</Label>
                    <Input
                      type="number"
                      name="wholesale_amount"
                      id="wholesale_amount"
                      value={formik.values.wholesale_amount}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      min="2"
                    />
                    {formik.touched["wholesale_amount"] && formik.errors["wholesale_amount"] && (
                      <p className="text-danger">{formik.errors["wholesale_amount"]}</p>
                    )}
                  </FormGroup>
                </Col>
              )}

              <Col xs={12}>
                <FormGroup>
                  <Label for="amount">Количество на складе</Label>
                  <Input
                    type="number"
                    name="amount"
                    id="amount"
                    value={formik.values.amount}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    min={0}
                  />
                  {formik.touched["amount"] && formik.errors["amount"] && (
                    <p className="text-danger">{formik.errors["amount"]}</p>
                  )}
                </FormGroup>
              </Col>

              <Col xs="12">
                <Button
                  disabled={false}
                  className="w-100"
                  type="submit"
                  color="primary"
                >
                  {"Создать"}
                </Button>
              </Col>

            </Row>
          </Form>
        </CardBody>
      </Card>
      <br />
      <br />
      <br />
      <br />
      <br />
    </>
  )
};

export default AddProduct;

const colourStyles = {
  control: styles => ({ ...styles, backgroundColor: 'white', borderColor: "#7367f0" }),
  option: (styles, { isDisabled, isFocused, isSelected }) => {
    return {
      ...styles,
      backgroundColor: isDisabled
        ? null
        : isSelected
          ? "#7367f0"
          : isFocused
            ? "rgba(115,103,240,0.15)"
            : null,
      ':active': {
        ...styles[':active'],
        backgroundColor:
          !isDisabled && (isSelected ? "#7367f0" : "white"),
      },
    };
  },
};
