import React from "react"
import { useLocation, useRouteMatch } from "react-router-dom"

import Breadcrumbs from "../../../components/@vuexy/breadCrumbs/BreadCrumb"

import "../../../assets/scss/pages/account-settings.scss"
import AccountPageRoutes from "./AccountPageRoutes";
import AccountPageNav from "./AccountPageNav"

function AccountPage() {
  const location = useLocation();
  const {path} = useRouteMatch();

  return (
    <React.Fragment>
      <Breadcrumbs
        breadCrumbTitle="Личный кабинет"
        breadCrumbParent="Главная"
        breadCrumbActive="Личный кабинет"
      />
      <div className="nav-vertical account-settings-page">
        <AccountPageNav path={path} currentPath={location.pathname} />
        <AccountPageRoutes path={path} />
      </div>
    </React.Fragment>
  )
}

export default AccountPage

