import React from 'react'
import { useSelector } from 'react-redux'
import ProductsCard from '../../../components/custom/ProductsCard/ProductsCard';
import { productsSelector } from '../../../redux/slices/products.slice'
import "../../../assets/scss/pages/app-ecommerce-shop.scss"



function Wishlist() {

  const data = useSelector(state=>productsSelector.selectAll(state.wishlist));

  const renderList = data.map(item=>(
    <ProductsCard data={item} key={`wishlist-item-${item.slug}`}/>
  ))

  return (
    <React.Fragment>
      <div className="ecommerce-application">
        { renderList?.length ?
          <div className="grid-view wishlist-items">{renderList}</div> :
          <div>
            <h3>У вас пока нет избранных товаров</h3>
          </div>
        }
      </div>
    </React.Fragment>
  )
}

export default Wishlist
