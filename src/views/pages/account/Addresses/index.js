import React from "react"
import {
  Row,
  Col,
  Card,
  CardBody,
  ListGroup
} from "reactstrap"
import { useDispatch, useSelector } from "react-redux";

import "flatpickr/dist/themes/light.css";
import "../../../../assets/scss/plugins/forms/flatpickr/flatpickr.scss"
import AddressItem from "./AddressItem";
import AddressForm from "./AddressForm";
import { addAddress } from "../../../../redux/slices/addresses.sclice";

export default function InfoTab() {
  const addressesIds = useSelector(state => state.addresses.ids);
  const dispatch = useDispatch()
  const handleSubmit = (values) => {
    dispatch(addAddress(values));
  };

  return (
    <Card className="mb-1">
      <CardBody>
        <Row>
          <Col sm={12}>
            <ListGroup>
              {addressesIds.map(id => (
                <AddressItem id={id} key={`addresses-item-${id}`} />
              ))}
            </ListGroup>
          </Col>
          <Card className={`mb-0`}>
            <CardBody>
              <AddressForm
                handleSubmit={handleSubmit}
              />
            </CardBody>
          </Card>
        </Row>
      </CardBody>
    </Card>
  )
}
