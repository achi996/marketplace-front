import React from "react"
import * as Yup from "yup"
import {
  Button,
  Row,
  Col,
  FormGroup,
  Input,
  Label,
  Form,
} from "reactstrap"
import { useFormik } from "formik";
import { useSelector } from "react-redux"
import FormError from "../../../../helpers/utils/FormError";

const AddressSchema = Yup.object().shape({
  country: Yup.string(),
    // .required("Это поле обязательно"),
  city: Yup.string(),
    // .required("Это поле обязательно"),
  street: Yup.string(),
    // .required("Это поле обязательно"),
  house_number: Yup.string(),
    // .required("Это поле обязательно"),
});

function AddressForm({
  handleSubmit,
  initialValues = {
    country: "",
    city: "",
    street: "",
    house_number: "",
  },
}) {
  const loading = useSelector(state=>state.addresses.loading);
  const respError = useSelector(state=>state.addresses.error?.type === FormError.type && state.addresses.error);

  const formik = useFormik({
    initialValues,
    onSubmit: (values)=>{
      handleSubmit(values);
      formik.resetForm();
    },
    validationSchema: AddressSchema,
  });

  return (
    <Form onSubmit={formik.handleSubmit}>
      {respError && respError.getField("detail")}
      <Row>
        <Col md="6" sm="12">
          <FormGroup className="form-label-group">
            <Input
              type="text"
              name="country"
              value={formik.values.country}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            <Label>Страна</Label>
            {formik.touched["country"] && formik.errors["country"] && (
              <p className="text-danger">{formik.errors["country"]}</p>
            )}
            {respError && respError.getField("country")}
          </FormGroup>
        </Col>
        <Col md="6" sm="12">
          <FormGroup className="form-label-group">
            <Input
              type="text"
              name="city"
              value={formik.values.city}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            <Label>Город / село</Label>
            {formik.touched["city"] && formik.errors["city"] && (
              <p className="text-danger">{formik.errors["city"]}</p>
            )}
            {respError && respError.getField("city")}
          </FormGroup>
        </Col>
        <Col md="6" sm="12">
          <FormGroup className="form-label-group">
            <Input
              type="text"
              name="street"
              value={formik.values.street}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            <Label>Улица</Label>
            {formik.touched["street"] && formik.errors["street"] && (
              <p className="text-danger">{formik.errors["street"]}</p>
            )}
            {respError && respError.getField("street")}
          </FormGroup>
        </Col>
        <Col md="6" sm="12">
          <FormGroup className="form-label-group">
            <Input
              type="text"
              name="house_number"
              value={formik.values.house_number}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            <Label>Номер квартиры/дома</Label>
            {formik.touched["house_number"] && formik.errors["house_number"] && (
              <p className="text-danger">{formik.errors["house_number"]}</p>
            )}
            {respError && respError.getField("house_number")}
          </FormGroup>
        </Col>
        <Button
          color="primary"
          type="submit"
          className="mx-1 w-100"
          disabled={loading}
        >
          {loading ? "Подождите..." : "Сохранить"}
        </Button>
      </Row>
    </Form>
  )
}

export default AddressForm
