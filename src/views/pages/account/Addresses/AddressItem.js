import React from "react"
import {
  ListGroupItem,
  CardBody,
  Collapse,
} from "reactstrap"
import { Check, Edit2, Trash } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { addressesSelector, deleteAddress, updateAddress } from "../../../../redux/slices/addresses.sclice";
import { toggleEditAddress } from "../../../../redux/slices/addresses.sclice";
import AddressForm from "./AddressForm";

function AddressItem({id}) {
  
  const data = useSelector(state=>addressesSelector.selectById(state.addresses, id));
  const dispatch = useDispatch();

  const toggleEdit = () => {
    dispatch(toggleEditAddress(id));
  };

  const handleSubmit = (values)=>{
    dispatch(updateAddress({
      id,
      ...values,
    }));
    dispatch(toggleEditAddress(id));
  };

  const handleDelete = ()=>{
    dispatch(deleteAddress({id}));
  }

  return (
    <ListGroupItem>
      <div className={`d-flex align-items-center justify-content-between`}>
        <p className={`m-0`}>{`${data.country}, ${data.city}, ${data.street}, ${data.house_number}`}</p>
        <div>
          {!data.isEdit ?
            <Edit2
              onClick={() => toggleEdit(data.id)}
              size={16} className={`mr-1`} /> :
            <Check
              onClick={() => toggleEdit(data.id)}
              size={16} className={`mr-1`} />
          }
          <Trash onClick={handleDelete} size={16} />
        </div>
      </div>
      <Collapse isOpen={!!data.isEdit}>
        <CardBody>
          <AddressForm initialValues={data} handleSubmit={handleSubmit}/>
        </CardBody>
      </Collapse>
    </ListGroupItem>
  )
}

export default AddressItem
