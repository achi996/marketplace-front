import React from "react"
import {
  Nav,
  NavItem,
  Collapse,
} from "reactstrap"
import classnames from "classnames"
import {
  ChevronDown,
  ChevronUp,
} from "react-feather"
import { Link } from "react-router-dom"
import navItems from './navigationsPages';

const AccountPageNavWithChildren = ({path, currentPath, item})=>(
  <NavItem>
    <Link
      to={`${path}${item.path}`}
      className={classnames("nav-link", {
        active: currentPath.startsWith(path + item.path),
      })}
    >
      <item.icon size={16} />
      <span className="align-middle ml-1">{item.label}</span>
      {currentPath.startsWith(path + item.path) ? <ChevronDown size={16} /> : <ChevronUp size={16} />}
    </Link>
    <Collapse isOpen={currentPath.startsWith(path + item.path)}>
      <div className={`ml-1`}>
        {item.children.map(subItem => ( !subItem.hidden &&
          <Link
            to={`${path}${item.path}${subItem.path}`}
            className={classnames("nav-link", {
              active: currentPath.startsWith(path + item.path + subItem.path),
            })}
            key={`account-nav-${item.id}-${subItem.id}`}
          >
            <subItem.icon size={16} />
            <span className="align-middle ml-1">{subItem.label}</span>
          </Link>
        ))}
      </div>
    </Collapse>
  </NavItem>
);

const AccountPageItem = ({path, currentPath, item})=>(
  <NavItem>
    <Link
      to={`${path}${item.path}`}
      className={classnames("nav-link", {
        active: currentPath.startsWith(path + item.path),
      })}
    >
      <item.icon size={16} />
      <span className="align-middle ml-1">{item.label}</span>
    </Link>
  </NavItem>
);

const AccountPageNav = ({ path, currentPath }) => {
  return (
    <Nav className="account-settings-tab nav-left mr-0 mr-sm-3" tabs>
      {navItems.map(item => !item.children ?
        <AccountPageItem key={`account-nav-${item.id}`} path={path} currentPath={currentPath} item={item}/> :
        <AccountPageNavWithChildren key={`account-nav-${item.id}`} path={path} currentPath={currentPath} item={item}/>
      )}
    </Nav>
  )
  
};

export default AccountPageNav;