import {
  Box,
  DollarSign,
  Heart,
  Info,
  Lock,
  MessageSquare,
  PlusSquare,
  // RotateCcw,
  Settings,
  ShoppingBag
} from "react-feather"
import AccountProfile from "./AccountProfile"
import ChangePassword from "./ChangePassword"
// import PurchaseHistory from "./PurchaseHistory"
import Wishlist from "./Wishlist";
import AccountStore from "./store";
import StoreOrders from './store/StoreOrders/StoreOrders';
import StoreFeedbacks from "./store/StoreFeedbacks";
// import StoreInfo from "./store/StoreInfo";
import AddProduct from "./store/StoreProducts/AddProduct";
import UpgradeProduct from "./store/StoreProducts/UpgradeProduct";
import StoreProductsList from "./store/StoreProducts/StoreProductsList";
import StoreProductsEdit from "./store/StoreProducts/StoreProductsEdit";
import StoreApplication from "./store/StoreApplication";

const navigationPages = [
  {
    id: "account",
    label: "Мои данные",
    icon: Settings,
    path: "/profile",
    component: AccountProfile,
  }, {
    id: "change-password",
    label: "Изменить пароль",
    icon: Lock,
    path: "/change-password",
    component: ChangePassword,
  },
  {
    id: "wishlist",
    label: "Избранное",
    icon: Heart,
    path: "/wishlist",
    component: Wishlist,
  },
  // {
  //   id: "orders",
  //   label: "История покупок",
  //   icon: RotateCcw,
  //   path: "/orders",
  //   component: PurchaseHistory,
  // },
  {
    id: "store",
    label: "Мой магазин",
    icon: ShoppingBag,
    path: "/store",
    component: AccountStore,
    children: [
      {
        id: "info",
        label: "Информация",
        icon: Info,
        path: "/info",
        component: StoreApplication,
        // component: StoreInfo,
      }, {
        id: "products",
        label: "Товары",
        icon: Box,
        path: "/products/list",
        component: StoreProductsList,
      },
      {
        id: "add-product",
        label: "Добавить товар",
        icon: PlusSquare,
        path: "/products/add",
        component: AddProduct,
      },
      {
        id: "edit-product",
        label: "Изменить товар",
        icon: PlusSquare,
        path: "/products/edit/:id",
        component: StoreProductsEdit,
        hidden: true,
      },
      {
        id: "advance-product",
        label: "Продвинуть товар",
        icon: PlusSquare,
        path: "/products/upgrade",
        component: UpgradeProduct,
      },
      // {
      //   id: "analytics",
      //   label: "Статистика",
      //   icon: PieChart,
      //   path: "/analytics",
      //   withoutCard: true,
      //   component: StoreAnalytics,
      // },
      {
        id: "feedbacks",
        label: "Отзывы",
        icon: MessageSquare,
        path: "/feedbacks",
        component: StoreFeedbacks,
      }, {
        id: "orders",
        label: "Заказы",
        icon: DollarSign,
        path: "/orders",
        component: StoreOrders,
      }
    ]
  },
];

export default navigationPages;
