import React from "react"
import { Button, FormGroup, Row, Col, Card, CardBody } from "reactstrap"
import { Formik, Form } from "formik"
import * as Yup from "yup"
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { changePassword } from "../../../redux/slices/auth.slice";
import FormError from "../../../helpers/utils/FormError";
import PasswordInput from "../../../components/custom/Form/PasswordInput";

const ChangePasswordSchema = Yup.object().shape({
  current_password: Yup.string()
    .required("Required"),
  new_password: Yup.string()
    .min(8, "Пароль должен быть не меньше 8 символов!")
    .required("Заполните это поле!"),
  confirm_pass: Yup.string()
    .oneOf([Yup.ref('new_password'), null], 'Пароли не совпадают!')
    .required("Заполните это поле!"),
});
class ChangePassword extends React.Component {
  handleSubmit = ({ current_password, new_password }, { resetForm }) => {
    this.props.changePassword({
      current_password,
      new_password,
    });
    resetForm();
  }

  render() {
    const { respError } = this.props
    return (
      <Card className="mb-1">
        <CardBody>
          <h4>Изменить пароль</h4>
          <Row className="pt-1">
            <Col sm="12">
              <Formik
                initialValues={{
                  current_password: "",
                  new_password: "",
                  confirm_pass: ""
                }}
                onSubmit={this.handleSubmit}
                validationSchema={ChangePasswordSchema}
              >
                {({ errors, touched, values, handleChange }) => (
                  <Form>
                    {respError && respError.getField("detail")}
                    <FormGroup className="position-relative has-icon-left">
                      <PasswordInput
                        onChange={handleChange}
                        value={values.current_password}
                        name="current_password"
                        id="current_password"
                        className={`form-control ${errors.current_password &&
                          touched.current_password &&
                          "is-invalid"}`}
                        placeholder="Старый пароль"
                      />
                      {errors.current_password && touched.current_password ? (
                        <div className="text-danger">{errors.current_password}</div>
                      ) : null}
                      {respError && respError.getField("current_password")}
                    </FormGroup>
                    <FormGroup className="position-relative has-icon-left">
                      <PasswordInput
                        onChange={handleChange}
                        value={values.new_password}
                        name="new_password"
                        placeholder="Новый пароль"
                        id="new_password"
                        className={`form-control ${errors.new_password &&
                          touched.new_password &&
                          "is-invalid"}`}
                      />
                      {errors.new_password && touched.new_password ? (
                        <div className="text-danger">{errors.new_password}</div>
                      ) : null}
                      {respError && respError.getField("new_password")}
                    </FormGroup>
                    <FormGroup className="position-relative has-icon-left">
                      <PasswordInput
                        onChange={handleChange}
                        value={values.confirm_pass}
                        name="confirm_pass"
                        id="confirm_pass"
                        className={`form-control ${errors.confirm_pass &&
                          touched.confirm_pass &&
                          "is-invalid"}`}
                        placeholder="Подтверждение пароля"
                      />
                      {errors.confirm_pass && touched.confirm_pass ? (
                        <div className="text-danger">{errors.confirm_pass}</div>
                      ) : null}
                    </FormGroup>
                    <div className="d-flex justify-content-between align-items-center flex-wrap">
                      <div className={`d-flex align-items-center`}>
                        <Button.Ripple
                          className="mr-1 mb-1"
                          color="primary"
                          type="submit"
                        >
                          Сохранить
                      </Button.Ripple>
                        <Button.Ripple
                          className="mb-1"
                          color="danger"
                          type="reset"
                          outline
                        >
                          Отменить
                      </Button.Ripple>
                      </div>
                      <NavLink to={"/page/reset-password"}>
                        Забыли пароль?
                    </NavLink>
                    </div>
                  </Form>
                )}
              </Formik>
            </Col>
          </Row>
        </CardBody>
      </Card>
    )
  }
}

const mapStateToProps = state => {
  const { loading, error } = state.auth;
  const respError = state.auth.error?.type === FormError.type && state.auth.error;
  return { loading, error, respError };
};

export default connect(mapStateToProps, { changePassword })(ChangePassword)
