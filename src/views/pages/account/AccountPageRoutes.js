import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import navItems from './navigationsPages';

const AccountPageRoutes = ({ path }) => {
  return (
    <Switch>
      <React.Fragment>
        <Route exact path={path}>
          <Redirect to={`${path}/profile`} />
        </Route>
        {navItems.map(item => (
          <React.Fragment key={`account-nav-route-${item.id}`}>
            <Route
              path={`${path}${item.path}`}
              component={item.component}
              exact={true}
            />
            {item.children && item.children.map(subItem => (
              <Route
                key={`account-nav-route-${item.id}-${subItem.path}`}
                path={`${path}${item.path}${subItem.path}`}
                component={subItem.component}
                exact={true}
              />
            ))}
          </React.Fragment>
        ))}
      </React.Fragment>
    </Switch>
  );
};

export default AccountPageRoutes;
