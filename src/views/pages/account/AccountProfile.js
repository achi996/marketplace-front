import React, { useEffect } from "react";
import {
  Button,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  Col,
  CardBody,
  Card
} from "reactstrap";
import * as Yup from 'yup';
import { useFormik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import FormError from "../../../helpers/utils/FormError";
import { changeProfile, clearAuthError } from "../../../redux/slices/auth.slice";
import "../../../assets/scss/pages/authentication.scss";
import ReactInputMask from "react-input-mask";

const ProfileSchema = Yup.object().shape({
  first_name: Yup.string()
    .required("Заполните это поле!"),
  last_name: Yup.string()
    .required("Заполните это поле!"),
  phone_number: Yup.string()
    .required("Заполните это поле!"),
});

function AccountProfile({ userData }) {

  const dispatch = useDispatch();
  const loading = useSelector(state => state.auth.loading);
  const respError = useSelector(state => state.auth.error?.type === FormError.type && state.auth.error);

  const formik = useFormik({
    initialValues: {
      first_name: userData?.first_name || "",
      last_name: userData?.last_name || "",
      phone_number: userData?.phone_number || "",
    },
    onSubmit: (values) => {
      dispatch(changeProfile(values));
    },
    validationSchema: ProfileSchema
  });

  useEffect(() => {
    return () => {
      dispatch(clearAuthError());
    }
  }, [dispatch]);

  return (
    <Card className="mb-1">
      <CardBody>
        <h4>Мои данные</h4>
        <Form onSubmit={formik.handleSubmit}>
          {respError && respError.getField("detail")}
          <Row>
            <Col sm="12">
              <FormGroup>
                <Label for="email">Email</Label>
                <Input
                  id="email"
                  value={userData?.email || ""}
                  readOnly
                />
              </FormGroup>
            </Col>
            <Col sm="12">
              <FormGroup>
                <Label for="first_name">Имя</Label>
                <Input
                  id="first_name"
                  name="first_name"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.first_name}
                />
                {formik.touched["first_name"] && formik.errors["first_name"] && (
                  <p className="text-danger">{formik.errors["first_name"]}</p>
                )}
                {respError && respError.getField("first_name")}
              </FormGroup>
            </Col>
            <Col sm="12">
              <FormGroup>
                <Label for="last_name">Фамилия</Label>
                <Input
                  id="last_name"
                  name="last_name"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.last_name}
                />
                {formik.touched["last_name"] && formik.errors["last_name"] && (
                  <p className="text-danger">{formik.errors["last_name"]}</p>
                )}
                {respError && respError.getField("last_name")}
              </FormGroup>
            </Col>
            <Col sm="12">
              <FormGroup>
                <Label for="phone_number">Номер телефона</Label>
                <ReactInputMask
                  id="phone_number"
                  className="form-control"
                  mask="+\9\96999999999"
                  name="phone_number"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.phone_number}
                />
                {formik.touched["phone_number"] && formik.errors["phone_number"] && (
                  <p className="text-danger">{formik.errors["phone_number"]}</p>
                )}
                {respError && respError.getField("phone_number")}
              </FormGroup>
            </Col>
            <Col xs="12">
              <Button
                disabled={loading}
                className=""
                type="submit"
                color="primary"
              >
                {loading ? "Подождите..." : "Сохранить"}
              </Button>
            </Col>
          </Row>
        </Form>
      </CardBody>
    </Card>
  )
}

export default (props) => {
  const userData = useSelector(state => state.auth.userData);
  if (!userData) return null;
  return <AccountProfile userData={userData} {...props} />
};
