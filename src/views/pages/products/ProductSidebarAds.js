import Axios from 'axios'
import React, { memo, useEffect, useState } from 'react'
import { Card } from 'reactstrap';
import { API_BASE_URL } from '../../../helpers/constants'
import Swiper from "react-id-swiper";

import '../../../assets/scss/components/custom/ad.scss';
import "../../../assets/scss/plugins/extensions/swiper.scss"

const fetchAds = async () => {
  const { data: { results } } = await Axios.get(`${API_BASE_URL}/store_settings/banners_for_ad/`);
  return results;
}

const useFetchAds = () => {
  const [ads, setAds] = useState([]);

  useEffect(()=>{
    fetchAds().then(setAds).catch(console.log);
  }, [setAds]);

  return ads;
}

function ProductSidebarAds() {
  const ads = useFetchAds();
  const gallerySwiperParams = {
    spaceBetween: 10,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
  }
  return (
    <div className="ad-list">
      <Card style={{aspectRatio: "1/2"}} className={`box-shadow-0 ad-card ads-swiper`}>
        {!!ads.length && (
          <Swiper containerClass="w-100" {...gallerySwiperParams}>
            { ads.map(ad=>(
              <a key={ad.id + '-ad'} href={ad.link} target="_blank" rel="noopener noreferrer">
                <img
                  src={ad.image} 
                  alt="Ad"
                  className="w-100 h-100"
                  style={{objectFit: "cover"}}
                />
              </a>
            )) }
          </Swiper>
        )}
      </Card>
    </div>
  )
}

export default memo(ProductSidebarAds)
