import React from "react"
import Select from "react-select"
import {Button, Card, Col, FormGroup, Input, Row,} from "reactstrap"
import {Search} from "react-feather"
import {connect} from "react-redux"
import {withRouter} from "react-router-dom"
// import Autocomplete from "../../../components/@vuexy/autoComplete/AutoCompleteComponent"
import {fetchProducts, productsSelector} from "../../../redux/slices/products.slice"
import CustomPagination from "../../../components/custom/CustomPagination/CustomPagination"
import ProductsCard from "../../../components/custom/ProductsCard/ProductsCard"

import {setQueryParam} from "../../../redux/slices/query.slice"
import "../../../assets/scss/plugins/forms/react-select/_react-select.scss"
import CategoryDetail from "./CategoryDetail"
// import CategoriesDropdown from "../../../components/custom/CategoriesDropdown/CategoriesDropdown"

const sortOptions = [
  {
    value: "-created_at",
    label: "Новые"
  },
  {
    value: "actual_wholesale_price",
    label: "По оптовой цене"
  },
  {
    value: "actual_retail_price",
    label: "По розничной цене"
  },
];

window.IS_PRODUCTS_FETCHING = false;

class ProductsContent extends React.Component {
  isNewQueryCounter = 0;

  componentDidMount() {
    let localIsNewCounter = this.isNewQueryCounter;
    setTimeout(() => { //? Ебать костыль 🤦 До чего я докатился? Ненавижу этот проект!
      if (localIsNewCounter === this.isNewQueryCounter) {
        this.props.fetchProducts();
      }
    }, 500)
  }

  componentDidUpdate(prevProps) {
    const isNewQuery = ["page", "ordering", "retail", "wholesale", "category", "product_spec", "search", "discount_wholesale_price_min",
      "discount_wholesale_price_max", "discount_retail_price_min", "discount_retail_price_max"]
      .find(key => prevProps[key] !== this.props[key]);
    if (isNewQuery) {
      let localIsNewCounter = ++this.isNewQueryCounter;
      setTimeout(() => {
        if (localIsNewCounter === this.isNewQueryCounter) {
          this.props.fetchProducts();
        }
      }, 500)
    }
  }

  render() {
    const {retail, wholesale} = this.props;
    const data = this.props.data;
    let renderProducts = data.map((product) => {
      return (
        <ProductsCard
          key={product.slug + "-products-page-item"}
          data={product}
        />
      )
    })
    return (
      <Card className="mb-1 shop-content mb-1" style={{minHeight: 700, boxShadow: "none"}}>
        <Row>
          <Col sm="12">
            <div className="ecommerce-header-items flex-column flex-md-row" style={{gridGap: 28}}>
              <div style={{gridGap: "3px"}} className="result-toggler d-flex full-width align-items-center p-0">
                <Button outline={!wholesale} color={'primary'} className={`full-width`}
                        onClick={() => this.props.setQueryParam({key: "wholesale", value: !wholesale})}
                >Оптом</Button>
                <Button outline={!retail} color={'primary'} className={`full-width`}
                        onClick={() => this.props.setQueryParam({key: "retail", value: !retail})}
                >В розницу</Button>
              </div>
              <div className="view-options d-flex justify-content-lg-end justify-content-between full-width p-0"
                   style={{gridGap: "3px"}}>
                {/* <CategoriesDropdown/> */}
                <Button onClick={() => this.props.mainSidebar(!this.props.sidebar)}
                        className={`d-lg-none col-6 d-sm-block`}
                        color='primary' outline>Фильтрация</Button>
                <Select
                  className="React-Select col-6 col-lg-12 p-0"
                  classNamePrefix="select"
                  defaultValue={sortOptions[0]}
                  value={sortOptions.find(item => item.value === this.props.ordering)}
                  name="sort"
                  options={sortOptions}
                  onChange={({value}) => {
                    this.props.setQueryParam({key: "ordering", value});
                    if (value === "actual_wholesale_price") {
                      this.props.setQueryParam({key: "retail", value: false})
                      this.props.setQueryParam({key: "wholesale", value: true})
                    }else if (value === "actual_retail_price") {
                      this.props.setQueryParam({key: "wholesale", value: false})
                      this.props.setQueryParam({key: "retail", value: true})
                    }
                  }}
                />
              </div>
            </div>
          </Col>

          <Col sm="12" className={`mb-3`}>
            <div className="ecommerce-searchbar d-flex align-items-center mt-1">
              <FormGroup className="position-relative full-width p-0 m-0">
                <Input
                  // suggestions={data}
                  className="search-product form-control"
                  placeholder="Поиск..."
                  // suggestionLimit={4}
                  id="search-products"
                  // filterKey='title'
                  onChange={e => this.props.setQueryParam({key: "search", value: e.target.value})}
                />
                <div className="form-control-position">
                  <Search size={22}/>
                </div>
              </FormGroup>
            </div>
          </Col>
          <Col sm="12">
            <div id="ecommerce-products" className="grid-view">
              {!!renderProducts.length ? renderProducts : "По Вашему запросу ничего не найдено."}
            </div>
          </Col>
          <Col sm="12">
            <Row className="justify-content-center">
              {!!renderProducts.length &&
              <CustomPagination totalPages={this.props.pagesCount} active={this.props.page}/>}
            </Row>
          </Col>
          <Col sm="12" className="mt-sm-2">
            <CategoryDetail/>
          </Col>
        </Row>
      </Card>
    )
  }
}

const mapStateToProps = state => {
  const data = productsSelector.selectAll(state.products);
  const productsCount = state.products.count;
  const page = state.query.page || 1;
  const {ordering, retail, wholesale, category, product_spec} = state.query;
  const pagesCount = Math.ceil(productsCount / (state.query.limit || 20));
  const search = state.query.search;
  const discount_wholesale_price_min = state.query.discount_wholesale_price_min;
  const discount_wholesale_price_max = state.query.discount_wholesale_price_max;
  const discount_retail_price_min = state.query.discount_retail_price_min;
  const discount_retail_price_max = state.query.discount_retail_price_max;
  // console.log({data, pagesCount, page, productsCount, ordering, retail, wholesale, category})
  return {
    data,
    pagesCount,
    page,
    productsCount,
    ordering,
    retail,
    search,
    wholesale,
    category,
    product_spec,
    discount_wholesale_price_min,
    discount_wholesale_price_max,
    discount_retail_price_min,
    discount_retail_price_max
  };
};

export default withRouter(connect(mapStateToProps, {fetchProducts, setQueryParam})(ProductsContent));
