import React from "react"
import { Button, Card, CardBody } from "reactstrap"
import "rc-slider/assets/index.css"

import "../../../assets/scss/plugins/extensions/slider.scss"
import { setQueryParam, appendQueryParamsString } from "../../../redux/slices/query.slice"
import { connect } from "react-redux"
import Axios from "axios"
import { API_BASE_URL } from "../../../helpers/constants"
import { REGION_CHOICES } from "../../../redux/slices/store/myShop.slice"
import { Link } from "react-router-dom"
import { Range } from "rc-slider"
import { categoriesDrawerCtx } from "../../../components/custom/CategoriesDrawer/CategoriesDrawer"
import CustomSelect from "../../../components/custom/CustomSelect/CustomSelect"
import ProductSidebarAds from "./ProductSidebarAds"
import CategoriesTreeMenu from "../../../components/custom/CategoriesTreeMenu/CategoriesTreeMenu"


class ShopSidebar extends React.Component {
  state = {
    selectedCategory: null,
    selectedCategoryList: [],
    productSpecs: [],
    productSpecVals: [],
    wholesalePrice: [0, 100000],
    retailPrice: [0, 100000],
    windowWidth: null,
  }

  getSelectedCategory() {
    const selectedCategory = this.props.flattenCategories
      .find(item => this.props.queryCategory[0] === item.id.toString()) || null;
    this.setState({ selectedCategory });
  }
  updateWidth = () => {
    this.setState({ windowWidth: window.innerWidth });
  }
  componentDidMount() {
    this.getSelectedCategory();
    if (window !== undefined) {
      this.updateWidth();
      window.addEventListener("resize", this.updateWidth);
    }
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWidth);
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.queryCategory !== this.props.queryCategory || this.props.flattenCategories !== prevProps.flattenCategories) {
      this.getSelectedCategory();
    }
    if (prevState.selectedCategory !== this.state.selectedCategory) {
      this.fetchProductSpecifications();
    }
    if (
      prevProps.discount_wholesale_price_min !== this.props.discount_wholesale_price_min ||
      prevProps.discount_wholesale_price_max !== this.props.discount_wholesale_price_max ||
      prevProps.discount_retail_price_min !== this.props.discount_retail_price_min ||
      prevProps.discount_retail_price_max !== this.props.discount_retail_price_max
    ) {
      this.setState({
        wholesalePrice: [this.props.discount_wholesale_price_min, this.props.discount_wholesale_price_max],
        retailPrice: [this.props.discount_retail_price_min, this.props.discount_retail_price_max]
      })
    }
  }

  fetchProductSpecifications = async () => {
    if(this.state.selectedCategory){
      const { data } = await Axios.get(`${API_BASE_URL}/store/product_specifications/?category=${this.state.selectedCategory.id}`);
      this.setState({ productSpecs: data.results });
    }
  }

  handleSelectProdSpec = (specId, specVal) => {
    const selectedProductSpecs = { ...this.props.selectedProductSpecs };
    selectedProductSpecs[specId] = specVal;

    // const queryString = new URLSearchParams();

    const queryData = {};

    Object.entries(selectedProductSpecs).forEach(([key, val]) => {
      if (val !== "unknown" || val !== "") {
        queryData[`product_specifications_values-${key}`] = val;
      } else {
        delete queryData[`product_specifications_values-${key}`];
      }
    })

    for (const key in queryData) {
      this.props.setQueryParam({ key, value: queryData[key] });
    }
    // this.props.appendQueryParamsString({ queryString });

  }

  //? Product Specification Dropdown
  renderProductSpecDropdown = (spec) => {
    const specValues = [{
      key: "all",
      value: "",
      label: "Все",
    }].concat(
      spec.product_specifications_values
        .map(specVal => ({
          label: specVal.value,
          value: specVal.id.toString(),
          key: specVal.id
        }))
    )

    return (
      <div key={`product-specification-${spec.id}`} className="mt-1">
        <p className="m-0">{spec.name}</p>
        <CustomSelect
          items={specValues}
          placeholder={spec.name}
          value={specValues.find(item => item.value === this.props.selectedProductSpecs[spec.id])?.label}
          handleItemClick={({ value }) => this.handleSelectProdSpec(spec.id, value)}
        />
      </div>
    )
  }

  regionFields = [{
    key: "all",
    value: "",
    label: "Все",
  }].concat(
    Object.entries(REGION_CHOICES)
      .map(([key, value]) => ({
        key: key,
        label: value,
        value: key,
      }))
  )

  render() {
    const { fullHeight } = this.props;
    const { wholesalePrice, retailPrice } = this.state;

    return (<>

      <Card style={fullHeight ? {} : { overflow: "auto", maxHeight: "600px"}} className={`box-shadow-0`}>
        <CardBody className="p-2">

          <div className="clear-filters mb-2">
            <Link to="/products">
              <categoriesDrawerCtx.Consumer>
                {({ toggleCategoriesDrawer }) => (
                  this.state.windowWidth >= 990 &&
                  <Button
                    className="d-block mx-auto mt-2"
                    color="primary"
                    onClick={toggleCategoriesDrawer}
                  >
                    Все категории
                  </Button>
                )}
              </categoriesDrawerCtx.Consumer>
            </Link>
          </div>
          <div>
            {!!this.state.selectedCategory &&
              <div>
                <h6 className="filter-title">Категория:</h6>
                <p className="text-primary">{this.state.selectedCategory.title}</p>
              </div>
            }
            {!!this.state.selectedCategory?.children && 
              <CategoriesTreeMenu parentCategory={this.state.selectedCategory} force/>
            }
            {!!this.state.productSpecs.length && !!this.state.selectedCategory &&
              <div>
                <h6 className="filter-title">Характеристики</h6>
                {this.state.productSpecs.map(this.renderProductSpecDropdown)}
              </div>
            }
          </div>

          <hr />

          <div className="multi-range-region">
            <div className="multi-range-title pb-75">
              <h6 className="filter-title mb-0">Регион</h6>
            </div>
            <CustomSelect
              items={this.regionFields}
              placeholder={"Регион"}
              value={this.regionFields.find(item => item.value === this.props.region)?.label}
              handleItemClick={({ value }) => this.props.setQueryParam({
                key: "region",
                value: value
              })}
            />
          </div>
          <hr />

          <div className="price-slider">
            <div className="price-slider-title mt-1">
              <h6 className="filter-title mb-0">Оптовая цена</h6>
            </div>
            <p className="m-0 mt-1">От: {wholesalePrice[0]}</p>
            <p className="m-0">До: {wholesalePrice[1]}</p>
            <div className="price-slider mt-1">
              <Range
                min={0}
                max={100000}
                value={wholesalePrice}
                tipFormatter={value => `${value}cом`}
                step={100}
                onChange={(v) => this.setState({ wholesalePrice: v })}
                onAfterChange={(value) => {
                  this.props.setQueryParam({
                    key: "discount_wholesale_price_min",
                    value: value[0]
                  })
                  this.props.setQueryParam({
                    key: "discount_wholesale_price_max",
                    value: value[1]
                  })
                }}
              />
            </div>
          </div>
          <hr />

          <div className="price-slider">
            <div className="price-slider-title mt-1">
              <h6 className="filter-title mb-0">Розничная цена</h6>
            </div>
            <p className="m-0 mt-1">От: {retailPrice[0]}</p>
            <p className="m-0">До: {retailPrice[1]}</p>
            <div className="price-slider mt-1">
              <Range
                min={0}
                max={100000}
                value={retailPrice}
                tipFormatter={value => `${value}cом`}
                step={100}
                onChange={(v) => this.setState({ retailPrice: v })}
                onAfterChange={(value) => {
                  this.props.setQueryParam({
                    key: "discount_retail_price_min",
                    value: value[0]
                  })
                  this.props.setQueryParam({
                    key: "discount_retail_price_max",
                    value: value[1]
                  })
                }}
              />
            </div>
          </div>
        </CardBody>
      </Card>
      <ProductSidebarAds/>
    </>
    )
  }
}

const mapStateToProps = state => {
  const {
    query: {
      retail,
      wholesale,
      category,
      selectedProductSpecs,
      region,
      discount_wholesale_price_min,
      discount_wholesale_price_max,
      discount_retail_price_min,
      discount_retail_price_max,
    },
    categories: {
      categories,
      flattenCategories,
    }
  } = state;
  return {
    retail,
    wholesale,
    categories,
    flattenCategories,
    queryCategory: category,
    selectedProductSpecs,
    region,
    discount_wholesale_price_min,
    discount_wholesale_price_max,
    discount_retail_price_min,
    discount_retail_price_max,
  };
}
export default connect(mapStateToProps, { setQueryParam, appendQueryParamsString })(ShopSidebar)
