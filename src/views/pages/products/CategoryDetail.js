import React from 'react'
import {useSelector} from 'react-redux'
import {Card, CardBody} from 'reactstrap'
import '../../../assets/scss/components/custom/category-detail.scss'

const CategoryDetail = () => {
  const category = useSelector( state => state.categories.flattenCategories.find(cat => cat.id.toString() === state.query.category) );

  if(!category?.image || !category?.description)return null
  return (
    <Card className="mb-1 category-detail">
      <CardBody>
        <h3 className="category-detail__title mb-2">{category?.title}</h3>
        {category?.image && <img className="category-detail__img float-left mx-1" src={category.image} alt={category.title}/>}
        <p className="category-detail__description">
          {category?.description}<br/>
          При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия.
          По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба.
          Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально.
          Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально.
        </p>
        {/* <div className="d-flex flex-row">
          <div className="category-detail__img-wrapper">
          </div>
          <div>
          </div>
        </div> */}
      </CardBody>
    </Card>
  )
}

export default CategoryDetail
