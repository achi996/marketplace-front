import React from "react"
import { Button, Card, CardBody, Col, Row } from "reactstrap"
import { Heart, ShoppingCart, Star, } from "react-feather"
import Breacrumbs from "../../../../components/@vuexy/breadCrumbs/BreadCrumb"
import RelatedProducts from "./RelatedProducts"

import { connect } from "react-redux"
import { fetchProduct, productsSelector } from "../../../../redux/slices/products.slice"
import { Link, NavLink, withRouter } from "react-router-dom"
import { toggleItemInCart, updateItemInCart } from "../../../../redux/slices/cart.slice"
import { toggleItemInWishlist } from "../../../../redux/slices/wishlist.slice"
import ProductsImagesSlider from "../../../../components/custom/ProductsImagesSlider/ProductsImagesSlider"
import ProductsReviews from "./ProductsReviews"

import CustomRange from "../../../../helpers/utils/range"
import { setQueryParam } from "../../../../redux/slices/query.slice"
import Axios from "axios"
import { API_BASE_URL } from "../../../../helpers/constants"
import { getPrices } from "../../../../helpers/utils/productPrices"
import ShareButtons from "./ShareButtons"
import CustomSelect from "../../../../components/custom/CustomSelect/CustomSelect"

import "../../../../assets/scss/pages/app-ecommerce-shop.scss"
import '../../../../assets/scss/pages/product-detail.scss'

class DetailPage extends React.Component {
  state = {
    specs: [],
    stars: Array(5).fill(0),
    selected_specifications: {}
  }

  getSpecs = async () => {
    try {
      const { data: {results: data} } = await Axios.get(`${API_BASE_URL}/store/product_specifications/?category=${this.props?.data?.category}`);
      const newData = data.map(spec => {
        const filteredSpecVals = spec.product_specifications_values
          .filter(specVal => this.props.data.product_specifications_values.includes(specVal.id));

        const product_specifications_values = filteredSpecVals.map(specVal=>({
          ...specVal,
          key: specVal.id,
          value: specVal.id,
          label: specVal.value,
        }));

        return {
          ...spec,
          // isSelected: this.props.data.product_specifications.includes(spec.id),
          product_specifications_values,
        }
      });
      this.setState({ specs: newData });
    } catch (error) {
      console.error({error})
    }
  }

  fetchStarsCount = async () => {
    try {
      const { data } = await Axios.get(`${API_BASE_URL}/store/products/${this.props?.data?.slug}/reviews_count_by_star/`)
      const stars = [...this.state.stars];
      data.forEach(item => stars[item.stars - 1] = item.rate_count);
      this.setState({ stars });
    } catch (error) {

    }
  }

  componentDidMount() {
    if (!this.props.data) {
      this.props.fetchProduct({ slug: this.props.match.params.slug });
    }
    window.scrollTo({ top: 0, behavior: 'smooth' });
    this.getSpecs();
    this.fetchStarsCount();
    this.getProductCategory();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.slug !== this.props.match.params.slug) {
      if (!this.props.data) {
        this.props.fetchProduct({ slug: this.props.match.params.slug });
      }
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
    if(prevProps.data?.category !== this.props.data?.category){
      this.getProductCategory();
    }
  }

  getProductCategory(){
    if(this.props.data){
      this.setState({
        productCategory: this.props.flattenCategories.find(cat=>cat.id===this.props.data.category)
      })
    }
  }

  handleAddToCart = () => {
    this.props.toggleItemInCart({
      ...this.props.data,
      selected_specifications: this.state.selected_specifications
    });
  }
  handleAddToWishlist = () => {
    this.props.toggleItemInWishlist(this.props.data);
  };

  handleSpecItemClick = (spec) => (item) => {
    const { isInCart, inCartData } = this.props;
    const selected_specifications = isInCart ? {...inCartData.selected_specifications} : {...this.state.selected_specifications};

    selected_specifications[spec.id] = {
      id: spec.id,
      name: spec.name,
      value: {
        id: item.key,
        name: item.label,
      }
    };
    if(isInCart){
      this.props.updateItemInCart({ slug: inCartData.slug, selected_specifications })
    }else{
      this.setState({ selected_specifications })
    }
  }

  renderStars(count) {
    return new CustomRange(0, 4).map((i) => (
      <Star
        key={`product-review-star-${i}`}
        size={20}
        fill={i < count ? "#ff9f43" : "#ffffff"}
        stroke={i < count ? "#ff9f43" : "#b8c2cc"}
      />
    ));
  };

  render() {
    const { data, loading, error, isInCart, inCartData } = this.props;
    if (loading || error || !data) return null;
    const average_rating = Math.round(data.average_rating || 0);
    const { productCategory } = this.state;

    const {
      viewPrice,
      strikethroughPrice,
    } = getPrices(this.props.data);

    const productSpecs = this.state.specs.filter((spec) => !!spec.product_specifications_values.filter(item=>!!item.value).length )
    const selected_specifications = isInCart ? {...inCartData.selected_specifications} : {...this.state.selected_specifications};

    return (
      <div className="ecommerce-application product-detail-page">
        <Breacrumbs
          breadCrumbTitle="Детали товара"
          breadCrumbParent={<Link to="/products?page=1">Товары</Link>}
          breadCrumbActive="Детали товара"
        />
        <Card className="mb-1 overflow-hidden app-ecommerce-details mb-1">
          <CardBody className="pb-0 px-2">
            <Row className="mb-5 mt-2">
              <Col
                className="mb-2 mb-md-0"
                sm="12"
                md="5"
              >
                <ProductsImagesSlider images={data?.images || []} />
                {/* <img src={data.product_options?.[0]?.images[0]?.image} alt={data.title} height="250" width="250" /> */}
              </Col>
              <Col md="7" sm="12">
                <Row>
                  <Col md={6}>
                    <h3>{data.title}</h3>
                    <NavLink className={`primary`} to={`/shops/${data.shop}`}>От {data.shop_name}</NavLink>
                    <p className="text-muted">{data?.owner || ""}</p>
                  </Col>
                  <div className="ratings ml-1 pl-1">
                    {this.renderStars(average_rating)}
                    <span className="ml-1 font-medium-1 text-dark align-middle">
                      {average_rating ? parseFloat(data.average_rating).toFixed(2) : 0} звёзд
                      </span>
                  </div>
                </Row>

                <br/>

                <div className="row m-0">
                  {data.retail && <div className="col-6">
                    <h6 className="col-12 p-0">В розницу</h6>
                    {strikethroughPrice &&
                      <span className="text-decoration-line-through text-muted mr-1">
                        {strikethroughPrice}
                      </span>
                    }
                    <div>
                      <h3 className="text-primary mt-0">
                        {viewPrice} сом
                      </h3>
                    </div>
                  </div>}
                  {data.wholesale && <div>
                    <div>
                      <h6 className="col-12 p-0">Оптом</h6>
                      <h3 className="text-primary mt-0">
                        {data.actual_wholesale_price} сом
                      </h3>
                    </div>
                  </div>}
                </div>
                <hr />
                <p>{data.description}</p>
                <ul className="">
                  {data.wholesale && (
                    <li>
                      <span className="align-middle font-weight-bold ml-50">
                        Доступно оптом
                      </span>
                    </li>
                  )}
                  {data.retail && (
                    <li>
                      <span className="align-middle font-weight-bold ml-50">
                        Доступно в розницу
                      </span>
                    </li>
                  )}
                </ul>
                <hr />

                {productSpecs.length > 0 &&
                  <div>
                    <b>Характеристики: </b>
                    {productSpecs.map((spec) =>
                      <div className="mb-1" key={`product-spec-${spec.id}`}>
                        <CustomSelect
                          className={"product-spec-select"}
                          items={spec.product_specifications_values}
                          placeholder={spec.name}
                          value={spec.product_specifications_values.find(item => item.value === selected_specifications[spec.id]?.value?.id)?.label}
                          handleItemClick={this.handleSpecItemClick(spec)}
                        />
                      </div>
                    )}
                  </div>
                }

                <hr />
                {!!productCategory && <>
                  <p>Категория: <Link to={`/products?category=${productCategory.id}`}>{productCategory.title}</Link></p>
                  <hr />
                </>}
                {!!data.address && <>
                  <p>Aдрес склада: <span>{data.address}</span></p>

                  <hr />
                </>}

                {data?.in_stock ?
                  <span className="text-success">Есть в наличии</span> :
                  <span className="text-danger">Нет на складе</span>
                }
                <div className="action-btns mt-1">
                  <Button.Ripple
                    onClick={this.handleAddToCart}
                    className="mr-1 mb-1"
                    color="primary"
                    disabled={!data?.in_stock}
                  >
                    <ShoppingCart size={15} />
                    <span className="align-middle ml-50">
                      {this.props.isInCart ? "Убрать из корзины" : "В корзину"}
                    </span>
                  </Button.Ripple>
                  <Button.Ripple
                    onClick={this.handleAddToWishlist}
                    className="mb-1"
                    color="danger"
                    outline={!this.props.isInWishlist}
                  >
                    <Heart size={15} />
                    <span className="align-middle ml-50">Избранное</span>
                  </Button.Ripple>
                </div>
                <div>
                  <ShareButtons description={data.title} url={window.location.href} />
                </div>
                {/*<div className="d-flex flex-wrap social-media-btns">*/}
                {/*  <ShareToSocials/>*/}
                {/*</div>*/}
              </Col>
            </Row>
          </CardBody>
        </Card>
        <div className="d-flex flex-wrap flex-md-nowrap flex-row">
          <div className="d-flex align-items-start">
            <Card className='mb-1' style={{ minWidth: '285px' }}>
              <CardBody>
                <h3>Рейтинг товара</h3>
                <p>{parseFloat(data.average_rating || 0).toFixed(2)} / 5</p>
                <p>
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <b> {this.state.stars[4]} </b>
                </p>
                <p>
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ffffff" stroke="#b8c2cc" />
                  <b> {this.state.stars[3]} </b>
                </p>
                <p>
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ffffff" stroke="#b8c2cc" />
                  <Star size={20} fill="#ffffff" stroke="#b8c2cc" />
                  <b> {this.state.stars[2]} </b>
                </p>
                <p>
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ffffff" stroke="#b8c2cc" />
                  <Star size={20} fill="#ffffff" stroke="#b8c2cc" />
                  <Star size={20} fill="#ffffff" stroke="#b8c2cc" />
                  <b> {this.state.stars[1]} </b>
                </p>
                <p>
                  <Star size={20} fill="#ff9f43" stroke="#ff9f43" />
                  <Star size={20} fill="#ffffff" stroke="#b8c2cc" />
                  <Star size={20} fill="#ffffff" stroke="#b8c2cc" />
                  <Star size={20} fill="#ffffff" stroke="#b8c2cc" />
                  <Star size={20} fill="#ffffff" stroke="#b8c2cc" />
                  <b> {this.state.stars[0]} </b>
                </p>
              </CardBody>
            </Card>
          </div>
          <div className="pl-md-1" style={{ flexGrow: 1 }}>
            <Card className="mb-1">
              <ProductsReviews productId={this.props.data?.id} reviews={this.props.data?.reviews} />
            </Card>
          </div>
        </div>
          <RelatedProducts excludeProductById={data.id} category={data.category} />
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  const data = productsSelector.selectById(state.products, props.match.params.slug);
  const inCartData = productsSelector.selectById(state.cart, data?.slug);
  const isInCart = !!inCartData;
  const isInWishlist = !!state.wishlist.entities[data?.slug];
  const { loading, error } = state.products;
  const { flattenCategories } = state.categories
  return {
    data,
    loading,
    error,
    inCartData,
    isInCart,
    isInWishlist,
    flattenCategories,
  };
};

export default connect(mapStateToProps, {
  fetchProduct,
  toggleItemInCart,
  updateItemInCart,
  toggleItemInWishlist,
  setQueryParam,
})(withRouter(DetailPage));
