import React, {memo, useEffect, useState} from "react"
import {Card, CardBody, Col, Row} from "reactstrap"
// import {
//   Star,
// } from "react-feather"
// import { Link } from "react-router-dom"
import "react-multi-carousel/lib/styles.css";
import ProductsCard from "../../../../components/custom/ProductsCard/ProductsCard"
import Axios from "axios";
import {API_BASE_URL} from "../../../../helpers/constants";
import Carousel from "react-multi-carousel";
import "../../../../assets/scss/pages/app-ecommerce-shop.scss"

const responsive = {
  desktop: {
    breakpoint: {max: 3000, min: 1224},
    items: 4,
    slidesToSlide: 1 // optional, default to 1.
  },
  tablet: {
    breakpoint: {max: 1224, min: 964},
    items: 3,
    slidesToSlide: 1 // optional, default to 1.
  },
  xsTablet: {
    breakpoint: {max: 924, min: 564},
    items: 2,
    slidesToSlide: 1 // optional, default to 1.
  },
  mobile: {
    breakpoint: {max: 564, min: 0},
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  }
};

async function fetchProducts(category) {
  try {
    const {data} = await Axios.get(`${API_BASE_URL}/store/products/?category=${category}`)
    return data.results
  } catch (err) {
    console.log(err)
  }
}


function RelatedProducts({category, excludeProductById = null}) {
  const [hoverSlide, setHoverSlide] = useState();
  const [products, setProducts] = useState([]);
  // const products = useSelector(state=>state.products.ids.slice(0,6).map(id=>state.products.entities[id]));
  useEffect(() => {
    fetchProducts(category).then((products) => {
      setProducts(products.filter(item => !excludeProductById || item.id !== excludeProductById))
    })
  }, [category, excludeProductById])

  if (!products.length) return null

  return (
    <Card className="mb-1">
      <CardBody className={`related-products`}>
        <Row>
          <Col className="details-page-swiper products-card-view text-center mt-5" sm="12">
            <div className="heading-section mb-3">
              <h2 className="text-uppercase mb-50">Похожие товары</h2>
              <p>Люди так же ищут эти товары</p>
            </div>
            <Carousel responsive={responsive}
                      swipeable={true}
                      draggable={false}
                      infinite={true}
                      autoPlay={!hoverSlide}
                      autoPlaySpeed={3000}
                      centerMode={false}
                      containerClass="carousel-container"
                      removeArrowOnDeviceType={["xsTablet", "mobile"]}
            >
              {products.map((item, index) => {
                return (
                  <div key={index + "-recomendations"}
                       className={`pl-1 pr-1`}
                       onMouseEnter={() => setHoverSlide(true)}
                       onMouseLeave={() => setHoverSlide(false)}
                  >
                    <ProductsCard data={item}/>
                  </div>
                );
              })}
            </Carousel>
          </Col>
        </Row>
      </CardBody>
    </Card>
  )
}

export default memo(RelatedProducts)
