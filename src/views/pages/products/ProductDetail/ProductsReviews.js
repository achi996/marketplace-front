import { useFormik } from 'formik';
import React, { useState } from 'react';
import { Star } from 'react-feather';
import { useDispatch } from 'react-redux';
import { Button, Card, CardBody, Col, Form, Input, InputGroup, InputGroupAddon, Row } from 'reactstrap';
import * as Yup from "yup";
import CustomRange from '../../../../helpers/utils/range';
import { addProductReview } from '../../../../redux/slices/products.slice';

const range = new CustomRange(0,4);

function ProductReviewItem({ data }) {
  const renderStars = range.map((i)=>(
    <Star
      key={`product-review-star-${i}`}
      size={20}
      fill={i<data.stars ? "#ff9f43" : "#ffffff"}
      stroke={i<data.stars ? "#ff9f43" : "#b8c2cc"}
    />
  ));

  return (
    <Card className="mb-1 border">
      <CardBody>
          <div className="ratings border-left ml-1 pl-1 float-right">
            {renderStars}
            <span className="ml-1 font-medium-1 text-dark align-middle">
              {data.stars || 0}
            </span>
          </div>
          <p className="m-0">
            {data.comment}
          </p>
      </CardBody>
    </Card>
  )
};

const ProductsReviewsSchema = Yup.object().shape({
  product: Yup.number()
    .required("Заполните это поле!"),
  stars: Yup.number()
    .test("MIN-MAX", "Поставьте оценку от 1 до 5", (a)=> a>=1&&a<=5 )
    // .min(1, "Поставьте оценку от 1 до 5")
    // .max(5, "Поставьте оценку от 1 до 5")
    .required("Заполните это поле!"),
  comment: Yup.string()
    .min(1)
    .required("Заполните это поле!"),
})

function ProductsReviewsForm({productId}){
  const dispatch = useDispatch();
  const [hoveredStar, setHoveredStar] = useState();
  const onSubmit = async ({product, comment, stars})=>{
    dispatch(addProductReview({
      product,
      comment,
      stars,
    }));
    resetForm();
  };

  const {
    values,
    handleChange,
    handleBlur,
    setFieldValue,
    handleSubmit,
    resetForm,
    errors,
  } = useFormik({
    initialValues: {
      product: productId,
      comment: "",
      stars: 0,
    },
    onSubmit,
    validationSchema: ProductsReviewsSchema,
  });

  const renderStars = range.map((i)=>(
    <Star
      key={`product-review-star-${i}`}
      className="cursor-pointer"
      size={30}
      fill={i<hoveredStar || i<values.stars ? "#ff9f43" : "#ffffff"}
      stroke={i<hoveredStar || i<values.stars ? "#ff9f43" : "#b8c2cc"}
      onMouseEnter={()=>setHoveredStar(i+1)}
      onMouseLeave={()=>setHoveredStar(0)}
      onClick={()=>{setFieldValue("stars", i+1)}}
    />
  ));

  return (
    <Form onSubmit={handleSubmit}>
      <p className="text-center">{renderStars}</p>
      {errors.stars && <p className="text-danger text-center">{errors.stars}</p>}
      <Col className="d-flex justify-content-center mx-auto flex-wrap flex-sm-nowrap">
        <InputGroup>
          <Input
            value={values.comment}
            onChange={handleChange}
            onBlur={handleBlur}
            placeholder="Оставить отзыв"
            name="comment"
          />
          <InputGroupAddon className="d-none d-sm-block" addonType="append">
            <Button
              type="submit"
              color="primary"
            >
              Отправить
            </Button>
          </InputGroupAddon>
        </InputGroup>
        <Button
          className="d-sm-none w-100"
          type="submit"
          color="primary"
        >
          Отправить
        </Button>
        {/* {JSON.stringify(errors)} */}
      </Col>
    </Form>
  )
}

function ProductsReviews({ reviews = [], productId }) {
  const [offset, setOffset] = useState(0);
  return (
    <CardBody>
      <div className="heading-section mb-3 text-center">
        <h2 className="text-uppercase mb-50">Отзывы</h2>
        <p>Всего отзывов: {reviews?.length || 0}</p>
      </div>
      <ProductsReviewsForm productId={productId} />
      <br/>
      <Row className="flex-column">
        {reviews.slice(0,offset+3).map((review, i) => (
          <Col key={`review-item-${review.id || i}`}>
            <ProductReviewItem data={review} />
          </Col>
        ))}
      </Row>
      {reviews?.length > offset + 3 &&
        <Button
          type="button"
          color="primary"
          className="d-block mx-auto"
          onClick={()=>setOffset(offset+3)}
        > Показать ещё </Button>
      }
    </CardBody>
  )
}

export default ProductsReviews
