import React from "react"
import Sidebar from "react-sidebar"
import ProductsSidebar from "./ProductsSidebar"
import ProductsContent from "./ProductsContent"
import Breacrumbs from "../../../components/@vuexy/breadCrumbs/BreadCrumb"
import "../../../assets/scss/pages/app-ecommerce-shop.scss"
import "../../../assets/scss/plugins/extensions/swiper.scss"

const mql = window.matchMedia(`(min-width: 992px)`)

class Products extends React.Component {
  state = {
    sidebarDocked: mql.matches,
    sidebarOpen: false
  }

  UNSAFE_componentWillMount() {
    // mql.addListener(this.mediaQueryChanged)
    mql.addEventListener("change", this.mediaQueryChanged);
  }

  componentWillUnmount() {
    mql.removeEventListener("change", this.mediaQueryChanged)
  }

  onSetSidebarOpen = open => {
    this.setState({sidebarOpen: open})
  }

  mediaQueryChanged = () => {
    this.setState({sidebarDocked: mql.matches, sidebarOpen: false})
  }

  render() {
    return (
      <React.Fragment>
        <Breacrumbs
          breadCrumbTitle="Товары"
          breadCrumbParent=""
          breadCrumbActive=""
        />
        <div className="ecommerce-application">
          <div
            className={`shop-content-overlay ${
              this.state.sidebarOpen ? "show" : ""
            }`}
            onClick={() => this.onSetSidebarOpen(false)}/>
          <div className="sidebar-section">
            <h6 className="filter-heading d-none d-lg-block">Фильтры</h6>
            <Sidebar
              sidebar={<ProductsSidebar fullHeight/>}
              docked={this.state.sidebarDocked}
              open={this.state.sidebarOpen}
              sidebarClassName="sidebar-shop"
              touch={true}
              contentClassName="sidebar-children d-none"
              rootClassName="sidebar-shop__container"
            >{""}</Sidebar>
          </div>
          <ProductsContent
            mainSidebar={this.onSetSidebarOpen}
            sidebar={this.state.sidebarOpen}
          />
        </div>
      </React.Fragment>
    )
  }
}

export default Products
