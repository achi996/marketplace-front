import React, {useEffect} from "react";
import {useDispatch} from "react-redux";
import {emailActivate} from "../../../../redux/slices/auth.slice";

export default function RegistrationActivation() {
  const link = window.location.search
  const search = new URLSearchParams(link)
  const uid = search.get("uid"), token = search.get("token")
  const dispatch = useDispatch()

  useEffect(() => {
    (uid && token) && dispatch(emailActivate({uid, token}))
  }, [dispatch, token, uid])
  return (
    <div>
      <h3>Подождите...</h3>
    </div>
  )
}
