import React from 'react'
import { useHistory } from 'react-router-dom';
import { Button } from 'reactstrap'

function RegisterSuccess() {
  const history = useHistory();
  const handleClose = ()=>{
    history.push(history.location.pathname);
  }

  return (
    <div>
      <h3>Вы успешно зарегистрировались!</h3>
      <br/>
      <p>Но перед тем как зайти, вам надо подтвердить вашу электронную почту.</p>
      <p>Проверте вашу почту</p>
      <br/>
      <Button className="w-100" color="primary" type="button" onClick={handleClose}>
        Ок
      </Button>
    </div>
  )
}

export default RegisterSuccess
