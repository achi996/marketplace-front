import React from "react"
import {
  Card,
  CardBody,
  Row,
  Col
} from "reactstrap"
import RegisterJWT from "./RegisterJWT"
import "../../../../assets/scss/pages/authentication.scss"
import { useSelector } from "react-redux";
import RegisterSuccess from "./RegisterSuccess";

export default function Register() {
  const authState = useSelector(state=>state.auth);

  return (
    <Card className="mb-1 bg-authentication rounded-0 mb-0 w-100">
      <Row className="m-0">
        <Col sm="12" className="p-0">
          <Card className="mb-1 rounded-0 mb-0 p-2">
            <CardBody className="pt-1 px-0 pb-50">
              {authState.registerSuccess ? (
                <RegisterSuccess/>
              ) : (
                <RegisterJWT/>
              )}
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Card>
  )
}
