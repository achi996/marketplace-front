import React, { useEffect, useState } from "react"
import { Form, FormGroup, Input, Label, Button, } from "reactstrap"
import {useFormik} from "formik";
import {useDispatch, useSelector} from "react-redux";
import {clearAuthError, registerWithEmail} from "../../../../redux/slices/auth.slice";
import FormError from "../../../../helpers/utils/FormError";
import * as Yup from 'yup';
import PasswordInput from "../../../../components/custom/Form/PasswordInput";
import { Link, useHistory } from "react-router-dom";
import CheckBoxesVuexy from "../../../../components/@vuexy/checkbox/CheckboxesVuexy";
import { Check } from "react-feather";
import Axios from "axios";
import { API_BASE_URL } from "../../../../helpers/constants";
import CustomSelect from "../../../../components/custom/CustomSelect/CustomSelect";
import ReactInputMask from "react-input-mask";

const selectOptions = [
  // { value: "", label: "Тип пользователя" },
  { value: "B", label: "Покупатель" },
  { value: "S", label: "Продавец" },
];

const userTypes = {
  "B": "Покупатель",
  "S": "Продавец"
};

const RegisterSchema = Yup.object().shape({
  email: Yup.string()
    .email("Введите правильный Email!")
    .required("Заполните это поле!"),
  phone_number: Yup.string()
    .min(13, "Введите правильный номер")
    .required("Заполните это поле!"),
  password: Yup.string()
    .min(8, "Пароль должен быть не меньше 8 символов!")
    .required("Заполните это поле!"),
  confirm_pass: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Пароли не совпадают!')
    .required("Заполните это поле!"),
  user_type: Yup.string()
    .oneOf(["S", "B"], "Выберите тип пользователя!"),
    // .required("Заполните это поле!"),
});

const fetchPrivacyPolicyUrl = async (initalUrl)=>{
  try {
    const { data: { results: [ { file } ] } } = await Axios.get(`${API_BASE_URL}/store_settings/privacy_policy/`);
    return file;
  } catch (error) {
    return initalUrl;
  }
};

const useFetchPrivacyPolicyUrl = (initalUrl)=>{
  const [url, setUrl] = useState(initalUrl);
  useEffect(()=>{
    fetchPrivacyPolicyUrl(initalUrl).then(setUrl);
  }, [initalUrl])
  return url;
}

function RegisterJWT() {

  const dispatch = useDispatch()

  const respError = useSelector(state=>state.auth.error?.type === FormError.type && state.auth.error);
  const loading = useSelector(state=>state.auth.loading);
  const history = useHistory();

  const privacyPolicyUrl = useFetchPrivacyPolicyUrl("/documents/privacy-policy.docx")

  const formik = useFormik({
    initialValues: {
      email: "",
      phone_number: "",
      password: "",
      confirm_pass: "",
      user_type: "",
    },
    onSubmit: values => {
      dispatch(registerWithEmail(values));
    },
    validationSchema: RegisterSchema,
  });

  const handleSelect = ({value})=>{
    formik.setFieldValue("user_type", value)
  };

  useEffect(() => {
    return () => {
      dispatch(clearAuthError());
    }
  }, [dispatch, history]);

  return (
    <Form action="/" onSubmit={formik.handleSubmit}>
      <h3>Регистрация</h3><br/>
      <FormGroup className="form-label-group">
        <Input
          type="email"
          placeholder="Email"
          name={"email"}
          value={formik.values.email}
          onChange={formik.handleChange}
          autoComplete="email"
          required
        />
        <Label>Email</Label>
        {formik.touched["email"] && formik.errors["email"] && (
          <p className="text-danger">{formik.errors["email"]}</p>
        )}
        {respError && respError.getField("email")}
      </FormGroup>

      <FormGroup className="form-label-group">
        <ReactInputMask
          id="phone_number"
          autoComplete="tel"
          className="form-control"
          mask="+\9\96999999999"
          name="phone_number"
          placeholder="Номер телефона"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.phone_number}
        />
        <Label>Номер телефона</Label>
        {formik.touched["phone_number"] && formik.errors["phone_number"] && (
          <p className="text-danger">{formik.errors["phone_number"]}</p>
        )}
        {respError && respError.getField("phone_number")}
      </FormGroup>
      <FormGroup className="form-label-group position-relative has-icon-left">
        <PasswordInput
          name="password"
          placeholder="Придумайте пароль"
          value={formik.values.password}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          autoComplete="new-password"
        />
        <Label>Пароль</Label>
        {formik.touched["password"] && formik.errors["password"] && (
          <p className="text-danger">{formik.errors["password"]}</p>
        )}
        {respError && respError.getField("password")}
      </FormGroup>
      <FormGroup className="form-label-group position-relative has-icon-left">
        <PasswordInput
          name="confirm_pass"
          placeholder="Повторите пароль"
          value={formik.values.confirm_pass}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          autoComplete="new-password"
        />
        <Label>Пароль</Label>
        {formik.touched["confirm_pass"] && formik.errors["confirm_pass"] && (
          <p className="text-danger">{formik.errors["confirm_pass"]}</p>
        )}
      </FormGroup>
      <FormGroup>
        <Label>Тип пользователя</Label>
        <CustomSelect
          items={selectOptions}
          placeholder={userTypes[formik.values.user_type] || "Тип пользователя..."}
          handleItemClick={handleSelect}
        />
        {formik.touched["user_type"] && formik.errors["user_type"] && (
          <p className="text-danger">{formik.errors["user_type"]}</p>
        )}
        {respError && respError.getField("user_type")}
      </FormGroup>
      <FormGroup>
        <Label>
          Вы соглашаетесь с нашей <br/>
          <a href={privacyPolicyUrl} rel="noopener noreferrer" target="_blank">
            политикой конфиденциальности
          </a>
        </Label>
        <CheckBoxesVuexy
          color="primary"
          icon={<Check className="vx-icon" size={16} />}
          label="Согласен"
          name="privacy_policy"
          checked={formik.values.privacy_policy}
          onChange={(e) => formik.setFieldValue("privacy_policy", e.target.checked)}
          required
        />
      </FormGroup>
      <Button
        disabled={loading}
        color="primary"
        type="submit"
        className="w-100"
      >
        {loading ? "Подождите..." : "Продолжить"}
      </Button>
      <p className="my-1 text-center">Уже есть аккаунт?</p>

      <Link className="w-100" to="?auth-modal=login">
        <Button
          className="w-100" 
          outline 
          color="primary" 
          type="button" 
        >
          Войти
        </Button>
      </Link>
    </Form>
  )
};

export default RegisterJWT;