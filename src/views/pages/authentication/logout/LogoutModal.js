import React from "react";
import {
  Button,
  Card,
  CardBody,
  Row,
  Col,
} from "reactstrap";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { logout } from "../../../../redux/slices/auth.slice";
// import { closeAuthModal } from "../../../../redux/slices/navbar.slice";
import "../../../../assets/scss/pages/authentication.scss";

export default function LogoutModal() {

  const dispatch = useDispatch();
  const history = useHistory();

  const handleCancel = ()=>{
    history.push(history.location.pathname);
  };
  const handleLogout = ()=>{
    dispatch(logout());
    history.push(history.location.pathname);
  };

  return (
    <Card className="mb-1 bg-authentication login-card rounded-0 mb-0 w-100">
      <Row className="m-0">
        <Col lg="12" md="12" className="p-0">
          <Card className="mb-1 rounded-0 mb-0 px-2">
            <CardBody className="px-0">
              <h3>Вы действительно хотите выйти?</h3><br/>
              <Button className="w-100 my-1" onClick={handleLogout} color="primary" outline>
                Выйти
              </Button>
              <Button className="w-100" onClick={handleCancel} color="primary">
                Отмена
              </Button>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Card>
  )
};
