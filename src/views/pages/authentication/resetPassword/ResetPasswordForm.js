import { useFormik } from 'formik';
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { Button, CardBody, Form, FormGroup, Input, Label } from 'reactstrap'
import FormError from '../../../../helpers/utils/FormError';
import { clearAuthError, resetPassword } from '../../../../redux/slices/auth.slice';
import * as Yup from 'yup';
import { Mail } from 'react-feather';


const FormSchema = Yup.object().shape({
  email: Yup.string()
    .email("Введите правильный Email!")
    .required("Заполните это поле!"),
});

function ResetPasswordForm() {
  const dispatch = useDispatch();
  const loading = useSelector(state=>state.auth.loading);
  const respError = useSelector(state=>state.auth.error?.type === FormError.type && state.auth.error);
  const history = useHistory();

  const formik = useFormik({
    initialValues: {
      email: ""
    },
    onSubmit: (values) => {
      dispatch(resetPassword(values));
    },
    validationSchema: FormSchema
  });

  useEffect(() => {
    return () => {
      dispatch(clearAuthError());
    }
  }, [dispatch, history]);

  return (

    <CardBody className="px-0">
      <h3>Восстановление пароля</h3><br />
      {respError && respError.getField("detail")}
      <Form action="/" onSubmit={formik.handleSubmit}>
        <p>Введите Email</p>
        <FormGroup className="form-label-group position-relative has-icon-left">
          <Input
            type="email"
            placeholder="Email"
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            autoComplete="email"
          />
          <div className="form-control-position">
            <Mail size={15} />
          </div>
          <Label>Email</Label>
          {formik.touched["email"] && formik.errors["email"] && (
            <p className="text-danger">{formik.errors["email"]}</p>
          )}
          {respError && respError.getField("email")}
        </FormGroup>

        <div>
          <Button
            disabled={loading}
            className="w-100"
            color="primary"
            type="submit"
          >
            {loading ? "Подождите..." : "Отправить"}
          </Button>
          <hr />
          <Link className="w-100" to="?auth-modal=login">
            <Button className="w-100" outline color="primary" type="button">
              Назад
            </Button>
          </Link>
        </div>

      </Form>
    </CardBody>
  )
}

export default ResetPasswordForm
