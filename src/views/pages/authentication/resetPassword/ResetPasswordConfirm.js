import { useFormik } from 'formik';
import React, { useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import { Button, CardBody, Form, FormGroup, Label } from 'reactstrap';
import * as Yup from 'yup';
import PasswordInput from '../../../../components/custom/Form/PasswordInput';
import FormError from '../../../../helpers/utils/FormError';
import { resetPasswordConfirm } from '../../../../redux/slices/auth.slice';


const FormSchema = Yup.object().shape({
  new_password: Yup.string()
    .min(8, "Пароль должен быть не меньше 8 символов!")
    .required("Заполните это поле!"),
  password_confirm: Yup.string()
    .oneOf([Yup.ref('new_password'), null], 'Passwords must match')
    .required("Заполните это поле!"),

});

function ResetPasswordConfirm() {
  const location = useLocation();
  const dispatch = useDispatch();
  const respError = useSelector(state=>state.auth.error?.type === FormError.type && state.auth.error);
  const loading = useSelector(state=>state.auth.loading);
  
  const { uid, token } = useMemo(() => {
    console.log("UseMemo")
    return Object.fromEntries(new URLSearchParams(location.search).entries());
  }, [location]);

  const handleSubmit = ({ new_password }) => {
    dispatch(resetPasswordConfirm({
      new_password,
      uid,
      token,
    }));
  };

  const formik = useFormik({
    initialValues: {
      new_assword: "",
      password_confirm: "",
    },
    onSubmit: handleSubmit,
    validationSchema: FormSchema,
  });

  return (
    <CardBody>
      <Form action="/" onSubmit={formik.handleSubmit}>
        <h3>Новый пароль</h3><br/>
        {respError && respError.getField("token") && (
          <div>
            {respError.getField("token")}
            <p className="text-danger">Попробуйте снова</p>
          </div>
        )}
        <FormGroup className="form-label-group position-relative has-icon-left">
          <PasswordInput
            name="new_password"
            placeholder="Придумайте новый пароль"
            value={formik.values.new_password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            autoComplete="new-password"
          />
          <Label>Пароль</Label>
          {formik.touched["new_password"] && formik.errors["new_password"] && (
            <p className="text-danger">{formik.errors["new_password"]}</p>
          )}
          {respError && respError.getField("new_password")}
        </FormGroup>

        <FormGroup className="form-label-group position-relative has-icon-left">
          <PasswordInput
            name="password_confirm"
            placeholder="Повторите пароль"
            value={formik.values.password_confirm}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            autoComplete="new-password"
          />
          <Label>Повторите пароль</Label>
          {formik.touched["password_confirm"] && formik.errors["password_confirm"] && (
            <p className="text-danger">{formik.errors["password_confirm"]}</p>
          )}
        </FormGroup>

        <Button
          disabled={loading}
          color="primary"
          type="submit"
          className="w-100"
        >
          {loading ? "Подождите..." : "Продолжить"}
        </Button>
        <hr/>
        <Link className="w-100" to="?auth-modal=reset_password">
          <Button
            className="w-100" 
            outline 
            color="primary" 
            type="button" 
          >
            Отправить снова
          </Button>
        </Link>
      </Form>
    </CardBody>
  )
}

export default ResetPasswordConfirm
