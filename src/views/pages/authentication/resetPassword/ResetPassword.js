import React from 'react'
import { useSelector } from 'react-redux';
import { Card,  Col, Row } from 'reactstrap'
import ResetPasswordForm from './ResetPasswordForm'
import ResetPasswordSuccess from './ResetPasswordSuccess';

function ResetPassword() {
  const authState = useSelector(state=>state.auth);

  return (
    <Card className="mb-1 bg-authentication login-card rounded-0 mb-0 w-100">
      <Row className="m-0">
        <Col lg="12" md="12" className="p-0">
          <Card className="mb-1 rounded-0 mb-0 px-2">
            {authState.resetPasswordSuccess ?
              <ResetPasswordSuccess/> :
              <ResetPasswordForm/>
            }
          </Card>
        </Col>
      </Row>
    </Card>
  )
}

export default ResetPassword
