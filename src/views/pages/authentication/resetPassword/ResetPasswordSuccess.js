import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Button, CardBody } from 'reactstrap'
import { clearResetPasswordSuccess } from '../../../../redux/slices/auth.slice';

function ResetPasswordSuccess() {

  const history = useHistory();
  const handleClose = ()=>{
    history.push(history.location.pathname);
  }
  const dispatch = useDispatch();

  useEffect(()=>()=>{
    dispatch(clearResetPasswordSuccess());
  },[dispatch])

  return (
    <CardBody>
      <h3>Проверте вашу почту!</h3>
      <br/>
      <p>Но перед тем как зайти, вам надо подтвердить вашу электронную почту.</p>
      <p>Проверте вашу почту</p>
      <br/>
      <Button className="w-100" color="primary" type="button" onClick={handleClose}>
        Ок
      </Button>
    </CardBody>
  )
}

export default ResetPasswordSuccess