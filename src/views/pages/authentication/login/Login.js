import React, { useEffect } from "react"
import {
  Button,
  Card,
  CardBody,
  Row,
  Col,
  FormGroup,
  Input,
  Label,
  Form
} from "reactstrap"
import { Mail } from "react-feather";
import {useFormik} from "formik";
import {useDispatch, useSelector} from "react-redux"
import { Link, useHistory } from "react-router-dom"
import * as Yup from 'yup';
import FormError from "../../../../helpers/utils/FormError"
import PasswordInput from "../../../../components/custom/Form/PasswordInput"
import { clearAuthError, loginWithEmail } from "../../../../redux/slices/auth.slice";
import "../../../../assets/scss/pages/authentication.scss";

const LoginSchema = Yup.object().shape({
  email: Yup.string()
    .email("Введите правильный Email!")
    .required("Заполните это поле!"),
  password: Yup.string()
    .required("Заполните это поле!")
});

export default function Login() {

  const dispatch = useDispatch();
  const loading = useSelector(state=>state.auth.loading);
  const respError = useSelector(state=>state.auth.error?.type === FormError.type && state.auth.error);
  const history = useHistory();

  const formik = useFormik({
    initialValues: {
      email: "",
      password: ""
    },
    onSubmit: (values) => {
      dispatch(loginWithEmail(values));
    },
    validationSchema: LoginSchema
  });

  useEffect(() => {
    return () => {
      dispatch(clearAuthError());
    }
  }, [dispatch, history]);

  return (
    <Card className="mb-1 bg-authentication login-card rounded-0 mb-0 w-100">
      <Row className="m-0">
        <Col lg="12" md="12" className="p-0">
          <Card className="mb-1 rounded-0 mb-0 px-2">
            <CardBody className="px-0">
              <h3>Вход</h3><br/>
              {respError && respError.getField("detail")}
              <Form action="/" onSubmit={formik.handleSubmit}>
                <FormGroup className="form-label-group position-relative has-icon-left">
                  <Input
                    type="email"
                    placeholder="Email"
                    name="email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    autoComplete="email"
                  />
                  <div className="form-control-position">
                    <Mail size={15}/>
                  </div>
                  <Label>Email</Label>
                  {formik.touched["email"] && formik.errors["email"] && (
                    <p className="text-danger">{formik.errors["email"]}</p>
                  )}
                  {respError && respError.getField("email")}
                </FormGroup>
                <FormGroup className="form-label-group position-relative has-icon-left">
                  <PasswordInput
                    name="password"
                    placeholder="Укажите пароль"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    autoComplete="current-password"
                  />
                  <Label>Пароль</Label>
                  {formik.touched["password"] && formik.errors["password"] && (
                    <p className="text-danger">{formik.errors["password"]}</p>
                  )}
                  {respError && respError.getField("password")}
                </FormGroup>
                <FormGroup className="d-flex justify-content-between align-items-center">
                  {/* <Checkbox
                    color="primary"
                    icon={<Check className="vx-icon" size={16}/>}
                    label="Запомнить меня?"
                  /> */}
                  <Link to="?auth-modal=reset_password" className="float-right">
                    Забыли пароль?
                  </Link>
                </FormGroup>
                <div>
                  <Button
                    disabled={loading}
                    className="w-100"
                    color="primary"
                    type="submit"
                  >
                    {loading ? "Подождите..." : "Войти"}
                  </Button>
                  <p className="my-1 text-center">Ещё нет аккаунта?</p>
                  <Link className="w-100" to="?auth-modal=register">
                    <Button className="w-100" outline color="primary" type="button">
                      Регистрация
                    </Button>
                  </Link>
                </div>
              </Form>
            </CardBody>
            {/* <div className="auth-footer">
              <div className="divider">
                <div className="divider-text">ИЛИ</div>
              </div>
              <div className="footer-btn">
                <Button.Ripple className="btn-facebook" color="">
                  <Facebook size={14}/>
                </Button.Ripple>
                <Button.Ripple className="btn-twitter" color="">
                  <Twitter size={14} stroke="white"/>
                </Button.Ripple>
                <Button.Ripple className="btn-google" color="">
                  <img src={googleSvg} alt="google" height="15" width="15"/>
                </Button.Ripple>
                <Button.Ripple className="btn-github" color="">
                  <GitHub size={14} stroke="white"/>
                </Button.Ripple>
              </div>
            </div> */}
          </Card>
        </Col>
      </Row>
    </Card>
  )
}
