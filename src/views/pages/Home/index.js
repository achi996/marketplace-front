import React, { Component } from 'react'
import {
  Row,
  Col,
  Card,
  CardBody,
  Badge,
  CardHeader,
  CardTitle
} from "reactstrap"
import {
  Star,
  Heart,
  ShoppingCart,
} from "react-feather"
import { Link } from "react-router-dom"
import Swiper from "react-id-swiper"
import { data } from './shopData'

import img1 from "../../../assets/img/slider/banner-8.jpg"
import img2 from "../../../assets/img/slider/banner-2.jpg"
import img3 from "../../../assets/img/slider/banner-3.jpg"
import img4 from "../../../assets/img/slider/banner-4.jpg"
import img5 from "../../../assets/img/slider/banner-5.jpg"

import "../../../assets/scss/plugins/forms/react-select/_react-select.scss"
import "../../../assets/scss/pages/app-ecommerce-shop-copy.scss"
import "../../../assets/scss/plugins/extensions/swiper.scss"

const data1 = data.slice(0, 3);
const data2 = data.slice(3, 6);
const data3 = data.slice(6, 9);

const params = {
  pagination: {
    el: ".swiper-pagination",
    type: "progressbar"
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev"
  }
}

class Home extends Component {

  state = {
    inCart: [],
    inWishlist: [],
    view: "grid-view"
  }

  handleAddToCart = i => {
    let cartArr = this.state.inCart
    cartArr.push(i)
    this.setState({
      inCart: cartArr
    })
  }

  handleView = view => {
    this.setState({
      view
    })
  }

  handleWishlist = i => {
    let wishlistArr = this.state.inWishlist
    if (!wishlistArr.includes(i)) wishlistArr.push(i)
    else wishlistArr.splice(wishlistArr.indexOf(i), 1)
    this.setState({
      inWishlist: wishlistArr
    })
  }

  renderProducts = (data, key="-card") => data.map((product, i) => {
    return (
      <Card key={i + key} className="ecommerce-card mb-1">
        <div className="card-content">
          <div className="item-img text-center">
            <Link to="/ecommerce/product-detail">
              <img
                className="img-fluid"
                src={product.img}
                alt={product.name}
              />
            </Link>
          </div>
          <CardBody>
            <div className="item-wrapper">
              <div className="item-rating">
                <Badge color="primary" className="badge-md">
                  <span className="mr-50 align-middle">4</span>
                  <Star size={14} />
                </Badge>
              </div>
              <div className="product-price">
                <h6 className="item-price">{product.price}</h6>
              </div>
            </div>
            <div className="item-name">
              <Link to="/ecommerce/product-detail">
                {" "}
                <span>{product.name}</span>
              </Link>
              <p className="item-company">
                By <span className="company-name">{product.by}</span>
              </p>
            </div>
            <div className="item-desc">
              <p className="item-description">{product.desc}</p>
            </div>
          </CardBody>
          <div className="item-options text-center">
            <div className="item-wrapper">
              <div className="item-rating">
                <Badge color="primary" className="badge-md">
                  <span className="mr-50 align-middle">4</span>
                  <Star size={14} />
                </Badge>
              </div>
              <div className="product-price">
                <h6 className="item-price">{product.price}</h6>
              </div>
            </div>
            <div className="wishlist" onClick={() => this.handleWishlist(i)}>
              <Heart
                size={15}
                fill={
                  this.state.inWishlist.includes(i)
                    ? "#EA5455"
                    : "transparent"
                }
                stroke={
                  this.state.inWishlist.includes(i) ? "#EA5455" : "#626262"
                }
              />
              <span className="align-middle ml-50">Wishlist</span>
            </div>
            <div className="cart" onClick={() => this.handleAddToCart(i)}>
              <ShoppingCart size={15} />
              <span className="align-middle ml-50">
                {this.state.inCart.includes(i) ? (
                  <Link to="/ecommerce/checkout" className="text-white">
                    {" "}
                    View In Cart{" "}
                  </Link>
                ) : (
                  "Add to basket"
                )}
              </span>
            </div>
          </div>
        </div>
      </Card>
    )
  })

  render() {
    return (
      <div className="ecommerce-application">
        <div className="shop-content">
          <Row>
            <Col sm="12">
              <Card className='mb-1'>
                <CardHeader>
                  <CardTitle>Новинки</CardTitle>
                </CardHeader>
                <CardBody>
                  <Swiper {...params}>
                    <div>
                      <img src={img1} alt="swiper 1" className="img-fluid" />
                    </div>
                    <div>
                      <img src={img2} alt="swiper 2" className="img-fluid" />
                    </div>
                    <div>
                      <img src={img3} alt="swiper 3" className="img-fluid" />
                    </div>
                    <div>
                      <img src={img4} alt="swiper 4" className="img-fluid" />
                    </div>
                    <div>
                      <img src={img5} alt="swiper 5" className="img-fluid" />
                    </div>
                  </Swiper>
                </CardBody>
              </Card>
            </Col>

            <h1>Популярные</h1>
            <Col sm="12">
              <div className={this.state.view}>
                {this.renderProducts(data1, "-data1")}
              </div>
            </Col>

            <h1>Новые</h1>
            <Col sm="12">
              <div className={this.state.view}>
                {this.renderProducts(data2, "-data2")}
              </div>
            </Col>

            <h1>Скидки</h1>
            <Col sm="12">
              <div className={this.state.view}>
                {this.renderProducts(data3, "-data3")}
              </div>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

export default Home
