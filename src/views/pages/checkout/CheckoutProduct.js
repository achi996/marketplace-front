import {Badge, Card, CardBody, UncontrolledCarousel} from "reactstrap";
import NumericInput from "react-numeric-input";
import {mobileStyle} from "../../forms/form-elements/number-input/InputStyles";
import {Heart, Star, X} from "react-feather";
import React, {useEffect, useMemo, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {changeItemCountInCart, removeItemFromCart, updateItemInCart,} from "../../../redux/slices/cart.slice";
import {productsSelector} from "../../../redux/slices/products.slice";
import {toggleItemInWishlist} from "../../../redux/slices/wishlist.slice";
import {getPrices} from '../../../helpers/utils/productPrices';
import classNames from "classnames";
import Axios from "axios";
import {API_BASE_URL} from "../../../helpers/constants";
import CustomSelect from "../../../components/custom/CustomSelect/CustomSelect";
import {store} from "../../..";

const getSpecs = async (category) => {
  const { data: { results: data } } = await Axios.get(`${API_BASE_URL}/store/product_specifications/?category=${category}`);
  return data.map(spec => {
    // const filteredSpecVals = spec.product_specifications_values
    //   .filter(specVal => this.props.data.product_specifications_values.includes(specVal.id));

    const product_specifications_values = spec.product_specifications_values.map(specVal => ({
      ...specVal,
      key: specVal.id,
      value: specVal.id,
      label: specVal.value,
    }));

    return {
      ...spec,
      // isSelected: this.props.data.product_specifications.includes(spec.id),
      product_specifications_values,
    }
  })    ;
};

const handleSpecItemClick = (data, spec) => (specValue) => {
  const selected_specifications = { ...data.selected_specifications };

  selected_specifications[spec.id] = {
    id: spec.id,
    name: spec.name,
    value: {
      id: specValue.key,
      name: specValue.label,
    }
  };
  store.dispatch(updateItemInCart({ slug: data.slug, selected_specifications }))
}

export default function Product({ product }) {
  const [specs, setSpecs] = useState([]);
  const isInWishlist = useSelector(state => !!productsSelector.selectById(state.wishlist, product.slug));
  const dispatch = useDispatch();
  const productOptionsImages = product.images
    .map(image => (
      {
        src: image.image,
        id: image.id,
        altText: "",
        caption: "",
        header: ""
      }
    ))

  const [
    handleRemove,
    handleAddToWishlist,
    handleChangeInCartCount,
  ] = useMemo(() => [
    () => dispatch(removeItemFromCart(product)),
    () => dispatch(toggleItemInWishlist(product)),
    (value) => {
      dispatch(changeItemCountInCart(
        {
          slug: product.slug,
          count: value < 1 ? 1 : value > product.amount ? product.amount : value
        }
      ));
    }
  ], [dispatch, product]);

  const {
    strikethroughPrice,
    actual_price,
  } = getPrices(product);

  useEffect(() => {
    getSpecs(product?.category).then(data => {
      const filteredSpecs = data
        .map(spec => {
          const filteredSpecVals = spec.product_specifications_values
            .filter(item => {
              return product.product_specifications_values.includes(item.id)
            });
          return filteredSpecVals.length ? {...spec, product_specifications_values: filteredSpecVals} : null;
        })
        .filter(Boolean);
      setSpecs(filteredSpecs);
    });
  }, [product])



  const renderSpecs = (spec) => {
    console.log(spec)
    if(!spec || !spec?.product_specifications_values?.length) return null;
    const selectValue = spec.product_specifications_values
      .find(item => {
        return item.value === product?.selected_specifications?.[spec.id]?.value?.id
      })?.label;
    return (
      <div data-value={selectValue} className="mb-1 product-spec-select__wrapper" key={`product-spec-${spec.id}`}>
        <CustomSelect
          className={"product-spec-select"}
          items={spec.product_specifications_values}
          placeholder={spec.name}
          value={selectValue}
          handleItemClick={handleSpecItemClick(product, spec)}
        />
        <p className="text-danger d-none error-message">Выберите характеристику</p>
      </div>
    );
  };


  return (
    <Card className="mb-1 ecommerce-card">
      <div className="card-content">
        <div className="slider">
          <UncontrolledCarousel items={productOptionsImages} autoPlay={false} indicators={false} />
        </div>
        <CardBody>
          <div className="item-name">
            <p>{product.title}</p>
            <p className="quantity-title mt-1">Характеристики</p>
              {specs.map(renderSpecs)}
            <p
              className={classNames(`m-0 stock-status-in`, {
                "danger": !product.in_stock
              })}
            >
              {product.in_stock ? "Есть в наличии" : "Нет в наличии"}
            </p>
            {product.wholesale &&
              <p className="m-0 stock-status-in">
                Доступно оптом
              </p>
            }
            {product.retail &&
              <p className="m-0 stock-status-in">
                Доступно в розницу
              </p>
            }
            <div className="item-quantity mt-1">

              <p className="quantity-title">Количество</p>
              <NumericInput
                min={1}
                max={product.amount}
                value={product.inCartCount}
                onChange={handleChangeInCartCount}
                mobile
                style={mobileStyle}
              />
              {product.wholesale && product.retail &&
                <p>оптом от {product.wholesale_amount}</p>
              }
            </div>
            <p className="delivery-date">{product.deliveryBy}</p>
            <p className="offers">{product.offers}</p>
          </div>
        </CardBody>
        <div className="item-options text-center">
          <div className="item-wrapper">
            <div className="item-rating">
              <Badge color="primary" className="badge-md mr-25">
                <span className="align-middle">{Math.round(product.average_rating * 10) / 10}</span>{" "}
                <Star size={15} />
              </Badge>
            </div>
            <div className="item-cost">
              {strikethroughPrice &&
                <h6
                  className={"item-price"}
                  style={{ color: "#dedede", textDecoration: "line-through" }}
                >
                  {strikethroughPrice} сом
                </h6>
              }
              <h6 className="item-price">
                {actual_price} сом
              </h6>

              <h6 className="item-price mt-1">
                Итого: {actual_price * (product.inCartCount || 1)} сом
              </h6>
            </div>
          </div>
          <div className="wishlist" onClick={handleRemove}>
            <X size={15} />
            <span className="align-middle ml-25">Убрать</span>
          </div>
          <div className="wishlist" onClick={handleAddToWishlist}>
            <Heart size={15}
              fill={
                isInWishlist
                  ? "#EA5455"
                  : "white"
              }
              stroke={
                isInWishlist ? "#EA5455" : "#fff"
              } />
            <span className="align-middle ml-25">В избранные</span>
          </div>
        </div>
      </div>
    </Card>
  )
}
