import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import BreadCrumbs from "../../../components/@vuexy/breadCrumbs/BreadCrumb";
import {Card, CardBody, Button, FormGroup, Label, Input, Col, Row} from "reactstrap";
import RadioVuexy from "../../../components/@vuexy/radio/RadioVuexy";
import {
  fetchAddresses,
  setSelfDelivered,
  setDelivered,
  setDeliveryAddress,
  setPhoneNumber,
  setUserInfo,
  setPaymentType,
  createOrder,
  setOrderComment
} from "../../../redux/slices/cart.slice";

import "../../../assets/scss/pages/app-ecommerce-shop.scss"
import "../../../assets/scss/plugins/extensions/toastr.scss"
import '../../../assets/scss/pages/checkout.scss'
import ReactInputMask from "react-input-mask";
import {productsSelector} from "../../../redux/slices/products.slice";
import {getTotalSum} from "../../../helpers/utils/productPrices";
import CustomSelect from "../../../components/custom/CustomSelect/CustomSelect";


const countryOptions = [
  {key: "Кыргызстан", label: "Кыргызстан"},
  {key: "Россия", label: "Россия"},
  {key: "Узбекистан", label: "Узбекистан"},
  {key: "Казахстан", label: "Казахстан"}
];
export default function CheckoutOrder() {
  const {
    self_deliver_addresses_list: addresses,
    self_delivered,
    self_deliver_address,
    country,
    city,
    street,
    oblast,
    house_number,
    phone_number,
    user_info,
    comment,
    payment_type,
  } = useSelector(state => state.cart);

  const cart = useSelector(state => productsSelector.selectAll(state.cart));

  const [errors, setErrors] = useState({});

  const dispatch = useDispatch();

  const handleSelfDelivered = (value) => {
    dispatch(setSelfDelivered({selfDeliverId: value}));
  };

  const handleDelivered = () => {
    dispatch(setDelivered())
  }

  useEffect(() => {
    dispatch(fetchAddresses());
  }, [dispatch]);

  const values = {country, city, street, house_number, oblast};

  const handleSubmit = () => {
    dispatch(createOrder());
  }

  const handleAddressChange = ({target: {value, name}}) => {
    dispatch(setDeliveryAddress({...values, [name]: value}));
  };

  const handlePhoneNumber = (e) => {
    dispatch(setPhoneNumber(e.target.value));
    if (e.target.value.replace("_", "").length < 13) {
      setErrors({...errors, phone_number: "Заполните номер телефона"})
    } else {
      const {phone_number, ...rest} = errors;
      setErrors(rest)
    }
  };
  const handleOrderComment = (e) => {
    dispatch(setOrderComment(e.target.value));
  };
  const handleUserInfo = (e) => {
    dispatch(setUserInfo(e.target.value));
    if (!e.target.value) {
      setErrors({...errors, user_info: "Заполните это поле"})
    } else {
      const {user_info, ...rest} = errors;
      setErrors(rest)
    }
  }

  const handleSetPaymentType = (e) => {
    dispatch(setPaymentType(e.target.value));
  }

  const handleCountrySelect = ({key: value}) => {
    dispatch(setDeliveryAddress({...values, country: value}));
  }

  const renderAddresses = (
    <Row className="d-flex flex-column my-1" style={{gridGap: "10px"}}>
      <Col sm={12}>
        <p className="m-0">C какого адреса вам удобнее забрать товар?</p>
      </Col>
      {addresses.map(item => (
        <Col
          key={`self-deliver-address-${item.id}`}
          sm="12"
          className="d-flex align-items-center"
        >
          <RadioVuexy
            value={item.id}
            label={item.address}
            name="self_deliver_address"
            checked={self_deliver_address === item.id}
            onChange={(e) => handleSelfDelivered(+e.target.value)}
          />
        </Col>
      ))}
    </Row>
  );

  const renderAddressForm = (
    <Row>
      <Col sm={12}>
        <p>Заполните ваш адрес</p>
      </Col>
      <Col md="6" sm="12">
        <FormGroup>
          <Label for="country"> Страна </Label>
          <CustomSelect
            className={"product-spec-select"}
            items={countryOptions}
            placeholder={'Выберите страну'}
            value={values.country}
            handleItemClick={handleCountrySelect}
          />
          {!values.country &&
          <p className={"text-danger"}>Это поле обязательно!</p>
          }
        </FormGroup>
      </Col>
      <Col md="6" sm="12">
        <FormGroup>
          <Label for="oblast"> Регион/Область </Label>
          <Input
            id="oblast"
            name="oblast"
            value={values.oblast}
            onChange={handleAddressChange}
          />
          {!values.oblast &&
          <p className={"text-danger"}>Это поле обязательно!</p>
          }
        </FormGroup>
      </Col>
      <Col md="6" sm="12">
        <FormGroup>
          <Label for="city"> Город/село </Label>
          <Input
            id="city"
            name="city"
            value={values.city}
            onChange={handleAddressChange}
          />
          {!values.city &&
          <p className={"text-danger"}>Это поле обязательно!</p>
          }
        </FormGroup>
      </Col>
      <Col md="6" sm="12">
        <FormGroup>
          <Label for="street">Улица</Label>
          <Input
            id="street"
            name="street"
            value={values.street}
            onChange={handleAddressChange}
          />
          {!values.street &&
          <p className={"text-danger"}>Это поле обязательно!</p>
          }
        </FormGroup>
      </Col>
      <Col md="6" sm="12">
        <FormGroup>
          <Label for="house_number"> Номер квартиры/дома </Label>
          <Input
            id="house_number"
            name="house_number"
            value={values.house_number}
            onChange={handleAddressChange}
          />
          {!values.house_number &&
          <p className={"text-danger"}>Это поле обязательно!</p>
          }
        </FormGroup>
      </Col>
    </Row>
  );

  const deliveryAddress = [country, oblast, city, street, house_number].filter(Boolean).join(", ");
  const isDisabled = (!self_delivered && (!country || !oblast || !city || !street || !house_number)) || !user_info || phone_number.replace("_", '').length < 13;

  return (

    <React.Fragment>
      <BreadCrumbs
        breadCrumbTitle="Заказ"
        breadCrumbParent={<Link to="/products">Товары</Link>}
        breadCrumbParent2={<Link to="/products">Корзина</Link>}
        breadCrumbActive="Заказ"
      />
      <div className="ecommerce-application checkout">
        <div className="list-view product-checkout">

          <div className="checkout-options">
            <Card className="mb-1">
              <CardBody>
                <p>Заполните ваши контактные данные</p>
                <Row>
                  <Col sm="12">
                    <FormGroup className="mb-0">
                      <Label for="phone_number"> Номер телефона <span className={"text-danger"}>*</span> </Label>
                      <ReactInputMask
                        className={"form-control"}
                        mask="+\9\96999999999"
                        placeholder="+996-XXX-XXX-XXX"
                        name="phone_number"
                        id="phone_number"
                        onChange={handlePhoneNumber}
                        // onBlur={formik.handleBlur}
                        value={phone_number}
                      />
                      <p className={`m-0 ${errors.phone_number ? "text-danger" : "d-none"}`}>
                        {errors.phone_number}
                      </p>
                    </FormGroup>
                  </Col>

                  <Col sm="12">
                    <FormGroup className="mb-0">
                      <Label for="user_info"> Ф.И.О. <span className={"text-danger"}>*</span></Label>
                      <Input
                        id="user_info"
                        placeholder="Оставьте комментарий..."
                        name="user_info"
                        onChange={handleUserInfo}
                        value={user_info}
                      />
                      <p className={`m-0 ${errors.user_info ? "text-danger" : "d-none"}`}>
                        {errors.user_info}
                      </p>
                    </FormGroup>
                  </Col>

                  <Col sm="12">
                    <FormGroup>
                      <Label for="comment"> Комментарий </Label>
                      <Input
                        id="comment"
                        placeholder="Оставьте комментарий..."
                        name="comment"
                        onChange={handleOrderComment}
                        value={comment}
                      />
                    </FormGroup>
                  </Col>

                  <Col sm={12}>
                    <FormGroup>
                      <p className={`mb-0`}> Вы хотите воспользоватся доставкой? </p>
                      {!self_delivered ? <span className={`danger m-0`}>Доставка оплачивается на месте</span> : ""}
                      <RadioVuexy
                        label={"Доставка"}
                        name="is_self_delivered"
                        checked={!self_delivered}
                        onChange={handleDelivered}
                      />
                      <RadioVuexy
                        label={"Я сам заберу товар"}
                        name="is_self_delivered"
                        checked={self_delivered}
                        onChange={() => handleSelfDelivered(addresses[0]?.id)}
                      />
                    </FormGroup>
                  </Col>
                </Row>

                {self_delivered ? renderAddresses : renderAddressForm}

                <Row>
                  <Col sm={12}>
                    <FormGroup>
                      <p> Выберите способ оплаты </p>
                      <RadioVuexy
                        value={"CASH"}
                        label={"Наличные"}
                        name="payment_type"
                        checked={payment_type === "CASH"}
                        onChange={handleSetPaymentType}
                      />
                      <RadioVuexy
                        value={"PAYBOX"}
                        label={"Безналиные"}
                        name="payment_type"
                        checked={payment_type === "PAYBOX"}
                        onChange={handleSetPaymentType}
                      />
                    </FormGroup>
                  </Col>
                </Row>

                <Col sm="12" className={`d-flex justify-content-center`}>
                  <Button.Ripple
                    type="submit"
                    color="primary"
                    onClick={handleSubmit}
                    disabled={isDisabled || !!Object.values(errors)?.length}
                  >
                    Оформить заказ
                  </Button.Ripple>
                </Col>
              </CardBody>
            </Card>
          </div>

          <div className="checkout-options">
            <Card className="mb-1">
              <CardBody>
                <div className="detail">
                  <div className="detail-title detail-total">Сумма заказа</div>
                  <div className="detail-amt total-amt">{getTotalSum(cart).discountedTotalPrice} сом</div>
                </div>

                <hr/>

                <div className="detail">
                  <div className="detail-title">Номер тел.</div>
                  <div className="detail-amt">{phone_number}</div>
                </div>

                <div className="detail">
                  <div className="detail-title">Ф.И.О.</div>
                  <div className="detail-amt">{user_info}</div>
                </div>

                <div className="detail">
                  <div className="detail-title">C доставкой</div>
                  <div className="detail-amt">{!self_delivered ? "Да" : "Нет"}</div>
                </div>

                <div className="detail">
                  <div className="detail-title">Адрес</div>
                  <div
                    className="detail-amt">{self_delivered ? addresses.find(a => a.id === self_deliver_address)?.address || deliveryAddress : deliveryAddress}</div>
                </div>

                <div className="detail">
                  <div className="detail-title">Комментарий</div>
                  <div className="detail-amt">{comment}</div>
                </div>

                <div className="detail">
                  <div className="detail-title">Cпособ оплаты</div>
                  <div className="detail-amt">{payment_type === "PAYBOX" ? "Безналичные" : "Наличные"}</div>
                </div>

              </CardBody>
            </Card>
          </div>
        </div>

      </div>
    </React.Fragment>
  )
}
