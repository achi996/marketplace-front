import React from "react";
import { useSelector } from "react-redux";
import Product from "./CheckoutProduct";
import { productsSelector } from "../../../redux/slices/products.slice";
import { Button, Card, CardBody } from "reactstrap";
import { getPrices, getTotalSum } from "../../../helpers/utils/productPrices";
import BreadCrumbs from "../../../components/@vuexy/breadCrumbs/BreadCrumb";
import { Link } from "react-router-dom";

import "../../../assets/scss/pages/app-ecommerce-shop.scss"
import "../../../assets/scss/plugins/extensions/toastr.scss"
import '../../../assets/scss/pages/checkout.scss'

const CheckoutProductPrices = ({ products }) => products.map(product => {
  const { actual_price } = getPrices(product);
  return (
    <div className="detail" key={product.slug}>
      <div className="detail-title">{product.title} | {actual_price} * {product.inCartCount || 1} </div>
      <div className="detail-amt">{actual_price * (product.inCartCount || 1)} сом</div>
    </div>
  )
});


export default function CheckoutCart() {
  const cart = useSelector(state => productsSelector.selectAll(state.cart));
  const {
    discountedTotalPrice
  } = getTotalSum(cart);

  const handleNext = (e)=>{
    const $inps = document.querySelectorAll(".product-spec-select__wrapper");
    let isAllInpsValidated = true
    for (let $inp of $inps){
      const val = $inp.getAttribute("data-value");
      if(!val){
        isAllInpsValidated = false;
        $inp.querySelector(".error-message").classList.remove("d-none");
      }else {
        $inp.querySelector(".error-message").classList.add("d-none");
      }
    }
    if (!isAllInpsValidated){
      e.preventDefault();
    }
  }

  return (

    <React.Fragment>
      <BreadCrumbs
        breadCrumbTitle="Корзина"
        breadCrumbParent={<Link to="/products">Товары</Link>}
        breadCrumbActive="Корзина"
      />
      <div className="ecommerce-application checkout">
        <div className="list-view product-checkout">
          <div className="checkout-items">
            {!!cart.length ? cart.map((item) => {
              return (
                <Product key={`${item.slug}-basket-product`} product={item} />
              )
            }) : "Ваша корзина пуста"}
          </div>
          <div className="checkout-options">
            <Card className="mb-1">
              <CardBody>
                <div className="price-details">
                  <p>Детали цен</p>
                </div>

                <hr />
                <CheckoutProductPrices products={cart} />
                <hr />
                <p className={'danger'}>Цена без учета доставки</p>

                <div className="detail">
                  <div className="detail-title detail-total">Итого</div>
                  <div className="detail-amt total-amt">{discountedTotalPrice} сом</div>
                </div>
                <Link className="d-block" to="/checkout/order">
                  <Button
                    onClick={handleNext}
                    color="primary"
                    className="btn-block"
                    disabled={!cart.length}
                  >
                    Оформить заказ
                  </Button>
                </Link>
              </CardBody>
            </Card>
          </div>
        </div>

      </div>
    </React.Fragment>
  )
}
