import React, { useState } from 'react'
import { ChevronDown } from 'react-feather'
import { Card, CardHeader, CardTitle } from 'reactstrap'
import BreadCrumbs from '../../../components/@vuexy/breadCrumbs/BreadCrumb'

import '../../../assets/scss/pages/categories-page.scss'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { history } from '../../../history'

function CategoriesPage() {
  const categories = useSelector(state=>state.categories.categories);
  const [selectedCategory, setSelectedCategory] = useState(null);

  const handleClick = (category) => {
    if(!category.children?.length){
      history.push(`/products?category=${category.id}`);
    }else{
      setSelectedCategory(category);
      window.scrollTo({top: 0});
    }
  }
  return (
    <React.Fragment>
      <BreadCrumbs
        breadCrumbTitle={!!selectedCategory ? selectedCategory.title : 'Категории'}
        breadCrumbParent={<Link to="/">Главная</Link>}
        breadCrumbParent2={!!selectedCategory && <Link to="/">Категории</Link>}
        breadCrumbActive={!!selectedCategory ? selectedCategory.title : 'Категории'}
      />
      <div className="ecommerce-application categories-page">

        <div className="categories-list">
          {(selectedCategory?.children || categories).map(category=>(
            <div key={category.id} className="categories-list__item">
              <Card
                onClick={() => handleClick(category)}
                className="categories-list__card"
              >
                <CardHeader>
                  <CardTitle className="lead collapse-title collapsed">
                    {category.title}
                    {!!category.children?.length && <ChevronDown className="ml-2 collapse-icon" size={15} />}
                  </CardTitle>
                </CardHeader>
              </Card>
            </div>
          ))}
            
        </div>

      </div>
    </React.Fragment>
  )
}

export default CategoriesPage
