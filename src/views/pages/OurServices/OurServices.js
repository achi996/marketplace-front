import Axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Button, Card, CardBody, Col, Row } from 'reactstrap';
import { API_BASE_URL } from '../../../helpers/constants';
import img1 from "../../../assets/img/pages/content-img-1.jpg";
import './OurServices.scss'

const fetchData = async () => {
  const { data } = await Axios.get(`${API_BASE_URL}/store_settings/services/`);
  return data.results;
}

function OurServices() {
  const [data, setData] = useState([]);
  useEffect(() => {
    fetchData().then(setData);
  }, [setData]);
  return (
    <div>
      <Row>
        {data.map(service => (
          <Col sm={6} xs={12} key={`our-services-item-${service.id}`}>
            <Card className='mb-1'>
              <CardBody>
                <Row>
                  <Col className="mb-2 mb-lg-0" lg={6} xs={12}>
                    <img src={service.logo ? service.logo : img1} alt={`${service?.name}`} className="w-100 our-services-item__img" />
                  </Col>
                  <Col lg={6} xs={12}>
                    <h5>{service?.name}</h5>
                    <p className="mb-0">{service?.description}</p>
                    <span>Elite Author</span>
                    <div className="card-btns d-flex justify-content-between mt-2">
                      {(service.link === "#" || !service.link) ?
                        <Button.Ripple color="primary" className="gradient-light-primary text-white">
                          Открыть
                        </Button.Ripple> :
                        <a href={service.link} target="_blank"
                          rel="noopener noreferrer">
                          <Button.Ripple color="primary" className="gradient-light-primary text-white">
                            Открыть
                          </Button.Ripple>
                        </a>
                      }
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        ))}
      </Row>
    </div>
  )
}

export default OurServices
