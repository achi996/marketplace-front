import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchAccountsShop} from "../../../redux/slices/accountsShop.slice";
import {Link, useRouteMatch} from "react-router-dom";
import {productsSelector} from "../../../redux/slices/products.slice";
import ProductsCard from "../../../components/custom/ProductsCard/ProductsCard";
import "../../../assets/scss/pages/app-ecommerce-shop.scss";
import "../../../assets/scss/pages/accounts-shop.scss";
import BreadCrumbs from "../../../components/@vuexy/breadCrumbs/BreadCrumb";

export default function Shop() {
  const {params: {shop_id}} = useRouteMatch()
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchAccountsShop(shop_id))
  }, [dispatch, shop_id])

  const {shopProducts, loading, logo, name, description} = useSelector(state => ({
    shopProducts: productsSelector.selectAll(state.accountsShop),
    loading: state.accountsShop.loading,
    error: state.accountsShop.error,
    logo: state.accountsShop.logo,
    name: state.accountsShop.name,
    description: state.accountsShop.description,
  }));
  if (loading) return null;

  return (
    <div className={`accounts-shop`}>
      {/* <div className={'shop-detail'}>
        <div className={'logo'}>
          <img src={logo} alt=""/>
        </div>
        <h2 className={`shop-name`}>
          {name}
        </h2>
        <p className={`shop-description`}>{description}</p>
      </div> */}
      <BreadCrumbs
        breadCrumbTitle="Детали магазина"
        breadCrumbParent={<Link to="/products">Товары</Link>}
        breadCrumbParent2="Детали магазина"
        breadCrumbActive={name}
      />
      <div className="shop-detail">
        <img src={logo} alt={name} className="float-left mr-2 shop-logo"/>
        <h1 className="shop-name">{name}</h1>
        <p className="shop-description">{description}</p>
      </div>
      <div className={`ecommerce-application`}>
        <div className={`shop-con`}>
          <div id="ecommerce-products" className={`grid-view`}>
            {shopProducts.map(product => (
              <ProductsCard disableShopAvatar data={product} key={`account-shop-products-${product.id}`}/>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}
