import React, { Suspense, lazy } from 'react'
import { Router, Switch, Route, Redirect } from 'react-router-dom'
import { history } from './history'
import { connect } from 'react-redux'
import Spinner from './components/@vuexy/spinner/Loading-spinner'
import { ContextLayout } from './utility/context/Layout'
import { $USER_ROLES } from './helpers/constants'
import QueryStringHandler from './views/pages/QueryStringHandler'
import ExtraPage from './views/pages/ExtraPage'

// Route-based code splitting
// const Home = lazy(() => import('./views/pages/Home'));
// const Page2 = lazy(() => import('./views/pages/Page2'));
//? Auth
// const login = lazy(() => import('./views/pages/authentication/login/Login'));
// const register = lazy(() => import('./views/pages/authentication/register/Register'));
const registrationActivation = lazy(() => import('./views/pages/authentication/register/RegistrationActivation'));

//? Profile
const profile = lazy(() => import('./views/pages/account'))

//? Seller
const sellerProducts = lazy(() => import('./views/apps/ecommerce/shop/Shop'))
const analyticsDashboard = lazy(() => import('./views/dashboard/analytics/AnalyticsDashboard'));
const sellerUsers = lazy(() => import('./views/apps/user/list/List'));
const sellerUser = lazy(() => import('./views/apps/user/view/View'));

const Home = lazy(() => import('./views/pages/main-page/MainPage'));
const productsPage = lazy(() => import('./views/pages/products/Products'));
const productDetailPage = lazy(() => import('./views/pages/products/ProductDetail/ProductDetail'));

const ourServices = lazy(() => import('./views/pages/OurServices/OurServices'));
const contacts = lazy(() => import('./views/pages/Contacts/contacts'));

const error404 = lazy(() => import('./views/pages/misc/error/404'))
const onDevelopment = lazy(() => import('./views/pages/misc/error/404'))

const checkoutCart = lazy(() => import('./views/pages/checkout/CheckoutCart'));
const checkoutOrder = lazy(() => import('./views/pages/checkout/CheckoutOrder'));

const shopProducts = lazy(() => import('./views/pages/shop/Shop'))

const faq = lazy(() => import('./views/pages/faq/FAQ'));

const invoice = lazy(() => import('./views/pages/account/store/StoreOrders/Invoice'));
const categoriesPage = lazy(() => import('./views/pages/categories/CategoriesPage'));

const ResetPasswordConfirmRedirect = ()=>{
  const { uid, token } = Object.fromEntries(new URLSearchParams(history.location.search).entries());
  return (
    <Redirect to={`/?auth-modal=reset_password_confirm&uid=${uid}&token=${token}`}/>
  )
};

// Set Layout and Component Using App Route
const RouteConfig = ({
  component: Component,
  fullLayout,
  permission,
  user,
  permissions,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      if (permissions && !permissions.includes(user)) {
        return <Redirect to='/' />
      }
      return (
        <ContextLayout.Consumer>
          {context => {
            let LayoutTag =
              fullLayout === true
                ? context.fullLayout
                : context.state.activeLayout === 'horizontal'
                  ? context.horizontalLayout
                  : context.VerticalLayout
            return (
              <LayoutTag {...props} permission={props.user}>
                <Suspense fallback={<Spinner />}>
                  <Component {...props} />
                </Suspense>
              </LayoutTag>
            )
          }}
        </ContextLayout.Consumer>
      )
    }}
  />
)
const mapStateToProps = state => {
  return {
    user: state.auth.userRole
  }
}

const AppRoute = connect(mapStateToProps)(RouteConfig)

class AppRouter extends React.Component {
  render() {
    return (
      // Set the directory path if you are deploying in sub-folder
      <Router history={history}>
        <Switch>
          <AppRoute
            exact
            path='/'
            component={Home}
          />
          <AppRoute
            exact
            path='/products'
            component={productsPage}
          />
          <AppRoute
            exact
            path='/account/store/invoice/:id'
            component={invoice}
          />
          <AppRoute
            exact
            path='/products/:slug'
            component={productDetailPage}
          />
          <AppRoute
            path='/seller/products'
            component={sellerProducts}
          // permissions={[$USER_ROLES.ADMIN,$USER_ROLES.SELLLER]}
          />
          <AppRoute
            path='/seller/statistics'
            component={analyticsDashboard}
            permissions={[$USER_ROLES.ADMIN, $USER_ROLES.SELLLER]}
          />
          <AppRoute
            path='/seller/clients'
            exact
            component={sellerUsers}
            permissions={[$USER_ROLES.ADMIN, $USER_ROLES.SELLLER]}
          />
          <AppRoute
            path='/seller/clients/:id'
            component={sellerUser}
            permissions={[$USER_ROLES.ADMIN, $USER_ROLES.SELLLER]}
          />
          <AppRoute
            path='/auth/activate'
            component={registrationActivation}
          />
          <AppRoute
            path='/auth/password/reset/confirm'
            component={ResetPasswordConfirmRedirect}
          />
          <AppRoute
            path='/account'
            component={profile}
            permissions={[$USER_ROLES.ADMIN, $USER_ROLES.SELLLER, $USER_ROLES.USER]}
          />


          <AppRoute
            path='/delivery'
            component={onDevelopment}
          />
          <AppRoute
            path='/services'
            component={ourServices}
          />
          <AppRoute
            path='/contacts'
            component={contacts}
          />
          <AppRoute
            path='/categories'
            component={categoriesPage}
          />
          <AppRoute
            path='/checkout'
            component={checkoutCart}
            exact
          />
          <AppRoute
            path='/checkout/order'
            component={checkoutOrder}
            exact
          />
          <AppRoute
            path='/checkout/invoice/:orderType/:id'
            component={invoice}
            exact
          />
          <AppRoute
            path='/faq'
            component={faq}
            exact
          />
          <AppRoute
            path='/shops/:shop_id'
            component={shopProducts}
          />
          <AppRoute
            path='/extra-page/:page'
            component={ExtraPage}
          />
          <AppRoute component={error404} fullLayout />
        </Switch>
        <QueryStringHandler />
      </Router>
    )
  }
}

export default AppRouter
