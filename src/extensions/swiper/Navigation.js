import React from "react"
import Swiper from "react-id-swiper"
const params = {
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev"
  },
  className: "Hello-world"
}
class NavigationSwiper extends React.Component {
  render() {
    return (
      <div className={"swiper-slide__size-auto"}>
        <Swiper {...params}>
          {this.props.imgs.map(i => (
            <div key={`${i}-banner`}>
              <img src={require(`../../assets/img/slider/banner-${i}.jpg`)} alt="swiper 1" className={`img-fluid`}/>
            </div>
          ))}
        </Swiper>
      </div>
    )
  }
}
export default NavigationSwiper
