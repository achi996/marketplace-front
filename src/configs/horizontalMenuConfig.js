import React from "react"
import {AlignLeft, Grid, Home, Layers, Phone} from "react-feather"
// import {$USER_ROLES} from "../helpers/constants"

const horizontalMenuConfig = [
  {
    id: "home",
    title: "Главная",
    type: "item",
    icon: <Home size={20}/>,
    navLink: "/",
  },
  {
    id: "Categories",
    title: "Категории",
    icon: <AlignLeft size={20}/>,
    type: "item",
    children: [],
  },
  {
    id: "Products",
    title: "Все товары",
    icon: <Grid size={20}/>,
    children: [],
    type: "item",
    navLink: "/products",
  },
  // {
  //   id: "Доставка",
  //   title: "Доставка",
  //   icon: <MapPin size={20}/>,
  //   children: [],
  //   type: "item",
  //   navLink: "/delivery",
  // },
  {
    id: "Контакты",
    title: "Контакты",
    icon: <Phone size={20}/>,
    children: [],
    type: "item",
    navLink: "/contacts",
  },
  {
    id: "Services",
    title: "Наши сервисы",
    icon: <Layers size={20}/>,
    children: [],
    type: "item",
    navLink: "/services",
  },
]

export default horizontalMenuConfig
