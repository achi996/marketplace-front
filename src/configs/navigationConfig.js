import React from "react"
import * as Icon from "react-feather"
import {$USER_ROLES} from "../helpers/constants"

const horizontalMenuConfig = [

  {
    type: "groupHeader",
    groupTitle: "Авторизация"
  },
  {
    id: "Login",
    title: "Вход",
    type: "item",
    icon: <Icon.LogIn size={20}/>,
    navLink: "?auth-modal=login",
    logedOut: true,
  }, {
    id: "Register",
    title: "Регистрация",
    type: "item",
    icon: <Icon.User size={20}/>,
    navLink: "?auth-modal=register",
    logedOut: true,
  }, {
    id: "Logout",
    title: "Выйти",
    type: "item",
    icon: <Icon.LogOut size={20}/>,
    navLink: "?auth-modal=logout",
    logedIn: true,
  },
  {
    type: "groupHeader",
    groupTitle: "Страницы"
  },
  {
    id: "home",
    title: "Главная",
    type: "item",
    icon: <Icon.Home size={20}/>,
    navLink: "/",
  },
  // {
  //   id: "Categories",
  //   title: "Категории",
  //   icon: <Icon.AlignLeft size={20}/>,
  //   children: [],
  // },
  {
    id: "Products",
    title: "Все товары",
    icon: <Icon.Grid size={20}/>,
    children: [],
    type: "item",
    navLink: "/products",
  },
  {
    id: "contacts",
    title: "Контакты",
    type: "item",
    icon: <Icon.Phone size={20}/>,
    navLink: "/contacts",
  },
  {
    id: "services",
    title: "Наши сервисы",
    type: "item",
    icon: <Icon.Layers size={20}/>,
    navLink: "/services",
  },
  {
    id: "faq",
    title: "Часто задаваемые вопросы",
    type: "item",
    icon: <Icon.Info size={20}/>,
    navLink: "/faq",
  },
  {
    id: "my-profile",
    title: "Личный кабинет",
    type: "collapse",
    icon: <Icon.Settings size={20}/>,
    permissions: [$USER_ROLES.ADMIN, $USER_ROLES.SELLLER, $USER_ROLES.USER],
    children: [
      {
        id: "account",
        title: "Мои данные",
        icon: <Icon.Settings size={20}/>,
        type: "item",
        navLink: "/account/profile",
      }, {
        id: "change-password",
        title: "Изменить пароль",
        icon: <Icon.Lock size={20}/>,
        type: "item",
        navLink: "/account/change-password",
      },
      {
        id: "wishlist",
        title: "Избранное",
        icon: <Icon.Heart size={20}/>,
        type: "item",
        navLink: "/account/wishlist",
      }, {
        id: "orders",
        title: "История покупок",
        icon: <Icon.RotateCcw size={20}/>,
        navLink: "/account/orders",
        type: "item",
      }, {
        id: "store",
        title: "Мой магазин",
        icon: <Icon.ShoppingBag size={20}/>,
        navLink: "/account/store",
        type: 'collapse',
        permissions: [$USER_ROLES.ADMIN, $USER_ROLES.SELLLER],
        children: [
          {
            id: "info",
            title: "Информация",
            icon: <Icon.Info size={20}/>,
            type: "item",
            navLink: "/account/store/info",
            permissions: [$USER_ROLES.ADMIN, $USER_ROLES.SELLLER],
          }, {
            id: "products",
            title: "Товары",
            icon: <Icon.Box size={20}/>,
            type: "item",
            navLink: "/account/store/products/list",
            permissions: [$USER_ROLES.ADMIN, $USER_ROLES.SELLLER],
          },
          {
            id: "add-product",
            title: "Добавить товар",
            type: "item",
            icon: <Icon.PlusSquare size={20}/>,
            navLink: "/account/store/products/add",
            permissions: [$USER_ROLES.ADMIN, $USER_ROLES.SELLLER],
          },
          {
            id: "edit-product",
            title: "Изменить товар",
            type: "item",
            icon: <Icon.PlusSquare size={20}/>,
            navLink: "/account/store/products/edit/:id",
            hidden: true,
            permissions: [$USER_ROLES.ADMIN, $USER_ROLES.SELLLER],
          },
          {
            id: "advance-product",
            title: "Продвинуть товар",
            type: "item",
            icon: <Icon.PlusSquare size={20}/>,
            navLink: "/account/store/products/upgrade",
            permissions: [$USER_ROLES.ADMIN, $USER_ROLES.SELLLER],
          },
          // {
          //   id: "analytics",
          //   title: "Статистика",
          //   icon: PieChart,
          //   navLink: "/analytics",
          //   withoutCard: true,
          //   permissions: [$USER_ROLES.ADMIN, $USER_ROLES.SELLLER],
          //
          // },
          {
            id: "feedbacks",
            title: "Отзывы",
            type: "item",
            icon: <Icon.MessageSquare size={20}/>,
            navLink: "/account/store/feedbacks",
          }, {
            id: "orders",
            title: "Заказы",
            type: "item",
            icon: <Icon.DollarSign size={20}/>,
            navLink: "/account/store/orders",
          }
        ]
      },
    ]
  }
]

export default horizontalMenuConfig
