import React, { useEffect, useState } from "react"
import ScrollToTop from "react-scroll-up"
import {Button, Col, Row} from "reactstrap"
import {ArrowUp} from "react-feather"
import classnames from "classnames"
import { Link } from "react-router-dom";
import Axios from "axios"
import { API_BASE_URL } from "../../../helpers/constants"

import "../../../assets/scss/components/custom/footer.scss"
import { history } from "../../../history"

const fetchPublicOffertaUrl = async (initalUrl)=>{
  try {
    const { data: { results: [ { file } ] } } = await Axios.get(`${API_BASE_URL}/store_settings/public_offer/`);
    return file;
  } catch (error) {
    return initalUrl;
  }
};

const fetchPrivacyPolicyUrl = async (initalUrl)=>{
  try {
    const { data: { results: [ { file } ] } } = await Axios.get(`${API_BASE_URL}/store_settings/privacy_policy/`);
    return file;
  } catch (error) {
    return initalUrl;
  }
};

const fetchBanner = async () => {
  try {
    const { data: { results: [ banner ] } } = await Axios.get(`${API_BASE_URL}/store_settings/banner_for_sale/`);
    return banner;
  } catch (error) {
    return null
  }
};

const useFetchBanner = ()=>{
  const [banner, setBanner] = useState(null);
  useEffect(()=>{
    fetchBanner().then(setBanner);
  },[]);
  return banner;
}

const useFetchPublicOffertaUrl = (initalUrl)=>{
  const [url, setUrl] = useState(initalUrl);
  useEffect(()=>{
    fetchPublicOffertaUrl(initalUrl).then(setUrl);
  }, [initalUrl])
  return url;
}

const useFetchPrivacyPolicyUrl = (initalUrl)=>{
  const [url, setUrl] = useState(initalUrl);
  useEffect(()=>{
    fetchPrivacyPolicyUrl(initalUrl).then(setUrl);
  }, [initalUrl])
  return url;
}

const Footer = props => {
  let footerTypeArr = ["sticky", "static", "hidden"];

  const publicOffertaUrl = useFetchPublicOffertaUrl("/documents/public-offerta.docx");
  const privacyPolicy = useFetchPrivacyPolicyUrl("/documents/privacy-policy.docx");
  const banner = useFetchBanner();

  return (
    <footer
      className={classnames("footer footer-light noprint pt-0", {
        "footer-static": props.footerType === "static" || !footerTypeArr.includes(props.footerType),
        "d-none": props.footerType === "hidden"
      })}
    >

      <div className={`cus-footer d-flex flex-column`}>
        {!!banner &&
          <div style={{backgroundImage: `url('${banner.image}')`}} className="footer-banner__wrapper">
            <div className="footer-banner">
              <h3 className="footer-banner__title">{banner.title}</h3>
              <p style={{maxWidth: "70ch"}} className="footer-banner__content" dangerouslySetInnerHTML={{__html: banner.content}}></p>
              <Button
                color="primary"
                className="mt-2"
                onClick={()=>history.push("/?auth-modal=register")}
              >
                Начать продавать
              </Button>
            </div>
          </div>
        }
        <div className={`footer-row footer-license`}>
          <Row className="justify-content-center">
            <Col style={{maxWidth: "1100px"}}>
            <Row className="justify-content-center">
            <Col md={4}>
              <h4 className="text-white">О торговой площадке</h4>
              <ul className="list-unstyled">
                <li>
                  <Link to="/extra-page/about_us">О нас</Link>
                </li>
                <li>
                  <Link to="/contacts">Контакты</Link>
                </li>
                <li>
                  <Link to="/services">Наши сервисы</Link>
                </li>
                <li>
                  <Link to="/extra-page/partner_program">Партнерская программа</Link>
                </li>
                <li>
                  <Link to="/extra-page/vacancy">Вакансии</Link>
                </li>
              </ul>
            </Col>
            <Col md={4}>
              <h4 className="text-white">Покупателям и продавцам</h4>
              <ul className="list-unstyled">
                <li>
                  <Link to="/extra-page/cooperation">Приглашение к сотрудничеству</Link>
                </li>
                <li>
                  <Link to="/extra-page/delivery_info">Доставка</Link>
                </li>
                <li>
                  <Link to="/faq">Часто задаваемые вопросы (FAQ)</Link>
                </li>
              </ul>
            </Col>
            <Col md={4}>
              <h4 className="text-white">Правовая информация</h4>
              <ul className="list-unstyled">
                <li>
                  <a href={publicOffertaUrl} rel="noopener noreferrer" target="_blank">Публичная оферта</a>
                </li>
                <li></li>
                  <a href={privacyPolicy} rel="noopener noreferrer" target="_blank">Политика конфиденциальности</a>
              </ul>
            </Col>
          </Row>
          </Col>
          </Row>
          <p className={`m-0 mt-2 text-center`}>© 2021 Doidoi.kg Все права защищены.</p>
        </div>
      </div>

      {!props.hideScrollToTop && (
        <ScrollToTop style={{zIndex: '99'}} showUnder={160}>
          <Button color="primary" className="btn-icon scroll-top scroll-top-btn">
            <ArrowUp size={15}/>
          </Button>
        </ScrollToTop>
      )}
    </footer>
  )
}

export default Footer
