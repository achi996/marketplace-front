import React, {useCallback} from 'react'
import * as Icon from "react-feather"
import {useDispatch, useSelector} from 'react-redux'
import { Link } from 'react-router-dom'
import {Badge, DropdownMenu, DropdownToggle, Row, UncontrolledDropdown} from 'reactstrap'

import { productsSelector } from '../../../redux/slices/products.slice'

import '../../../assets/scss/components/custom/bottom-navbar.scss'
import PerfectScrollbar from "react-perfect-scrollbar";
import NavbarDropdownProductsList from "./NavbarDropdownProductsList";
import {removeItemFromWishlist} from "../../../redux/slices/wishlist.slice";

function BottomNavBar() {
  const cart = useSelector(state=>productsSelector.selectAll(state.cart));
  const userData = useSelector(state => state.auth.userData);
  const wishlist = useSelector(state=>productsSelector.selectAll(state.wishlist));
  const dispatch = useDispatch();
  const handleRemove = useCallback((item)=>{
    dispatch(removeItemFromWishlist(item));
  },[dispatch]);


  return (
    <div className="bottom-navbar bg-primary-gradient">
      <Row className="m-0 justify-content-between">
        <Link className="" to="/">
          <div className="bottom-navbar__btn">
            <Icon.Home size={30}/>
          </div>
        </Link>
        <Link className="" to="/categories">
          <div className="bottom-navbar__btn">
            <Icon.Grid size={30}/>
          </div>
        </Link>
        <Link className="" to="/checkout">
          <div className="bottom-navbar__btn">
            <span className="position-relative">
              <Icon.ShoppingCart size={30}/>
              {!!cart.length &&
                <Badge pill color="primary" className="badge-up">
                  {cart.length}
                </Badge>
              }
            </span>
          </div>
        </Link>
        <Link className="" to={userData ? '/account/profile' : '?auth-modal=login'}>
          <div className="bottom-navbar__btn">
            {userData ?
              <Icon.User size={30}/> :
              <Icon.LogIn size={30}/>
            }
          </div>
        </Link>
        <UncontrolledDropdown
          tag="div"
          className="dropdown-notification dropdown-notification-bottom  nav-item"
        >
          <DropdownToggle tag="a" className="nav-link position-relative p-0">
            <div className="bottom-navbar__btn">
            <span className="position-relative">
              <Icon.Heart size={30} />
              {wishlist.length > 0 ? (
                <Badge pill color="primary" className="badge-up">
                  {wishlist.length}
                </Badge>
              ) : null}
            </span>
            </div>
          </DropdownToggle>
          <DropdownMenu
            tag="ul"
            right
            style={{minWidth: 300, maxHeight: "80vh", overflow: "auto"}}
            className={`dropdown-menu-media dropdown-cart ${wishlist.length === 0 ? "empty-cart" : ""}`}
          >
            <li
              className={`dropdown-menu-header ${wishlist.length === 0 ? "d-none" : "d-block"}`}
            >
              <div className="dropdown-header m-0">
                <h3 className="white">
                  К-во товаров: {wishlist.length}
                </h3>
                <span className="notification-title">Избранное</span>
              </div>
            </li>
            <PerfectScrollbar
              className="media-list overflow-hidden position-relative"
              options={{
                wheelPropagation: false
              }}
            >
              <NavbarDropdownProductsList data={wishlist} handleRemove={handleRemove}/>
            </PerfectScrollbar>
            <li
              className={`empty-cart ${wishlist.length > 0 ? "d-none" : "d-block"} p-2`}
            >
              Список избранных пуст
            </li>
          </DropdownMenu>
        </UncontrolledDropdown>
      </Row>
    </div>
  )
}

export default BottomNavBar
