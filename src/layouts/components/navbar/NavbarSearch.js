import {Button, InputGroup, InputGroupAddon} from "reactstrap";
import React, {useEffect, useState} from "react";
import Autocomplete from "../../../components/@vuexy/autoComplete/AutoCompleteComponent";
import Axios from "axios";
import {API_BASE_URL} from "../../../helpers/constants";
import {useDispatch} from "react-redux";
import {setQueryParam} from "../../../redux/slices/query.slice";
import {useHistory} from "react-router-dom";
import { Search } from "react-feather";

async function fetchDataByAC(search) {
  try {
    const {data} = await Axios.get(`${API_BASE_URL}/store/products_auto_complete/?search=${search}`)
    return {data, error: false}
  } catch (error) {
    return {data: null, error}
  }
}

export default function NavbarSearch() {
  const [suggestions, setSuggestions] = useState([])
  const [ACValue, setACValue] = useState('')
  const history = useHistory();
  const dispatch = useDispatch();
  useEffect(() => {
    ACValue && fetchDataByAC(ACValue)
      .then(data => {
        const rewrittenSuggestion = data.data.results.map(product => ({...product, link: `/products/${product.slug}/`}))
        dispatch(setQueryParam({key: "search", value: ACValue}))
        setSuggestions(rewrittenSuggestion)
      })
  }, [ACValue, dispatch])

  const onChangeAC = e => {
    setACValue(e.target.value)
  }

  const onClickSearch = () => {
    history.push('/products');
    dispatch(setQueryParam({key: 'search', value: ACValue}));
  }

  return (
    <InputGroup className="col-md-6">
      <div className={`col-10 p-0`}>
        <Autocomplete suggestions={suggestions} onChange={onChangeAC} placeholder={'Поиск . . .'} filterKey={'title'}
                      suggestionLimit={5}/>
      </div>
      <InputGroupAddon addonType="append" className={`col-2 p-0`}>
        <Button style={{ padding: '10px' }} type='submit' onClick={onClickSearch} color="primary">
          <Search size={20}/>
        </Button>
      </InputGroupAddon>
    </InputGroup>
  )
}
