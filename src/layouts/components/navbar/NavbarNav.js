import React from "react";
import NavbarCart from "./NavbarCart";
// import NavbarSearch from "./NavbarSearch";
import NavbarUser from "./NavbarUser";
import NavbarWishList from "./NavbarWishList";

export default function NavbarNav(props) {
  return (
    <ul className="nav navbar-nav navbar-nav-user float-right">
      <NavbarCart/>
      <NavbarWishList />
      <NavbarUser/>
    </ul >
  )
}
