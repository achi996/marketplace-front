import React from "react"
import { Media } from "reactstrap"
import * as Icon from "react-feather"
import { history } from "../../../history"
import { getPrices } from "../../../helpers/utils/productPrices";

function NavbarDropdownProductsList({ data, handleRemove }) {
  return data.map(item => {
    return (
      <div className="cart-item" key={item.slug}>
        <Media
          className="p-0"
          onClick={() => history.push(`/products/${item.slug}`)}
        >
          <Media className="text-center pr-0 mr-1" left>
            <img
              className={"basket-item-img"}
              src={item.images[0]?.image}
              width="100"
              alt="dropdown products Item"
            />
          </Media>
          <Media className="overflow-hidden pr-1 py-1 pl-0" body>
            <span className="item-title text-truncate text-bold-500 d-block mb-50">
              {item.title}
            </span>
            <span className="item-desc font-small-2 text-truncate d-block">
              {item.description}
            </span>
            <div className="d-flex justify-content-between align-items-center mt-1">
              <span className="align-middle d-block">{item.inCartCount || 1} x {getPrices(item).actual_price}</span>
              <Icon.X
                className="danger"
                size={15}
                onClick={e => {
                  e.stopPropagation()
                  handleRemove(item)
                }}
              />
            </div>
          </Media>
        </Media>
      </div>
    )
  });
}

export default NavbarDropdownProductsList
