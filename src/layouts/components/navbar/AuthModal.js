import React, { useEffect } from 'react'
import {
  Modal,
  ModalHeader,
  ModalBody,
} from "reactstrap"
import { useDispatch, useSelector } from "react-redux";
import { closeAuthModal, toggleAuthModal } from '../../../redux/slices/navbar.slice';
import { history } from '../../../history';
import AUTH_MODALS from './authModals';

function AuthModal() {
  const authModal = useSelector( state => state.navbar.authModal );
  const dispatch = useDispatch();
  const search = history.location.search;
  
  const toggle = ()=>{
    history.push(history.location.pathname);
  };

  useEffect(()=>{
    const queryAuthModal = new URLSearchParams(search).get("auth-modal");
    const modal = AUTH_MODALS[queryAuthModal];
    
    if(!queryAuthModal) dispatch( closeAuthModal() );
    if(modal && authModal !== queryAuthModal) dispatch( toggleAuthModal(modal.name) );
    
  },[search, authModal, dispatch]);

  const modal = AUTH_MODALS[authModal];

  return (
    <Modal
      isOpen={!!authModal}
      toggle={toggle}
      className="modal-dialog-centered"
    >
      <ModalHeader toggle={toggle}>
      </ModalHeader>
      <ModalBody>
        {modal && <modal.component/>}
      </ModalBody>
    </Modal>
  )
}

export default AuthModal
