import Login from "../../../views/pages/authentication/login/Login";
import LogoutModal from "../../../views/pages/authentication/logout/LogoutModal";
import Register from "../../../views/pages/authentication/register/Register";
import ResetPassword from "../../../views/pages/authentication/resetPassword/ResetPassword";
import ResetPasswordConfirm from "../../../views/pages/authentication/resetPassword/ResetPasswordConfirm";

const AUTH_MODALS = {
  "login": {
    name: "login",
    component: Login,
  },
  "register": {
    name: "register",
    component: Register,
  },
  "logout": {
    name: "logout",
    component: LogoutModal,
  },
  "reset_password": {
    name: "reset_password",
    component: ResetPassword,
  },
  "reset_password_confirm": {
    name: "reset_password_confirm",
    component: ResetPasswordConfirm,
  },
};

export default AUTH_MODALS;