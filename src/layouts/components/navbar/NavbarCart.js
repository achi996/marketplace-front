import React, { useCallback } from "react"
import {
  UncontrolledDropdown,
  // Dropdown,
  DropdownMenu,
  // DropdownItem,
  DropdownToggle,
  Badge,
} from "reactstrap"
import PerfectScrollbar from "react-perfect-scrollbar"
import * as Icon from "react-feather"
import { history } from "../../../history"
import { useDispatch, useSelector } from "react-redux"
import { productsSelector } from "../../../redux/slices/products.slice"
import classNames from "classnames"
import { removeItemFromCart, toggleDropdown } from "../../../redux/slices/cart.slice"
import NavbarDropdownProductsList from "./NavbarDropdownProductsList"

function NavbarCart() {
  const cart = useSelector(state=>productsSelector.selectAll(state.cart));
  const isOpen = useSelector(state=>state.cart.dropdownIsOpen);
  const dispatch = useDispatch();

  const toggle = ()=>{
    dispatch(toggleDropdown());
  }

  const handleRemove = useCallback((item)=>{
    dispatch(removeItemFromCart(item));
  },[dispatch]);

  const handleCheckout = useCallback(() =>{
    history.push("/checkout");
    dispatch(toggleDropdown());
  }, [dispatch]);

  return (

    <UncontrolledDropdown
      tag="li"
      className="dropdown-notification nav-item"
      isOpen={isOpen}
      toggle={toggle}
    >
      <DropdownToggle tag="a" className="nav-link position-relative">
        <Icon.ShoppingCart size={21} />
        {cart.length > 0 ? (
          <Badge pill color="primary" className="badge-up">
            {cart.length}
          </Badge>
        ) : null}
      </DropdownToggle>
      <DropdownMenu
        tag="ul"
        right
        className={`dropdown-menu-media dropdown-cart ${cart.length === 0 ? "empty-cart" : ""}`}
      >
        <li
          className={`dropdown-menu-header ${cart.length === 0 ? "d-none" : "d-block"}`}
        >
          <div className="dropdown-header m-0">
            <h3 className="white">
              К-во товаров: {cart.length}
          </h3>
            <span className="notification-title">в вашей корзине</span>
          </div>
        </li>
        <PerfectScrollbar
          className="media-list overflow-hidden position-relative"
          options={{
            wheelPropagation: false
          }}
        >
          <NavbarDropdownProductsList data={cart} handleRemove={handleRemove} />
        </PerfectScrollbar>
        <li
          className={classNames(`dropdown-menu-footer border-top`, cart.length === 0 ? "d-none" : "d-block")}
        >
          <div
            className="dropdown-item p-1 text-center text-primary"
            onClick={handleCheckout}
          >
            <Icon.ShoppingCart size={15} />
            <span className="align-middle text-bold-600 ml-50">
              Оформить
          </span>
          </div>
        </li>
        <li
          className={classNames(`empty-cart p-2`, cart.length > 0 ? "d-none" : "d-block")}
        >
          Ваша корзина пуста
      </li>
      </DropdownMenu>
    </UncontrolledDropdown>
  )
}

export default NavbarCart
