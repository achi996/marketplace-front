import React, { useCallback } from "react"
import {
  UncontrolledDropdown,
  DropdownMenu,
  DropdownToggle,
  Badge,
} from "reactstrap"
import PerfectScrollbar from "react-perfect-scrollbar"
import * as Icon from "react-feather"
import { useDispatch, useSelector } from "react-redux"
import { productsSelector } from "../../../redux/slices/products.slice"
import NavbarDropdownProductsList from "./NavbarDropdownProductsList"
import { removeItemFromWishlist } from "../../../redux/slices/wishlist.slice"

function NavbarWishList() {
  const wishlist = useSelector(state=>productsSelector.selectAll(state.wishlist));
  const dispatch = useDispatch();
  const handleRemove = useCallback((item)=>{
    dispatch(removeItemFromWishlist(item));
  },[dispatch]);

  return (

    <UncontrolledDropdown
      tag="li"
      className="dropdown-notification nav-item"
    >
      <DropdownToggle tag="a" className="nav-link position-relative">
        <Icon.Heart size={21} />
        {wishlist.length > 0 ? (
          <Badge pill color="primary" className="badge-up">
            {wishlist.length}
          </Badge>
        ) : null}
      </DropdownToggle>
      <DropdownMenu
        tag="ul"
        right
        className={`dropdown-menu-media dropdown-cart ${wishlist.length === 0 ? "empty-cart" : ""}`}
      >
        <li
          className={`dropdown-menu-header ${wishlist.length === 0 ? "d-none" : "d-block"}`}
        >
          <div className="dropdown-header m-0">
            <h3 className="white">
              К-во товаров: {wishlist.length}
          </h3>
            <span className="notification-title">Избранное</span>
          </div>
        </li>
        <PerfectScrollbar
          className="media-list overflow-hidden position-relative"
          options={{
            wheelPropagation: false
          }}
        >
          <NavbarDropdownProductsList data={wishlist} handleRemove={handleRemove}/>
        </PerfectScrollbar>
        <li
          className={`empty-cart ${wishlist.length > 0 ? "d-none" : "d-block"} p-2`}
        >
          Список избранных пуст
      </li>
      </DropdownMenu>
    </UncontrolledDropdown>
  )
}

export default NavbarWishList
