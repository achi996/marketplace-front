import React, { useEffect } from "react"
import { Navbar, NavItem, NavLink } from "reactstrap"
import classnames from "classnames"
import NavbarNav from "./NavbarNav"
import AuthModal from "./AuthModal"
import "../../../assets/scss/components/custom/navbar/navbar.scss"
import { Link } from "react-router-dom"
import * as Icon from "react-feather";
import logo from '../../../assets/img/logo/logo.png';
import NavbarSearch from "./NavbarSearch";

const ThemeNavbar = props => {
  const colorsArr = ["primary", "danger", "success", "info", "warning", "dark"];
  const navbarTypes = ["floating", "static", "sticky", "hidden"];


  // const [windowWidth, setWindowWidth] = useState(null);

  // const updateWidth = useCallback(
  //   () => {
  //     setWindowWidth(window.innerWidth);
  //   },
  //   [setWindowWidth],
  // );

  useEffect(() => {
    if (window !== undefined) {
      const navbar = document.querySelector('.header-navbar');
      let last_scroll_top = 0;
      function handleScroll() {
        let scroll_top = window.scrollY;
        if (scroll_top < last_scroll_top) {
          navbar.classList.remove('scrolled-down');
          navbar.classList.add('scrolled-up');
        }
        else {
          navbar.classList.remove('scrolled-up');
          navbar.classList.add('scrolled-down');
        }
        last_scroll_top = scroll_top;
      }
      window.addEventListener('scroll', handleScroll);
      return () => {
        window.removeEventListener('scroll', handleScroll);
      }
    }
  }, []);

  return (
    <React.Fragment>
      <div className="content-overlay noprint" />
      <div className="header-navbar-shadow noprint" />
      <Navbar
        className={classnames(
          "header-navbar navbar-expand-lg navbar navbar-with-menu noprint m-0 w-100",
          {
            "navbar-light": props.navbarColor === "default" || !colorsArr.includes(props.navbarColor),
            "navbar-dark": colorsArr.includes(props.navbarColor),
            "bg-primary":
              props.navbarColor === "primary" && props.navbarType !== "static",
            "bg-danger":
              props.navbarColor === "danger" && props.navbarType !== "static",
            "bg-success":
              props.navbarColor === "success" && props.navbarType !== "static",
            "bg-info":
              props.navbarColor === "info" && props.navbarType !== "static",
            "bg-warning":
              props.navbarColor === "warning" && props.navbarType !== "static",
            "bg-dark":
              props.navbarColor === "dark" && props.navbarType !== "static",
            "d-none": props.navbarType === "hidden" && !props.horizontal,
            "floating-nav":
              (props.navbarType === "floating" && !props.horizontal) || (!navbarTypes.includes(props.navbarType) && !props.horizontal),
            "navbar-static-top":
              props.navbarType === "static" && !props.horizontal,
            "fixed-top": props.navbarType === "sticky" || props.horizontal,
            "scrolling": props.horizontal && props.scrolling
          }
        )}
      >
        <div className="navbar-wrapper">
          <div className="navbar-container content">
            <div
              className="navbar-collapse custom-navbar d-flex justify-content-between align-items-center"
              id="navbar-mobile"
            >
              <div className="d-flex align-items-center">

                <ul className="navbar-nav d-lg-none">
                  <NavItem className="mobile-menu mr-auto">
                    <NavLink
                      className="nav-menu-main menu-toggle hidden-xs is-active pl-0"
                      onClick={props.sidebarVisibility}>
                      <Icon.Menu className="ficon" />
                    </NavLink>
                  </NavItem>
                </ul>
                <div className="logo ">
                  <Link to="/" className="d-block">
                    {/* <div className="brand-logo mr-50"></div> */}
                    <h2 className="text-primary brand-text mb-0 d-flex align-items-center">
                      <img src={logo} alt="doidoi shop logo" height="46px" />
                      {/* <span className="ml-1">Doidoi</span> */}
                    </h2>
                  </Link>
                </div>
              </div>
              <NavbarSearch />
              {props.horizontal ? (
                <NavbarNav
                  handleAppOverlay={props.handleAppOverlay}
                  changeCurrentLang={props.changeCurrentLang}
                />
              ) : null}
            </div>
          </div>
        </div>
      </Navbar>
      <AuthModal />
    </React.Fragment>
  )
}


export default ThemeNavbar
