import React, { useState } from "react"
import {
  DropdownMenu,
  DropdownItem,
  ModalBody,
  Modal,
  Button,
  UncontrolledDropdown,
  DropdownToggle,
} from "reactstrap"
import * as Icon from "react-feather"
import { history } from "../../../history"
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../../redux/slices/auth.slice"

const NavbarUser = ({ userName }) => {
  const dispatch = useDispatch();
  const [openConfirmLogOut, setConfirmLogout] = useState(false)
  const userData = useSelector(state => state.auth.userData);
  const handleOnLogout = () => {
    dispatch(logout())
    setConfirmLogout(false)
  };
  const handleOnLogin = () => {
    // dispatch(toggleLoginModal());
    history.push(history.location.pathname+"?auth-modal=login");
  }
  const handleOnRegister = () => {
    // dispatch(toggleRegisterModal());
    history.push(history.location.pathname+"?auth-modal=register");
  };

  return (
    <UncontrolledDropdown tag="li" className="dropdown-user nav-item">
      <DropdownToggle tag="a" className="nav-link">
        {/* <div className="user-nav d-sm-flex d-none">
                <span className="user-name text-bold-600">
                  {userName}
                </span>
              </div> */}
        {userData ? (
          <Icon.User size={21} />
        ) : (
          <Icon.LogIn size={21} />
        )}
      </DropdownToggle>
      {userData ? (
        <DropdownMenu right>
          <br />
          <span>{userName}</span>
          <br />
          <br />
          <DropdownItem tag="a" onClick={() => history.push('/account')}>
            <Icon.User size={14} className="mr-50" />
            <span className="align-middle">Мой профиль</span>
          </DropdownItem>

          <DropdownItem tag="div" onClick={() => history.push('/checkout')}>
            <Icon.ShoppingCart size={14} className="mr-50" />
            <span className="align-middle">Корзина</span>
          </DropdownItem>

          <DropdownItem tag="a" onClick={() => history.push('/account/store')}>
            <Icon.Box size={14} className="mr-50" />
            <span className="align-middle">Мой магазин</span>
          </DropdownItem>
          <DropdownItem divider />
          <DropdownItem
            tag="a"
            href="#"
            onClick={() => setConfirmLogout(true)}
          >
            <Icon.LogOut size={14} className="mr-50" />
            <span className="align-middle">Выйти</span>
          </DropdownItem>
          <Modal
            isOpen={openConfirmLogOut}
            toggle={() => setConfirmLogout(!openConfirmLogOut)}
            className="modal-dialog-centered"
          >
            <ModalBody>
              <p>Вы уверены что хотите выйти?</p>
              <div className={`d-flex justify-content-end`}>
                <Button onClick={handleOnLogout} color="primary" outline>
                  выйти
              </Button>
                <Button onClick={() => setConfirmLogout(false)} className={`ml-1`} color="primary">
                  Отменить
              </Button>
              </div>
            </ModalBody>
          </Modal>
        </DropdownMenu>
      ) : (
        <DropdownMenu right>
          <DropdownItem tag="a" onClick={handleOnLogin}>
            <Icon.LogIn size={14} className="mr-50" />
            <span className="align-middle">Вход</span>
          </DropdownItem>

          <DropdownItem tag="a" onClick={handleOnRegister}>
            <Icon.User size={14} className="mr-50" />
            <span className="align-middle">Регистрация</span>
          </DropdownItem>
        </DropdownMenu>
      )}
    </UncontrolledDropdown>
  );
};

export default NavbarUser;
