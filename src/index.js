import React, { Suspense, lazy } from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import { Layout } from "./utility/context/Layout"
import * as serviceWorker from "./serviceWorker"
// import { store } from "./redux/storeConfig/store"
import Spinner from "./components/@vuexy/spinner/Fallback-spinner"
import configureStore from './redux/store';
import { ToastContainer } from "react-toastify"
import { PersistGate } from 'redux-persist/integration/react';
import { CategoriesDrawer } from "./components/custom/CategoriesDrawer/CategoriesDrawer"

import 'react-toastify/dist/ReactToastify.css';
import "./index.scss"
import "./@fake-db"

const LazyApp = lazy(() => import("./App"))

export const { 
  store, 
  persistor
} = configureStore({});

// configureDatabase()
ReactDOM.render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <CategoriesDrawer>
          <Suspense fallback={<Spinner />}>
            <Layout>
                <LazyApp />
            </Layout>
          </Suspense>
        </CategoriesDrawer>
        <ToastContainer/>
      </PersistGate>
    </Provider>,
  document.getElementById("root")
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
