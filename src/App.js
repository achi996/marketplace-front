import React, { useEffect } from "react"
import Router from "./Router"
import { history } from "./history"
import "./components/@vuexy/rippleButton/RippleButton"

import "swiper/css/swiper.css"
import "react-perfect-scrollbar/dist/css/styles.css"
import "prismjs/themes/prism-tomorrow.css"

const App = () => {
  useEffect(() => {
    return history.listen(() => { 
      window.scrollTo({top: 0})
    }) 
 },[])
  return <Router />
}


export default App
