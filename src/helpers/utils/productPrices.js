export const getPrices = ({ 
  wholesale, 
  retail, 
  wholesale_amount, 
  old_retail_price, 
  // new_retail_price,
  actual_retail_price,
  actual_wholesale_price,
  // extra_charge_retail,
  // extra_charge_wholesale,
  wholesale_price,
  inCartCount = 1,
}) => {
  const result = {
    wholesale,
    retail,
    strikethroughPrice: null,
    viewPrice: 0,
    wholesale_price,
    actual_wholesale_price,
    actual_price: 0,
  };
  if(retail){
    result.strikethroughPrice = old_retail_price && old_retail_price !== "0.00" ? 
      old_retail_price : 
      null;
    result.viewPrice = actual_retail_price;
    result.actual_price = wholesale && (inCartCount >= wholesale_amount) ? 
      actual_wholesale_price :
      actual_retail_price;
  } else if(wholesale){
    result.viewPrice = actual_wholesale_price;
    result.actual_price = actual_wholesale_price;
  };

  return result;
};

export const getTotalSum = (products) => products.reduce((a, b) => {
  const { actual_price, strikethroughPrice } = getPrices(b);
  return {
    regTotalPrice: a.regTotalPrice + (b.inCartCount || 1) * (strikethroughPrice || actual_price),
    discountedTotalPrice: a.discountedTotalPrice + (b.inCartCount || 1) * actual_price,
  }
}, {
  regTotalPrice: 0,
  discountedTotalPrice: 0
});