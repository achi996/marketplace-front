// First we need to import axios.js
import Axios from 'axios';
// import { history } from '../../history';
// import { history } from '../../history';
import {API_BASE_URL} from '../constants';
// Next we make an 'instance' of it
const authAxios = Axios.create({
// .. where we make our configurations
  baseURL: API_BASE_URL,
  // headers: {
  //   "Autho"
  // }
});

// Where you would set stuff like your 'Authorization' header, etc ...
// instance.defaults.headers.common['Authorization'] = 'AUTH TOKEN FROM INSTANCE';

// Also add/ configure interceptors && all the other cool stuff

// instance.interceptors.request...

authAxios.interceptors.request.use(
  config => {
    const token = window.localStorage.getItem("token.access");
    if (token && token !== "") {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  error => Promise.reject(error)
);

authAxios.interceptors.response.use(
  resp => resp,
  async (error) => {
    if (error?.response?.status === 401) {
      const refresh = window.localStorage.getItem("token.refresh");
      if (!refresh) {
        window.localStorage.removeItem("token.refresh");
        window.localStorage.removeItem("token.access");
        // history.push("?auth-modal=login");
        return Promise.reject(error);
      }
      try {
        const {data: {access}} = await Axios.post(`${API_BASE_URL}/auth/jwt/refresh/`, {
          refresh
        });
        window.localStorage.setItem("token.access", access);
        const resp = await authAxios(error.config);
        return resp;
      } catch (e) {
        window.localStorage.removeItem("token.refresh");
        window.localStorage.removeItem("token.access");
        // history.push("?auth-modal=login");
        return Promise.reject(e);
      }
    }
    console.log(error);
    return Promise.reject(error);
  }
)

export default authAxios;
