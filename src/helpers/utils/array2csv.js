const array2csv = (data)=>{
  if(!Array.isArray(data)) throw new Error("\"data\" must be Array!");
  if(!data.length) throw new Error("\"data\" is empty!");
  const replacer = (_, value) => value === null ? '' : value // specify how you want to handle null values here
  const header = Object.keys(data[0])
  
  const csv = [
    header.join(','), // header row first
    ...data.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','))
  ].join('\r\n');

  const url = window.URL.createObjectURL(new Blob([csv], {type: 'text/csv'}));

  return {url, csv};
}

export default array2csv;