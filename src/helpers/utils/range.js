class CustomRange{
  constructor(from,to){
    this.generator = CustomRange.createGenerator(from,to);
    this.from = from;
    this.to = to;
  }

  static createGenerator = function* (a,b){
    if(b==null){
      b=a;
      a=0;
    }
    for(let i = a; i!==b; i=a>b?i-1:i+1){
      yield i;
    }
    yield b;
  }

  map(cb){
    let a = this.generator.next();
    const arr = []
    while(!a.done){
      arr.push(cb(a.value, this));
      a = this.generator.next();
    };
    this.generator = CustomRange.createGenerator(this.from,this.to);
    return arr;
  }
};

export default CustomRange;