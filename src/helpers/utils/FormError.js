import React from 'react';

class FormError extends Error{
  static type = "FORM_ERROR";
  constructor({data}){
    super("Form Error");
    this.type = "FORM_ERROR";
    this.data = data;
  }

  static fromResponse(error){
    if(error.type === this.type)return error
    if(error?.response?.status === 400 || error?.response?.status === 401){
      return new this({
        data: error.response.data,
      })
    }
    return null;
  }

  getField(field){
    return Array.isArray(this.data[field]) ? (
      this.data[field].map((err, i)=>(
        <p key={`response-error-${field}-${i}`} className="text-danger">{err}</p>
      ))
    ) : !!this.data[field] && (
      <p key={`response-error-${field}`} className="text-danger">{this.data[field]}</p>
    )
  }
  
};


export default FormError;