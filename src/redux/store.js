import { configureStore } from '@reduxjs/toolkit';
import { persistStore } from 'redux-persist';
import { rootReducer } from './reducer';
// import { fetchAddresses } from './slices/addresses.sclice';
import { getAccountInfo } from './slices/auth.slice';
import { getMyStore } from './slices/store/myStore.slice';
import { fetchProducts } from './slices/products.slice';
import { updateQueryState } from './slices/query.slice';
import { getCategories } from './slices/categories.slice';
// import { fetchBanners } from './slices/banners.slice';

export default () => {
  const store = configureStore({
    reducer: rootReducer,
    devTools: process.env.NODE_ENV !== 'production',
    // middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(applyMiddleware(thunk)),
    // enhancers: [compose],
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [
          "auth/authError",
          "auth/resetPasswordConfirm/rejected",
          "addresses/addressesError",
          "store/myStore/getMyStoreError",
          "store/myStore/myStoreError",
          "store/feedbacks/fetchFeedbacks",
          "store/myStore/getMyStore/rejected",
          "products/productsError",
          "products/fetchProducts/rejected",
          "products/addProductReview/rejected",
          "persist/PERSIST",
          "banners/fetchBanners/rejected",
          "categories/categoriesError",
          "products/fetchMainPageProducts/rejected",
        ],
        ignoredPaths: [
          "register",
          "auth.error",
          "addresses.error",
          "categories.error",
          "banners.error",
          "store.myStore.error",
          "products.error",
          "store.myShop.error",
          "store.myFeedbacks.error",
        ]
      },
    }),
  });
  store.dispatch(updateQueryState());
  store.dispatch(getAccountInfo());
  // store.dispatch(fetchAddresses());
  store.dispatch(getMyStore());
  store.dispatch(fetchProducts());
  store.dispatch(getCategories());
  // store.dispatch(fetchBanners())
  const persistor = persistStore(store);
  return { 
    store, 
    persistor 
  };
};