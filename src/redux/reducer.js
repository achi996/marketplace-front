import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

// import { reducer as toastrReducer } from 'react-redux-toastr';

import authReducer from './slices/auth.slice';
import navbarReducer from './slices/navbar.slice';
import customizerReducer from './slices/customizer.slice';
import categoriesReducer from './slices/categories.slice';
// import addressesReducer from './slices/addresses.sclice';
import storeReducer from './slices/store/reducer';
import productsReducer from './slices/products.slice';
import queryReducer from './slices/query.slice';
import cartReducer from './slices/cart.slice';
import wishlistReducer from './slices/wishlist.slice';
import accountsShopReducer from "./slices/accountsShop.slice";
import bannersReducer from './slices/banners.slice';

export const rootReducer = combineReducers({
  auth: authReducer,
  navbar: navbarReducer,
  customizer: customizerReducer,
  categories: persistReducer({
    key: "categories",
    storage,
    blacklist: ["error", "loading"],
  }, categoriesReducer ),
  // addresses: addressesReducer,
  banners: bannersReducer,
  store: storeReducer,
  products: productsReducer,
  accountsShop: accountsShopReducer,
  query: queryReducer,
  cart: persistReducer({
    key: "cart",
    storage,
    blacklist: ["error", "loading"],
  }, cartReducer ),
  wishlist: persistReducer({
    key: "wishlist",
    storage,
    blacklist: ["error", "loading"],
  }, wishlistReducer ),
});
