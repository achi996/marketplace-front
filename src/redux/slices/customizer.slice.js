import { createSlice } from '@reduxjs/toolkit';
import themeConfig from "../../configs/themeConfig";

const customizerSlice = createSlice({
  name: "customizer", 
  initialState: themeConfig,
  reducers: {
    changeMode(state, action){
      state.theme = action.payload;
    },
    collapseSidebar(state, action){
      state.sidebarCollapsed = action.payload;
    },
    changeNavbarColor(state, action){
      state.navbarColor = action.payload;
    },
    changeNavbarType(state, action){
      state.navbarType = action.payload;
    },
    changeFooterType(state, action){
      state.footerType = action.payload;
    },
    changeMenuColor(state, action){
      state.menuTheme = action.payload;
    },
    hideScrollToTop(state, action){
      state.hideScrollToTop = action.payload;
    },
  },
  extraReducers: {

  }
});


export const {
  actions:{
    changeMode,
    collapseSidebar,
    changeNavbarColor,
    changeNavbarType,
    changeFooterType,
    changeMenuColor,
    hideScrollToTop
  },
} = customizerSlice;

export default customizerSlice.reducer;