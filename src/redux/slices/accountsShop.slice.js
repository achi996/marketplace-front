import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import Axios from "axios";
import {API_BASE_URL} from "../../helpers/constants";
import {productsAdapter} from "./products.slice";

const initialState = {
  shop_id: null,
  name: "",
  logo: "",
  description: "",
  loading: false,
  error: null
};

export const fetchAccountsShop = createAsyncThunk(
  "shop-products/fetchAccountsShop",
  async (shop_id, {rejectWithValue}) => {
    try {
      const {data} = await Axios.get(`${API_BASE_URL}/accounts/shops/${shop_id}`);
      return data;
    } catch (error) {
      return rejectWithValue(error.response || error);
    }
  }
);


const setLoading = (state) => {
  state.loading = true;
  state.error = null;
};

const accountsShopSlice = createSlice({
  name: "shop-products",
  initialState: productsAdapter.getInitialState(initialState),
  reducers: {
    accountsShopError(state, {payload}) {
      state.error = payload;
      state.loading = false;
    }
  },
  extraReducers: {
    [fetchAccountsShop.pending]: setLoading,
    [fetchAccountsShop.fulfilled]: (state, {payload}) => {
      productsAdapter.setAll(state, payload.products);
      state.shop_id = payload.shop_id;
      state.name = payload.name;
      state.logo = payload.logo;
      state.description = payload.description;
      state.loading = false;
      state.error = null;
    },
  },
});

export const {
  accountsShopError,
} = accountsShopSlice.actions;

const accountsShopReducer = accountsShopSlice.reducer;
export default accountsShopReducer;
