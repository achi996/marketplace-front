import {createSlice} from "@reduxjs/toolkit";
import {productsAdapter, productsSelector} from "./products.slice";

const initialState = {};

const wishlistSlice = createSlice({
  name: "wishlist",
  initialState: productsAdapter.getInitialState(initialState),
  reducers: {
    clearWishlist(state) {
      productsAdapter.removeAll(state);
    },
    addItemToWishlist(state, {payload}) {
      productsAdapter.addOne(state, {...payload});
    },
    removeItemFromWishlist(state, {payload}) {
      productsAdapter.removeOne(state, payload.slug);
    },
    toggleItemInWishlist(state, {payload}) {
      if (productsSelector.selectById(state, payload.slug)) {
        productsAdapter.removeOne(state, payload.slug);
      } else {
        productsAdapter.addOne(state, {...payload});
      }
    },
  },
  extraReducers: {},
});

export const {
  clearWishlist,
  addItemToWishlist,
  removeItemFromWishlist,
  toggleItemInWishlist,
  updateItemInWishlist,
} = wishlistSlice.actions;

const wishlistReducer = wishlistSlice.reducer;

export default wishlistReducer;
