import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import Axios from 'axios';
import { searchResult } from "../../@fake-db/navbar/navbarBookmarkSearch"
import AUTH_MODALS from '../../layouts/components/navbar/authModals';

const defaultStarred = searchResult.filter(item => {
  return item.starred === true
});

const initialState = {
  suggestions: [],
  isLoading: false,
  value: "",
  starred: defaultStarred,
  noSuggestions: false,
  extraStarred: [],
  authModal: null, //? "login" | "register"
};

export const loadSuggestions = createAsyncThunk(
  "navbar/loadSuggestions",
  async (_, { rejectWithValue }) => {
    try {
      const { data } = await Axios.get("/api/search/bookmarks/data");
      return data.searchResult;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);

export const updateStarred = createAsyncThunk(
  "navbar/updateStarred",
  async (data, { dispatch, rejectWithValue }) => {
    try {
      await Axios.post("api/update/bookmarks", { obj: data })
      dispatch(loadSuggestions());
      return data;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);

const onUpdateStarredSuccess = (state, action) => {
  const starredState = state.starred.map(i => i.id)
  const extraStarredState = state.extraStarred.map(j => j.id)
  // Update main suggestions
  if (state) {
    state.suggestions.find(i => {
      if (i.id === action.payload.id) {
        i.starred = !action.payload.starred
        return { ...state }
      } else {
        return null
      }
    })
  }
  // checks if it includes and length is 0 < 10 then pushes object
  if (
    !starredState.includes(action.payload.id) &&
    state.starred.length < 10
  ) {
    let newState = state.starred.push(action.payload)
    return { ...state, newState }
  }

  // checks if it already includes and then removes it
  else if (starredState.includes(action.payload.id)) {
    if (state.extraStarred.length === 0) {
      let newState = state.starred.splice(
        starredState.indexOf(action.payload.id),
        1
      )
      return { ...state, newState }
    } else if (state.extraStarred.length > 0) {
      let getBookmark = state.extraStarred.splice(0, 1)
      state.starred.splice(starredState.indexOf(action.payload.id), 1)
      let updatedState = state.starred.push(getBookmark[0])
      return { ...state, updatedState }
    } else {
      return { ...state }
    }
  }

  // checks if extraState doesn't includes and main starred length is 10+
  else if (
    !extraStarredState.includes(action.payload.id) &&
    state.starred.length >= 10
  ) {
    let extraStarred = state.extraStarred.concat(action.payload)
    return { ...state, extraStarred }
  } else {
    return { ...state }
  }
};


const navbarSlice = createSlice({
  name: "navbar",
  initialState,
  reducers: {
    closeAuthModal(state){
      state.authModal = null;
    },
    toggleAuthModal(state, { payload }){
      if(!!AUTH_MODALS[payload]){
        state.authModal = state.authModal !== payload ? payload : null;
      }
    },
    
  },
  extraReducers: {
    [loadSuggestions.pending]: (state) => {
      state.isLoading = true;
    },
    [loadSuggestions.fulfilled]: (state, action) => {
      state.suggestions = action.payload;
      state.isLoading = false;
    },

    // [updateStarred.pending]: (state) => {
    //   state.isLoading = true;
    // },
    [updateStarred.fulfilled]: onUpdateStarredSuccess
  }
});

export const {
  actions: {
    toggleAuthModal,
    closeAuthModal,
  },
} = navbarSlice;

export default navbarSlice.reducer;