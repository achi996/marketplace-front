import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {history} from "../../history";

const initialState = {
  page: 1,
  limit: 21,
  search: "",
  "auth-modal": null,
  queryString: "",
  ordering: "price",
  retail: true,
  wholesale: true,
  category: [],
  discount_wholesale_price_min: null,
  discount_wholesale_price_max: null,
  discount_retail_price_min: null,
  discount_retail_price_max: null,
  region: "",
  paid: false,
  delivered: false,
  selectedProductSpecs: {},
};

export const setQueryParam = createAsyncThunk(
  "query/setQueryParam",
  ({key, value}, {dispatch}) => {
    const query = new URLSearchParams(history.location.search);
    query.set(key, value);
    if(key!=="page"){
      query.set("page", 1)
    }
    history.push(`?${query}`);
    dispatch(updateQueryState());
  }
)

export const appendQueryParamsString = createAsyncThunk(
  "query/appendQueryParamsString",
  ({ queryString }, { dispatch }) => {
    const currentQuery = new URLSearchParams(history.location.search);

    const prodSpecVals = [...new URLSearchParams(queryString).entries()].filter(([key, val])=>!!val);

    for( let [key, value] of prodSpecVals) {
      currentQuery.append(key, value);
    };
    history.push(`?${currentQuery}`);
    dispatch(updateQueryState());
  }
)

const querySlice = createSlice({
  name: "query",
  initialState,
  reducers: {
    resetQuery() {
      return initialState;
    },
    updateQueryState(state) {
      const urlSearchParams = new URLSearchParams(history.location.search);
      const s = Object.fromEntries(urlSearchParams.entries());
      if (s["auth-modal"]) state["auth-modal"] = s["auth-modal"];
      if (s["page"]) state.page = s["page"];
      if (s["limit"]) {
        state.limit = s["limit"]
        if (s["search"]) state.limit = ''
      }
      if (s['discount_wholesale_price_min'] || s['discount_wholesale_price_min'] === '') state.discount_wholesale_price_min = s['discount_wholesale_price_min'];
      if (s['discount_wholesale_price_max'] || s['discount_wholesale_price_max'] === '') state.discount_wholesale_price_max = s['discount_wholesale_price_max'];

      if (s['discount_retail_price_min'] || s['discount_retail_price_min'] === '') state.discount_retail_price_min = s['discount_retail_price_min'];
      if (s['discount_retail_price_max'] || s['discount_retail_price_max'] === '') state.discount_retail_price_max = s['discount_retail_price_max'];
      // console.log(s['category'])
      if (s["ordering"]) state.ordering = s["ordering"];
      state.search = s["search"] || "";
      state.retail = s["retail"] === "true";
      state.wholesale = s["wholesale"] === "true";
      state.category = s["category"] ? s["category"].split(",") : [];
      state.region = s["region"] || "";
      state.delivered = s["delivered"] || false;
      state.paid = s["paid"] || false;

      //? Selected Product Specifications
      for(let [key, val] of urlSearchParams.entries()){
        if(key.startsWith("product_specifications_values-")){
          state.selectedProductSpecs[key.slice(30)] = val;
          if(!state.selectedProductSpecs[key.slice(30)]){
            delete state.selectedProductSpecs[key.slice(30)]
          }
        }
      }

      const queryString = new URLSearchParams({
        "offset": (state.page - 1) * state.limit,
        "limit": state.limit,
        "search": state.search,
        "wholesale": state.wholesale || "unknown",
        "retail": state.retail || "unknown",
        ordering: state.ordering,
        'actual_wholesale_price_min': state.discount_wholesale_price_min || "",
        'actual_wholesale_price_max': state.discount_wholesale_price_max || "",
        'actual_retail_price_min': state.discount_retail_price_min || "",
        'actual_retail_price_max': state.discount_retail_price_max || "",
        'delivered': state.delivered || false,
        'paid': state.paid || false,
      });

      Object.values(state.selectedProductSpecs).forEach((val)=>{
        queryString.append("product_specifications_values", val)
      })

      state.category.forEach(cat=>{
        queryString.append("category", cat);
      })

      if(!!state.region){
        queryString.set("region", state.region);
      }

      state.queryString = queryString.toString();
    }
  },
  extraReducers: {},
});

export const {
  resetQuery,
  updateQueryState,
} = querySlice.actions;

const queryReducer = querySlice.reducer;
export default queryReducer;
