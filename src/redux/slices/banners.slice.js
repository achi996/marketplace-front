import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import Axios from "axios";
import { API_BASE_URL } from "../../helpers/constants";

const initialState = {
  bannersOne: [],
  bannersTwo: [],
  bannersThree: [],
  loading: false,
  error: null,
};

export const fetchBanners = createAsyncThunk(
  "banners/fetchBanners",
  async (_, { rejectWithValue })=>{
    try {
      const banners1Res = Axios.get(`${API_BASE_URL}/store_settings/banners_one/`);
      const banners2Res = Axios.get(`${API_BASE_URL}/store_settings/banners_two/`);
      const banners3Res = Axios.get(`${API_BASE_URL}/store_settings/banners_three/`);
      const [
        { data: {results: bannersOne} },
        { data: {results: bannersTwo} },
        { data: {results: bannersThree} },
      ] = await Promise.all([banners1Res, banners2Res, banners3Res]);
      return {
        bannersOne,
        bannersTwo,
        bannersThree,
      };
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

const setLoading = (state)=>{
  state.loading = true;
  state.error = null;
}

const bannersSlice = createSlice({
  name: "banners",
  initialState,
  reducers:{},
  extraReducers:{
    [fetchBanners.pending]: setLoading,
    [fetchBanners.fulfilled]: (state, { payload: { bannersOne, bannersTwo, bannersThree } })=>{
      state.bannersOne = bannersOne;
      state.bannersTwo = bannersTwo;
      state.bannersThree = bannersThree;
      state.loading = false;
      state.error = null;
    },
    [fetchBanners.rejected]: (state, { payload })=>{
      state.loading = false;
      state.error = payload;
    }
  },
});

export const {
  reducer: bannersReducer,
  // actions: {},
} = bannersSlice;

export default bannersReducer;