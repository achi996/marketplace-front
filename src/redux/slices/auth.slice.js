import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import Axios from 'axios';
import {API_BASE_URL} from '../../helpers/constants';
import authAxios from '../../helpers/utils/authAxios';
import FormError from '../../helpers/utils/FormError';
import {history} from '../../history';
// import { closeAuthModal } from './navbar.slice';
import {toast} from 'react-toastify';

const initialState = {
  userRole: null,
  loading: false,
  error: null,
  userData: null,
  accessToken: null,
  isEmailActivated: false,
  registerSuccess: null,
  resetPasswordSuccess: null,
  store: null,
};

export const loginWithEmail = createAsyncThunk(
  'auth/login/with-email',
  async ({email, password}, {dispatch, rejectWithValue}) => {
    try {
      const {data: {refresh, access}} = await Axios.post(`${API_BASE_URL}/auth/jwt/create/`, {email, password});
      window.localStorage.setItem("token.refresh", refresh);
      window.localStorage.setItem("token.access", access);
      // history.push("/");
      // dispatch(closeAuthModal());
      history.push(history.location.pathname);
      dispatch(getAccountInfo());
      return {
        refresh,
        access,
        email,
      }
    } catch (error) {
      if(error?.response?.status === 401){
        error.response.data.detail = "Не правильный email или пароль!";
      }
      dispatch(authError(error));
      throw error;
    }
  }
);

export const refreshToken = createAsyncThunk(
  'auth/login/refresh-token',
  async (_, {dispatch}) => {
    try {
      const {data: {access}} = await Axios.post(`${API_BASE_URL}/auth/jwt/refresh/`);
      window.localStorage.setItem("token.access", access);
      return {access};
    } catch (error) {
      // dispatch(authError(error));
      dispatch(logout());
    }
  }
);

export const registerWithEmail = createAsyncThunk(
  'auth/register/with-email',
  async ({email, phone_number, password, user_type}, {dispatch}) => {
    try {
      await Axios.post(`${API_BASE_URL}/auth/users/`, {
        email,
        phone_number,
        password,
        user_type
      });
    } catch (error) {
      dispatch(authError(error));
      throw error;
    }
  }
);

export const getAccountInfo = createAsyncThunk(
  'auth/getAccountInfo',
  async () => {
    try {
      const {data} = await authAxios.get(`/accounts/users/me/`);
      return data;
    } catch (error) {
      // dispatch(authError(error));
      throw error;
    }
  }
);

export const emailActivate = createAsyncThunk(
  'auth/emailActivate',
  async ({uid, token}, {dispatch}) => {
    try {
      const {data} = await authAxios.post(`/auth/users/activation/`, {
        uid,
        token
      });
      history.push("/")
      return data;
    } catch (error) {
      dispatch(authError(error));
      throw error;
    }
  }
);

export const resetPassword = createAsyncThunk(
  'auth/resetPassword',
  async ({email}, { dispatch, rejectWithValue }) => {
    try {
      const {data} = await Axios.post(`${API_BASE_URL}/auth/users/reset_password/`, {
        email
      });
      return data;
    } catch (error) {
      dispatch(authError(error))
    }
  }
);

export const resetPasswordConfirm = createAsyncThunk(
  'auth/resetPasswordConfirm',
  async ({
    uid,
    token,
    new_password,
  }, { dispatch, rejectWithValue }) => {
    try {
      const {data} = await Axios.post(`${API_BASE_URL}/auth/users/reset_password_confirm/`, {
        uid,
        token,
        new_password,
      });
      history.push("/?auth-modal=login");
      toast.success(`Ваш пароль изменён!`);
      return data;
    } catch (error) {
      dispatch(authError(error))
      return rejectWithValue(error)
    }
  }
);

export const changeEmail = createAsyncThunk(
  'auth/changeEmail',
  async ({current_password, new_email}, {dispatch}) => {
    try {
      const {data} = await authAxios.post(`/auth/users/set_email/`, {
        current_password,
        new_email,
      });
      return data;
    } catch (error) {
      dispatch(authError(error));
      throw error;
    }
  }
);

export const changePassword = createAsyncThunk(
  'auth/changePassword',
  async ({current_password, new_password}, {dispatch}) => {
    try {
      const {data} = await authAxios.post(`/auth/users/set_password/`, {
        current_password,
        new_password,
      });
      return data;
    } catch (error) {
      dispatch(authError(error));
      throw error;
    }
  }
);

export const changeProfile = createAsyncThunk(
  "auth/changeProfile",
  async ({first_name, last_name, phone_number}, {dispatch}) => {
    try {
      const {data} = await authAxios.patch(`/accounts/users/me/`, {
        first_name,
        last_name,
        phone_number,
      });
      return data
    } catch (error) {
      dispatch(authError(error));
      throw error;
    }
  }
)

const setLoading = (state) => {
  state.loading = true;
  state.error = null;
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    changeRole(state, action) {
      state.userRole = action.payload;
    },
    authError(state, action) {
      console.log(action);
      const e = FormError.fromResponse(action.payload);
      state.error = e || action.payload;
      state.loading = false;
    },
    clearAuthError(state) {
      state.error = null;
    },
    clearResetPasswordSuccess(state){
      state.resetPasswordSuccess = null;
    },
    logout(state) {
      state.accessToken = null;
      state.error = null;
      state.loading = false;
      state.userData = null;
      state.userRole = null;
      window.localStorage.removeItem("token.refresh");
      window.localStorage.removeItem("token.access");
      toast.warn(`Вы вышли из аккаунта!`);
    },
  },
  extraReducers: {

    [loginWithEmail.pending]: setLoading,
    [loginWithEmail.fulfilled]: (state, action) => {
      state.accessToken = action.payload.access;
      state.loading = false;
      state.error = null;
      toast.success(`Вы зашли через аккаунт ${action.payload.email}`);
    },

    [refreshToken.fulfilled]: (state, action) => {
      state.accessToken = action.access;
    },

    [registerWithEmail.pending]: (state) => {
      setLoading(state);
      state.registerSuccess = null;
    },
    [registerWithEmail.rejected]: (state) => {
      state.registerSuccess = false;
    },
    [registerWithEmail.fulfilled]: (state) => {
      state.loading = false;
      state.error = null;
      state.registerSuccess = true;
    },

    [getAccountInfo.pending]: setLoading,
    [getAccountInfo.rejected]: (state) => {
      state.loading = false;
      state.userRole = null;
      state.userData = null;
    },
    [getAccountInfo.fulfilled]: (state, action) => {
      state.userData = action.payload;
      state.loading = false;
      state.error = null;
      state.userRole = action.payload.user_type;
    },

    [emailActivate.pending]: setLoading,
    [emailActivate.fulfilled]: (state) => {
      state.isEmailActivated = true;
      state.loading = false;
      state.error = null;
      toast.success(`Ваша почта подтверждена! Теперь вы можете зайти в свой аккаунт`);
    },

    [resetPassword.pending]: (state)=>{
      setLoading(state);
      state.resetPasswordSuccess = null;
    },
    [resetPassword.rejected]: (state)=>{
      state.resetPasswordSuccess = false;
    },
    [resetPassword.fulfilled]: (state) => {
      state.loading = false;
      state.error = null;
      state.resetPasswordSuccess = true;
    },

    [resetPasswordConfirm.pending]: setLoading,
    [resetPasswordConfirm.fulfilled]: (state) => {
      state.loading = false;
      state.error = null;
    },

    [changeEmail.pending]: setLoading,
    [changeEmail.fulfilled]: (state) => {
      state.loading = false;
      state.error = null;
    },

    [changePassword.pending]: setLoading,
    [changePassword.fulfilled]: (state) => {
      state.loading = false;
      state.error = null;
      toast.success(`Ваш пароль изменён!`);
    },

    [changeProfile.pending]: setLoading,
    [changeProfile.fulfilled]: (state, {payload}) => {
      state.userData = payload;
      state.loading = false;
      state.error = null;
      toast.success("Ваши данные изменены");
    },
  },
});

export const {
  actions: {
    authError,
    clearAuthError,
    clearResetPasswordSuccess,
    changeRole,
    logout,
  },
} = authSlice;

export default authSlice.reducer;
