import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import Axios from 'axios';
import { API_BASE_URL } from '../../helpers/constants';
import FormError from '../../helpers/utils/FormError';

export const transformCategories = (categories, parent) => {
  const arr = [];
  categories.forEach(cat=>{
    arr.push(cat);
    cat.navLink = `?category=${cat.id}`;
    cat.key = `category-${cat.id}`;
    cat.nodes = cat.children;
    cat.label = cat.title;
    cat.parentCategory = parent;
    if(cat.children){
      arr.push(...transformCategories(cat.children, {
        id: cat.id,
        title: cat.title,
        slug: cat.slug,
        parentCategory: cat.parentCategory,
      }));
    }
  })
  return arr;
};

const initialState = {
  loading: false,
  error: null,
  categories: [],
  flattenCategories: [],
};

const setLoading = (state)=>{
  state.loading = true;
  state.error = null;
}

export const getCategories = createAsyncThunk(
  'categories/getCategories',
  async (_,{dispatch})=>{
    try {
      const {data} = await Axios.get(`${API_BASE_URL}/store/categories/`);
      return data;
    } catch (error) {
      dispatch(categoriesError(error));
      throw error;
    }
  }
);

const categoriesSlice = createSlice({
  name: "categories",
  initialState,
  reducers: {
    categoriesError(state, action){
      const e = FormError.fromResponse(action.payload);
      state.error = e || action.payload;
      state.loading = false;
    },
  },
  extraReducers: {
    [getCategories.pending]: setLoading,
    [getCategories.fulfilled]: (state, action)=>{
      const flattenCategories = transformCategories(action.payload);
      state.categories = action.payload;
      state.flattenCategories = flattenCategories;
      state.loading = false;
      state.error = null;
    },
  },
});

export const {
  actions:{
    categoriesError,
  },
} = categoriesSlice;

export default categoriesSlice.reducer;
