import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import Axios from "axios";
// import { MD5 } from "crypto-js";
import {API_BASE_URL} from "../../helpers/constants";
import authAxios from "../../helpers/utils/authAxios";
import {getTotalSum} from "../../helpers/utils/productPrices";
import {history} from "../../history";
import {productsAdapter, productsSelector} from "./products.slice";

const initialState = {
  self_deliver_addresses_list: [],
  self_delivered: false,
  self_deliver_address: null,
  country: "",
  oblast: "",
  city: "",
  street: "",
  house_number: "",
  loading: false,
  error: null,
  payment_type: "CASH",
  phone_number: "",
  user_info: "",
  comment: "",
  currentOrderData: null,
  dropdownIsOpen: true,
};

//? payment_type = PAYBOX || CASH

export const fetchAddresses = createAsyncThunk(
  "cart/fetchAddresses",
  async (_,{dispatch})=>{
    try {
      const { data } = await Axios.get(`${API_BASE_URL}/accounts/self_deliver_addresses/`);
      return data.results;
    } catch (error) {
      dispatch(cartError(error));
    }
  }
);

export const createPayboxOrder = createAsyncThunk(
  "cart/createPayboxOrder",
  async ({order_id}, { getState })=>{
    const state = getState();
    const cart = productsSelector.selectAll(state.cart);

    const { discountedTotalPrice = 0 } = getTotalSum(cart);

    const formValues = {
      pg_amount: discountedTotalPrice,
      pg_currency: "KGS",
      pg_description: "Товары",
      pg_merchant_id: "538283",
      pg_order_id: order_id,
      pg_testing_mode: 1,
    };

    // const sortedEntries = Object.entries(formValues).sort((a,b) => a[0].toString().localeCompare(b[0].toString()));

    // console.log(sortedEntries);

    // const pg_sig = [
    //   "init_payment.php",
    //   ...sortedEntries.map(entries=>entries[1]),
    //   "wvPjxMzqsldkCtsS"
    // ].join(";");

    // formValues.pg_sig = MD5(pg_sig).toString();

    const {data} = await Axios.post(`${API_BASE_URL}/accounts/paybox/init/`, formValues);
    console.log(data);

    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(data.content,"text/xml");

    // console.log(xmlDoc, url, formValues, data)

    // function pay(amount) {
    //   const data = {
    //     token: "b0f0F1sntz3NbEy3ZFcwkdkgY8jZpVxA",
    //     payment: {
    //       order: order_id || "12345",
    //       amount: `${amount}`,
    //       currency: "KGS",
    //       description: state.cart.comment || "Описание заказа",
    //       param1: "string",
    //       param2: "string",
    //       param3: "string",
    //       test: 1,  // testing mode
    //       options: {
    //         callbacks: {
    //           result_url: "https://doidoi-shop.web.app",
    //           check_url: "https://doidoi-shop.web.app"
    //         },
    //         custom_params: {},
    //         user: {
    //           email: state.auth?.userData?.email || "emir0725mk@gmail.com",
    //           phone: state.cart.phone_number,
    //         }
    //       }
    //     },
    //     successCallback: function (payment) {
    //         //...
    //         console.log(payment)
    //     },
    //     errorCallback: function (payment) {
    //         //...
    //         console.log("error.payobx", payment)
    //     }
    //   }
  
    //   var paybox = new window.PayBox(data);
    //   paybox.create();
    // }
    // pay(discountedTotalPrice)
    window.location.href = xmlDoc.getElementsByTagName("pg_redirect_url")[0].childNodes[0].nodeValue;
    return data.content;
  }
);

export const createOrder = createAsyncThunk(
  "cart/createOrder",
  async (_, {rejectWithValue, getState, dispatch})=>{
    try {
      const state = getState().cart;
      const products = productsSelector.selectAll(state);
      const { data: orderData } = await authAxios.post(`/accounts/orders/`, {
        country: state.country,
        city: state.city,
        street: state.street,
        house_number: state.house_number,
        payment_type: state.payment_type,
        oblast: state.oblast,
        phone_number: state.phone_number,
        comment: state.comment,
        user_info: state.user_info,
        self_delivered: state.self_delivered,
        self_deliver_address: state.self_deliver_address,
      });

      const orderProducts = [];
      for (const product of products){
        const { data } = await authAxios.post(`/accounts/order_items/`, {
          quantity: product.inCartCount,
          order: orderData.id,
          product: product.id,
          product_specifications: product.selected_specifications,
        });
        orderProducts.push(data);
      }

      // const orderProducts = await Promise.all(productsRes);
      if(state.payment_type === "PAYBOX") {
        dispatch(createPayboxOrder({ order_id: orderData.order_id }));
      }else{
        history.push(`/checkout/invoice/user/${orderData.id}/`);
      }
      return {
        orderData,
        orderProducts,
      }
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);


export const updateOpder = createAsyncThunk(
  "cart/updateOpder",
  async (_, {rejectWithValue, getState})=>{
    try {
      const state = getState().cart;
      const { data: orderData } = await Axios.patch(`${API_BASE_URL}/accounts/orders/`, {
        country: state.country,
        oblast: state.oblast,
        city: state.city,
        street: state.street,
        house_number: state.house_number,
        payment_type: state.payment_type,
        phone_number: state.phone_number,
        comment: state.comment,
        user_info: state.user_info,
        self_delivered: state.self_delivered,
        self_deliver_address: state.self_deliver_address,
      });
      return {
        orderData,
      }
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

const setLoading = (state)=>{
  state.loading = true;
  state.error = null;
}

const cartSlice = createSlice({
  name: "cart",
  initialState: productsAdapter.getInitialState(initialState),
  reducers: {
    cartError(state, action){
      state.error = action.payload;
      state.loading = false;
    },
    clearCart(state) {
      productsAdapter.removeAll(state);
    },
    addItemToCart(state, {payload}) {
      const item = {...payload};
      productsAdapter.addOne(state, item);
    },
    removeItemFromCart(state, {payload}) {
      productsAdapter.removeOne(state, payload.slug);
    },
    toggleItemInCart(state, {payload}) {
      if (productsSelector.selectById(state, payload.slug)) {
        productsAdapter.removeOne(state, payload.slug);
      } else {
        const item = {...payload};
        productsAdapter.addOne(state, item);
      }
    },
    changeItemCountInCart(state, {payload: {slug, count}}) {
      productsAdapter.updateOne(state, {
        id: slug,
        changes: {
          inCartCount: count
        }
      });
    },
    updateItemInCart(state, {payload}) {
      productsAdapter.updateOne(state, {
        id: payload.slug,
        changes: payload
      });
    },

    setCountPrices(state, {payload}) {
      // console.log(payload)
      state.countPrices = payload
    },

    setSelfDelivered(state, {payload: { selfDeliverId }}){
      state.self_delivered = true;
      state.self_deliver_address = selfDeliverId;
    },

    setDeliveryAddress(state, {
      payload: {
        country,
        city,
        oblast,
        street,
        house_number
      }
    }){
      state.country = country;
      state.city = city;
      state.street = street;
      state.oblast = oblast;
      state.house_number = house_number;
      state.self_delivered = false;
      state.self_deliver_address = null;
    },
    setDelivered(state){
      state.self_delivered = false;
      state.self_deliver_address = null;
    },
    setPhoneNumber(state, {payload}){
      state.phone_number = payload;
    },
    setOrderComment(state, {payload}){
      state.comment = payload;
    },
    setUserInfo(state, {payload}){
      state.user_info = payload;
    },
    setPaymentType(state, {payload}){
      state.payment_type = payload;
    },
    toggleDropdown(state){
      state.dropdownIsOpen = !state.dropdownIsOpen;
    },
    setDropdownIsOpen(state, { payload }){
      state.dropdownIsOpen = !!payload
    }
  },
  extraReducers: {
    [fetchAddresses.pending]: setLoading,
    [fetchAddresses.fulfilled]: (state, { payload })=>{
      state.self_deliver_addresses_list = payload;
      state.loading = false;
      state.error = null;
    },

    [createOrder.pending]: setLoading,
    [createOrder.fulfilled]: (_, { payload: {orderData, orderProducts} })=>{
      return {
        ...productsAdapter.getInitialState(initialState),
        currentOrderData: {...orderData, orderProducts}
      }
    }
  },
});

export const {
  cartError,
  clearCart,
  addItemToCart,
  removeItemFromCart,
  toggleItemInCart,
  updateItemInCart,
  changeItemCountInCart,
  setSelfDelivered,
  setDeliveryAddress,
  setDelivered,
  setPhoneNumber,
  setUserInfo,
  setPaymentType,
  setOrderComment,
  toggleDropdown,
} = cartSlice.actions;

const cartReducer = cartSlice.reducer;

export default cartReducer;
