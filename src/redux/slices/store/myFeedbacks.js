import { createAsyncThunk, createEntityAdapter, createSlice } from "@reduxjs/toolkit";
// import { toast } from "react-toastify";
import authAxios from "../../../helpers/utils/authAxios";
import FormError from "../../../helpers/utils/FormError";

const initialState = {
  loading: false,
  error: null,
};

const feedbacksAdapter = createEntityAdapter({
  // sortComparer: (a, b)=>{
  //   const aStr = a.country+a.city+a.street+a.house_number;
  //   const bStr = b.country+b.city+b.street+b.house_number;
  //   return aStr.localeCompare(bStr);
  // },
});

export const feedbacksSelector = feedbacksAdapter.getSelectors();

export const fetchFeedbacks = createAsyncThunk(
  "store/feedbacks/fetchFeedbacks",
  async (search, {dispatch})=>{
    console.log(search)
    try {
      const { data } = await authAxios.get(`/accounts/my_shop_reviews/?search=${search}`);
      return data.results.map((item, index)=>({...item, id: index}));
    } catch (error) {
      dispatch(feedbacksError(error));
      throw error;
    }
  }
);

// export const addAddress = createAsyncThunk(
//   "addresses/addAddress",
//   async ({
//     country,
//     city,
//     street,
//     house_number,
//   },{ getState, dispatch })=>{
//     try {
//       const profile = getState().auth.userData.profile.id
//       const { data } = await authAxios.post(`/accounts/my_addresses/`, {
//         country,
//         city,
//         street,
//         house_number,
//         profile,
//       });
//       return data;
//     } catch (error) {
//       dispatch(feedbacksError(error));
//       throw error
//     }
//   }
// );

// export const updateAddress = createAsyncThunk(
//   "addresses/updateAddress",
//   async ({
//     id,
//     country,
//     city,
//     street,
//     house_number,
//   },{ dispatch })=>{
//     try {
//       const { data } = await authAxios.patch(`/accounts/my_addresses/${id}/`, {
//         country,
//         city,
//         street,
//         house_number,
//       });
//       return data;
//     } catch (error) {
//       dispatch(feedbacksError(error));
//       throw error
//     }
//   }
// );

// export const getAddress = createAsyncThunk(
//   "addresses/getAddress",
//   async ({id}, {dispatch})=>{
//     try {
//       const { data } = await authAxios.get(`/accounts/my_addresses/${id}`);
//       return data;
//     } catch (error) {
//       dispatch(feedbacksError(error));
//       throw error;
//     }
//   }
// );

// export const deleteAddress = createAsyncThunk(
//   "addresses/deleteAddress",
//   async ({id}, {dispatch})=>{
//     try {
//       await authAxios.delete(`/accounts/my_addresses/${id}`);
//       return id;
//     } catch (error) {
//       dispatch(feedbacksError(error));
//       throw error;
//     }
//   }
// );

const setLoading = (state)=>{
  state.loading = true;
  state.error = null;
};

const feedbacksSclice = createSlice({
  name: "feedbacks",
  initialState: feedbacksAdapter.getInitialState(initialState),
  reducers: {
    feedbacksError(state, action){
      const e = FormError.fromResponse(action.payload);
      state.error = e || action.payload;
      state.loading = false;
    },
    // toggleEditAddress(state, action){
    //   const id = action.payload;
    //   const isEdit = feedbacksSelector.selectById(state, id)?.isEdit || false;
    //   feedbacksAdapter.updateOne(state, {
    //     id,
    //     changes: {
    //       isEdit: !isEdit,
    //     }
    //   });
    // }
  },
  extraReducers: {
    [fetchFeedbacks.pending]: setLoading,
    [fetchFeedbacks.fulfilled]: (state, action)=>{
      feedbacksAdapter.setAll(state, action.payload);
      state.loading = false;
      state.error = null;
    },

    // [addAddress.pending]: setLoading,
    // [addAddress.fulfilled]: (state, action)=>{
    //   feedbacksAdapter.addOne(state, action.payload);
    //   state.loading = false;
    //   state.error = null;
    //   toast.success("Адрес добавлен!");
    // },

    // [updateAddress.pending]: setLoading,
    // [updateAddress.fulfilled]: (state, action)=>{
    //   feedbacksAdapter.updateOne(state, {
    //     id: action.payload.id,
    //     changes: action.payload,
    //   });
    //   state.loading = false;
    //   state.error = null;
    //   toast.success("Адрес изменён!");
    // },

    // [deleteAddress.pending]: setLoading,
    // [deleteAddress.fulfilled]: (state, action)=>{
    //   feedbacksAdapter.removeOne(state, action.payload);
    //   state.loading = false;
    //   state.error = null;
    //   toast.warn("Адрес Удалён!");
    // },
  },
});

export const {
  actions:{
    feedbacksError,
    // toggleEditAddress,
  }
} = feedbacksSclice;

export default feedbacksSclice.reducer
