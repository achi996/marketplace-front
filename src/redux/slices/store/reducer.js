import { combineReducers } from "redux";
import myStoreReducer from "./myStore.slice";
import myShopReducer from "./myShop.slice"
import myFeedbacksReducer from "./myFeedbacks";

const storeReducer = combineReducers({
  myStore: myStoreReducer,
  myShop: myShopReducer,
  myFeedbacks: myFeedbacksReducer,
});

export default storeReducer;