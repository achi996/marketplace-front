import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import authAxios from '../../../helpers/utils/authAxios';
import FormError from '../../../helpers/utils/FormError';
import {toast} from 'react-toastify';
import {history} from '../../../history';

const initialState = {
  isActive: false,
  data: null,
  loading: true,
  error: null,
  isStoreNotFound: false,
};

export const createMyStore = createAsyncThunk(
  "store/myStore/createMyStore",
  async (formData, {dispatch}) => {
    try {
      toast.info("Идет запрос на создание магазина")
      const {data} = await authAxios.post(`/accounts/my_shop/`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        }
      });
      dispatch(getMyStore());
      history.push("/account/store/info");
      return data
    } catch (error) {
      dispatch(myStoreError(error));
      throw error;
    }
  }
);

export const changeMyStore = createAsyncThunk(
  "store/myStore/changeMyStore",
  async ({formData, id, /*isUpdate*/}, {dispatch}) => {
    try {
      const {data} = await authAxios.patch(`/accounts/my_shop/${id}/`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        }
      })
      return data
    } catch (error) {
      dispatch(myStoreError(error));
      throw error;
    }
  }
);

export const getMyStore = createAsyncThunk(
  "store/myStore/getMyStore",
  async (_, {getState, rejectWithValue}) => {
    try {
      const oldData = getState().store.myStore.data;
      if (oldData) {
        return oldData;
      }
      const {data} = await authAxios.get(`/accounts/my_shop/`);
      return data;
    } catch (error) {
      // dispatch(getMyStoreError(error));
      // throw error
      return rejectWithValue(error);
    }
  }
);

const setLoading = (state) => {
  state.loading = true;
  state.error = null;
};

const myStoreSlice = createSlice({
  name: "store/myStore",
  initialState,
  reducers: {
    myStoreError(state, action) {
      console.log(action);
      const e = FormError.fromResponse(action.payload);
      state.error = e || action.payload;
      state.loading = false;
    },
    getMyStoreError(state, {payload}) {
      state.error = payload?.response?.status === 404 ?
        {type: "NOT_FOUND", message: "Not Found"} :
        payload;
      state.loading = false;
    }
  },
  extraReducers: {
    [getMyStore.pending]: setLoading,
    [getMyStore.fulfilled]: (state, {payload}) => {
      state.data = payload;
      state.isActive = payload.is_active
      state.loading = false;
      state.error = null;
    },
    [getMyStore.rejected]: (state, {payload})=>{
      if(payload?.response?.status === 404){
        state.isStoreNotFound = true
        state.error = {type: "NOT_FOUND", message: "Not Found"}
      }else{
        state.error = payload
      }
      state.loading = false;
    },

    [createMyStore.pending]: setLoading,
    [createMyStore.fulfilled]: () => {
      toast.success("Запрос на создание магазина отправлен!");
    },

    [changeMyStore.pending]: setLoading,
    [changeMyStore.fulfilled]: (state, {payload}) => {
      state.store = payload;
      state.loading = false;
      state.error = null;
      toast.success("Ваши данные изменены");
    },
  },
});

export const {
  actions: {
    myStoreError,
    getMyStoreError,
  }
} = myStoreSlice;

const myStoreReducer = myStoreSlice.reducer;
export default myStoreReducer;
