import {createAsyncThunk, createEntityAdapter, createSlice} from "@reduxjs/toolkit";
import {toast} from "react-toastify";
import authAxios from '../../../helpers/utils/authAxios';
import FormError from '../../../helpers/utils/FormError';

const initialState = {
  product: null,
  products: null,
  loading: false,
  error: null,
  previous: null,
  next: null,
  count: 0,
  paymentTypes: [],
  days: [],
};

const myShopAdapter = createEntityAdapter({
  sortComparer: (a, b) => a.title.localeCompare(b.title),
});

export const myShopSelector = myShopAdapter.getSelectors();

export const REGION_CHOICES = {
  IKL: 'Иссык-кульская область',
  CHU: 'Чуйская область',
  BSH: 'г. Бишкек',
  OSH: 'Ошская область',
  TLS: 'Талаская область',
  NRN: 'Нарынская область',
  BTK: 'Баткенская область',
  JLB: 'Джал-Абадская область',
};

// export const fetchMyProduct = createAsyncThunk(
//   "store/myShop/fetchMyProduct",
//   async ({id}, {dispatch})=>{
//     try {
//       const {data} = await authAxios.get("/accounts/s")
//     } catch (error) {

//     }
//   }
// )

export const fetchProductUpgradeOptions = createAsyncThunk(
  "store/myShop/fetchProductUpgradeOptions",
  async (_,{dispatch})=>{
    try {
      const { data: { results: paymentTypes } } = await authAxios.get(`/store/product_upgrade_payment_types/`);
      const { data: { results: days } } = await authAxios.get(`/store/product_upgrade_days/`);
      return { paymentTypes, days };
    } catch (error) {
      // toast.error("Что то пошло не так (((");
      dispatch(myShopError(error));
    }
  }
)

export const fetchMyProducts = createAsyncThunk(
  "store/myShop/fetchMyProducts",
  async (_, {dispatch, getState}) => {
    try {
      const {data} = await authAxios.get(`/accounts/shops/${getState().store.myStore.data?.shop_id}/`);
      return {
        next: null,
        previous: null,
        count: data.products.length,//? total count of elements in server
        results: data.products,
      };
    } catch (error) {
      dispatch(myShopError(error));
      toast.error("Что то пошло не так (((");
      throw error;
    }
  }
);

export const updateMyProductImage = createAsyncThunk(
  "store/myShop/updateMyProductImage",
  async ({id, is_feature}, {dispatch}) => {
    try {
      const {data} = await authAxios.patch(`/store/product_options_images/${id}/`, {
        is_feature
      }, {headers: {"Content_Type": "multipart/form-data"}});
      return data;
    } catch (error) {
      dispatch(myShopError(error));
      toast.error("Что то пошло не так (((");
      throw error;
    }
  }
);

export const addMyProduct = createAsyncThunk(
  "store/myShop/addMyProduct",
  async ({
    title,
    description,
    address,
    retail,
    wholesale,
    category,
    amount,
    product_specifications,
    product_specifications_values,
    old_retail_price,
    wholesale_price,
    new_retail_price,
    wholesale_amount,
    callback,
    region,
  }, {dispatch}) => {
    try {
      if(!REGION_CHOICES[region])throw new Error(`Неверный регион ${region}! Регион должен быть одним из ${Object.keys(REGION_CHOICES)}`);
      const {data} = await authAxios.post(`/store/products/`, {
        title,
        description,
        address,
        retail,
        wholesale,
        category,
        amount,
        product_specifications,
        product_specifications_values,
        old_retail_price,
        wholesale_price,
        new_retail_price,
        wholesale_amount,
        is_active: false,
        region,
      });
      callback(data);
      return data;
    } catch (error) {
      dispatch(myShopError(error));
      toast.error("Что то пошло не так (((");
      throw error
    }
  }
);

export const updateMyProduct = createAsyncThunk(
  "store/myShop/updateMyProduct",
  async ({
    is_active,
    title,
    description,
    address,
    retail,
    wholesale,
    category,
    amount,
    product_specifications,
    product_specifications_values,
    old_retail_price,
    wholesale_price,
    new_retail_price,
    wholesale_amount,
    slug,
    region,
  }, {dispatch}) => {
    try {
      if(!REGION_CHOICES[region])throw new Error("Неверный регион");
      const {data} = await authAxios.patch(`/store/products/${slug}/`, {
        is_active,
        title,
        description,
        address,
        retail,
        wholesale,
        category,
        amount,
        product_specifications,
        product_specifications_values,
        old_retail_price,
        wholesale_price,
        new_retail_price,
        wholesale_amount,
        region,
      });
      return data;
    } catch (error) {
      dispatch(myShopError(error));
      toast.error("Что то пошло не так (((");
      throw error
    }
  }
);

export const deleteMyProduct = createAsyncThunk(
  "store/myShop/deleteMyProduct",
  async (product, {dispatch}) => {
    try {
      await authAxios.delete(`/store/products/${product.slug}`);
      return product;
    } catch (error) {
      dispatch(myShopError(error));
      toast.error("Что то пошло не так (((");
      throw error;
    }
  }
);


export const addImageToProduct = createAsyncThunk(
  "store/myShop/addImageToProduct",
  async ({
    formData,
  }, {dispatch}) => {
    try {
      toast.info("Изображение загружается...");
      const res = await authAxios.post(`/store/product_images/`, formData, {
        headers: {
          'Content-Type': "multipart/form-data"
        }
      });
      toast.success("Изображение добавлено!");
      dispatch(fetchMyProducts());
      return res;
    } catch (error) {
      toast.error("Что то пошло не так (((");
    }
  }
);

export const removeProductFormImage = createAsyncThunk(
  "store/myShop/removeProductFormImage",
  async (id, {dispatch}) => {
    try {
      toast.info("Изображение удаляется...");
      await authAxios.delete(`/store/product_images/${id}`);
      toast.success("Изображение удалено!");
      dispatch(fetchMyProducts());
    } catch (error) {
      toast.error("Что то пошло не так (((");
    }
  }
);

export const addProductOption = createAsyncThunk(
  "store/myShop/addProductOption",
  async ({
    description,
    amount,
    old_retail_price,
    wholesale_price,
    new_retail_price,
    productId,
  }, { dispatch })=>{
    try {
      toast.info("загружается...");
      const formData = new FormData();
      formData.append("images", []);
      formData.append("product", productId)
      formData.append("description", description);
      formData.append("amount", amount);
      formData.append("old_retail_price", old_retail_price);
      formData.append("wholesale_price", wholesale_price);
      formData.append("new_retail_price", new_retail_price);
      const res = await authAxios.post(`/store/product_options/`, formData, {
        headers: {
          'Content-Type': "multipart/form-data"
        }
      });
      toast.success("добавлено!");
      dispatch(fetchMyProducts());
      return res;
    } catch (error) {
      toast.error("Что то пошло не так (((");
    }
  }
);

export const updateProductOption = createAsyncThunk(
  "store/myShop/updateProductOption",
  async ({
    description,
    amount,
    old_retail_price,
    wholesale_price,
    new_retail_price,
    id,
  },{dispatch})=>{
    try {
      toast.info("загружается...");
      const formData = new FormData();
      formData.append("description", description);
      formData.append("amount", amount);
      formData.append("old_retail_price", old_retail_price);
      formData.append("wholesale_price", wholesale_price);
      formData.append("new_retail_price", new_retail_price);
      const res = await authAxios.patch(`/store/product_options/${id}/`, formData, {
        headers: {
          'Content-Type': "multipart/form-data"
        }
      });
      toast.success("Изменено!");
      dispatch(fetchMyProducts());
      return res;
    } catch (error) {
      toast.error("Что то пошло не так (((");
    }
  }
);

export const upgradeMyProduct = createAsyncThunk(
  "upgradeMyProduct",
  async (formData, {dispatch}) => {
    try {
      toast.info("загружается...");
      const { data } = await authAxios.post(`/store/product_upgrade/`, formData, {
        headers: {
          'Content-Type': "multipart/form-data"
        }
      });
      toast.success("Отправлено!");
      return data;
    } catch (error) {
      toast.error("Что то пошло не так (((");
      dispatch(myShopError(error))
    }
  }
)

const setLoading = (state) => {
  state.loading = true;
  state.error = null;
};

const myShopSlice = createSlice({
  name: "store/myShop",
  initialState: myShopAdapter.getInitialState(initialState),
  reducers: {
    resetMyShopSlice() {
      return myShopAdapter.getInitialState(initialState)
    },
    myShopError(state, action) {
      console.log(action);
      const e = FormError.fromResponse(action.payload);
      state.error = e || action.payload;
      state.loading = false;
    },
  },
  extraReducers: {
    [fetchMyProducts.pending]: setLoading,
    [fetchMyProducts.fulfilled]: (state, {payload}) => {
      myShopAdapter.setAll(state, payload.results);
      state.next = payload.next;
      state.previous = payload.previous;
      state.count = payload.count;
      state.loading = false;
      state.error = null;
    },

    [addMyProduct.pending]: setLoading,
    [addMyProduct.fulfilled]: (state, {payload}) => {
      myShopAdapter.addOne(state, payload);
      state.loading = false;
      state.error = null;
      toast.success("Товар добавлен!");
    },

    [updateMyProduct.pending]: setLoading,
    [updateMyProduct.fulfilled]: (state, {payload}) => {
      // myShopAdapter.addOne(state, payload);
      myShopAdapter.updateOne(state, {
        id: payload.id,
        changes: payload
      })
      state.loading = false;
      state.error = null;
      toast.success("Товар изменён!");
    },

    [deleteMyProduct.pending]: setLoading,
    [deleteMyProduct.fulfilled]: (state, {payload}) => {
      myShopAdapter.removeOne(state, payload.id);
      state.loading = false;
      state.error = null;
      toast.warn("Товар Удалён!");
    },

    [fetchProductUpgradeOptions.fulfilled]: (state, { payload: { paymentTypes, days } })=>{
      state.days = days;
      state.paymentTypes = paymentTypes;
    }
  },
});

export const {
  actions: {
    resetMyShopSlice,
    myShopError,
  }
} = myShopSlice;


export default myShopSlice.reducer
