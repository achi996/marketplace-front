import { createAsyncThunk, createEntityAdapter, createSlice } from "@reduxjs/toolkit";
import Axios from "axios";
import { API_BASE_URL } from "../../helpers/constants";
import authAxios from "../../helpers/utils/authAxios";
import { history } from "../../history";

const initialState = {
  loading: false,
  error: null,
  previous: null,
  next: null,
  count: 0,
  mainPageProducts: [],
};

export const productsAdapter = createEntityAdapter({
  selectId: item=>item.slug
});

export const productsSelector = productsAdapter.getSelectors();

export const fetchProduct = createAsyncThunk(
  "products/fetchProduct",
  async ({ slug }, {dispatch})=>{
    try {
      const { data } = await Axios.get(`${API_BASE_URL}/store/products/${slug}/`);
      return data;
    } catch (error) {
      dispatch(productsError(error));
      throw error;
    }
  }
);

export const fetchMainPageProducts = createAsyncThunk(
  "products/fetchMainPageProducts",
  async (_, { rejectWithValue })=>{
    try {
      //TODO поменять на другой endpoint
      const { data } = await Axios.get(`${API_BASE_URL}/store/products/?featured=true`);
      return data.results;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
)

export const fetchProducts = createAsyncThunk(
  "products/fetchProducts",
  async (_, {rejectWithValue, getState})=>{
    try {
      const queryString = getState().query.queryString || "limit=21";
      const { data } = await Axios.get(`${API_BASE_URL}/store/products/?${queryString}`);
      data.results.forEach(item => {
        item.inCartCount = 1;
      });
      return data;
    } catch (error) {
      return rejectWithValue(error.response || error);
    }
  }
);

export const addProductReview = createAsyncThunk(
  "products/addProductReview",
  async ({
    product,
    comment,
    stars,
  },{rejectWithValue})=>{
    try {
      const { data } = await authAxios.post(`/store/reviews/`, {
        product,
        comment,
        stars,
      });
      return data;
    } catch (error) {
      if(error?.response?.status === 401){
        history.push("/?auth-modal=login");
      }
      return rejectWithValue(error);
    }
  }
)


const setLoading = (state)=>{
  state.loading = true;
  state.error = null;
};

const productsSlice = createSlice({
  name: "products",
  initialState: productsAdapter.getInitialState(initialState),
  reducers: {
    resetProductsSlice(){
      return productsAdapter.getInitialState(initialState);
    },
    productsError(state, {payload}){
      state.error = payload;
      state.loading = false;
    }
  },
  extraReducers: {
    [fetchProducts.pending]: setLoading,
    [fetchProducts.fulfilled]: (state, {payload})=>{
      state.next = payload.next;
      state.previous = payload.previous;
      state.count = payload.count;
      state.loading = false;
      state.error = null;
      productsAdapter.setAll(state, payload.results);
    },
    [fetchProducts.rejected]: (state, {payload})=>{
      if(payload.status === 404){
        productsAdapter.setAll(state, []);
        state.loading = false;
      }
    },

    [fetchProduct.pending]: setLoading,
    [fetchProduct.fulfilled]: (state, {payload})=>{
      state.loading = false;
      state.error = null;
      if(state.ids.includes(payload.slug)){
        productsAdapter.updateOne(state,{
          id: payload.slug,
          changes: payload,
        })
      }else{
        productsAdapter.addOne(state, payload);
      }
    },

    [addProductReview.fulfilled]: (state, {payload})=>{
      // const product = productsSelector.selectById(state, payload.product);
      const product = productsSelector.selectAll(state).find(item=>item.id === payload.product);
      if(product){
        productsAdapter.updateOne(state, {
          id: product.slug,
          changes: {
            reviews: [...product.reviews, payload]
          }
        });
      }
    },

    // [fetchMainPageProducts.pending]: setLoading,
    [fetchMainPageProducts.fulfilled]: (state, { payload })=>{
      state.mainPageProducts = payload;
    },
  },
});

export const {
  resetProductsSlice,
  productsError,
} = productsSlice.actions;

const productsReducer = productsSlice.reducer;
export default productsReducer;
