// function createCalcFunction(n){
//   return function (){
//     console.log(1000 * n)
//   }
// }
//
// const calc = createCalcFunction(42)
// calc()

// function createIncrementer(n){
//   return function (num){
//     return n + num
//   }
// }
//
// const addOne = createIncrementer(1)
// const addTen = createIncrementer(10)
//
// console.log(addOne(10))
// console.log(addOne(41))
// console.log(addTen(10))
// console.log(addTen(41))

// function urlGenerator(domain){
//   return function (url){
//     return `https://${url}.${domain}`
//   }
// }
//
// const comUrl = urlGenerator('com')
//
// console.log(comUrl("yahoo"))

//
// function logPerson(){
//   console.log(`Person: ${this.name}, ${this.age}, ${this.job}`)
// }
//
// function bind(person, func){
//   // this.person = person.name
//   // this.age = person.age
//   // this.job = person.job
//   //
//   // return func()
//
//   return function(...args){
//     console.log(args)
//     func.apply(person, args)
//   }
// }
//
// const person1 = {name: "Viktor", age: 22, job: 'Frontend'};
// const person2 = {name: "Elena", age: 19, job: 'SMM'};
//
// bind(person1, logPerson)()
// bind(person2, logPerson)()
//
// const p = new Promise(function (resolve, reject) {
//   setTimeout(() => {
//
//     console.log('Preparing data...')
//
//     const backedData = {
//       server: "aws",
//       port: 2000,
//       status: 'working'
//     }
//     resolve(backedData)
//   }, 2000)
// }).then(res => {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       res.modified = true
//       reject(res)
//     }, 2000)
//   })
// }).then(clientData => {
//   clientData.fromPromise = true
//   return clientData
// }).then(d => {
//   console.log(d)
// }).catch(err => {
//   console.log('Error', err)
// }).finally(() => {
//   console.log("Finally")
// })

// const sleep = ms => new Promise(resolve =>
//   setTimeout(() => resolve(), ms)
// )
//
// Promise.all([sleep(2000), sleep(5000)]).then(() => {
//   console.log('after all promises')
// })
//
// Promise.race([sleep(3000), sleep(3000)]).then(() => {
//   console.log('race')
// })

function hello(){
  console.log('hello', this)
}
const person = {
  name: "Vladilen",
  age: 25,
  sayHello: hello,
  sayHelloWindow: hello.bind(document),
  logInfo: function (job, phone){
    console.group(`${this.name} info: `)
    console.log(`Name: ${this.name}`)
    console.log(`Age is ${this.age}`)
    console.log(`Job is ${job}`)
    console.log(`phone is ${phone}`)
    console.groupEnd()
  },
}

const lena = {
  name: "Elena",
  age: 23
}

// person.logInfo.bind(lena, "Frontend", "8-999-765-67-67")()
//
// person.logInfo.call(lena,  "Frontend", "8-999-765-67-67")

person.logInfo.apply(lena, ["Frontend", "8-999-765-67-67"])

const array = [1, 2, 3, 4, 5]

// function multBy(arr ,n){
//   return arr.map(i => {
//     return i * n
//   })
// }

Array.prototype.multBy =  function (n){
  return this.map(i => {
    return i * n
  })
}
console.log(array.multBy(5));